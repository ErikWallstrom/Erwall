# Todo

* Increase tests to cover every language feature
* Type cast compatability
* BigInt, BigFloat, Glyphs etc.
* Main function arguments
* Defer and unsafe
* Inline functions
* Tail calls
* Interpreter
