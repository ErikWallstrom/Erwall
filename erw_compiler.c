/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "erw_compiler.h"
#include "erw_semantics.h"
#include "erw_parser.h"
#include "file.h"
#include "log.h"

static Vec(struct Str) getlines(const char* source)
{
	log_assert(source, "is NULL");

	Vec(struct Str) lines = vec_ctor(struct Str, 0);
	char* newlinepos;
	while((newlinepos = strchr(source, '\n')))
	{
		struct Str line;
		size_t offset = newlinepos - source + 1;
		str_ctorfmt(&line, "%.*s", (int)(offset - 1), source);
		vec_pushback(lines, line);
		source += offset;
	}

	struct Str line;
	str_ctor(&line, "");
	vec_pushback(lines, line);
	return lines;
}

struct erw_CompilerResult erw_compile(const char* filename)
{
	log_assert(filename, "is NULL");

	struct File file;
	file_ctor(&file, filename, FILEMODE_READ);

	struct erw_CompilerResult result;
	result.lines = getlines(file.content);
	Vec(struct erw_Token) tokens = erw_tokenize(
		file.content, 
		result.lines, 
		filename
	);
	file_dtor(&file);

	result.ast = erw_parse(tokens, result.lines, filename);
	result.scope = erw_checksemantics(result.ast, result.lines, filename);
	return result;
}

