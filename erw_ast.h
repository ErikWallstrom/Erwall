/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ERW_AST_H
#define ERW_AST_H

#include "erw_tokenizer.h"
#include "vec.h"

struct erw_ASTNodeType
{
	const char* name;
};

typedef const struct erw_ASTNodeType* erw_ASTNodeType;

extern erw_ASTNodeType const erw_ASTNODETYPE_START;
extern erw_ASTNodeType const erw_ASTNODETYPE_FUNCDEF;
extern erw_ASTNodeType const erw_ASTNODETYPE_TYPEDECLR;
extern erw_ASTNodeType const erw_ASTNODETYPE_VARDECLR;
extern erw_ASTNodeType const erw_ASTNODETYPE_BLOCK;
extern erw_ASTNodeType const erw_ASTNODETYPE_IF;
extern erw_ASTNodeType const erw_ASTNODETYPE_ELSEIF;
extern erw_ASTNodeType const erw_ASTNODETYPE_ELSE;
extern erw_ASTNodeType const erw_ASTNODETYPE_RETURN;
extern erw_ASTNodeType const erw_ASTNODETYPE_ASSIGNMENT;
extern erw_ASTNodeType const erw_ASTNODETYPE_UNEXPR;
extern erw_ASTNodeType const erw_ASTNODETYPE_BINEXPR;
extern erw_ASTNodeType const erw_ASTNODETYPE_FUNCCALL;
extern erw_ASTNodeType const erw_ASTNODETYPE_CAST;
extern erw_ASTNodeType const erw_ASTNODETYPE_DEFER;
extern erw_ASTNodeType const erw_ASTNODETYPE_WHILE;
extern erw_ASTNodeType const erw_ASTNODETYPE_ENUM;
extern erw_ASTNodeType const erw_ASTNODETYPE_STRUCTMEMBER;
extern erw_ASTNodeType const erw_ASTNODETYPE_ENUMMEMBER;
extern erw_ASTNodeType const erw_ASTNODETYPE_STRUCT;
extern erw_ASTNodeType const erw_ASTNODETYPE_UNION;
extern erw_ASTNodeType const erw_ASTNODETYPE_UNSAFE;
extern erw_ASTNodeType const erw_ASTNODETYPE_REFERENCE;
extern erw_ASTNodeType const erw_ASTNODETYPE_ARRAY;
extern erw_ASTNodeType const erw_ASTNODETYPE_SLICE;
extern erw_ASTNodeType const erw_ASTNODETYPE_LITERAL;
extern erw_ASTNodeType const erw_ASTNODETYPE_TYPE;
extern erw_ASTNodeType const erw_ASTNODETYPE_FUNCTYPE;
extern erw_ASTNodeType const erw_ASTNODETYPE_ACCESS;
extern erw_ASTNodeType const erw_ASTNODETYPE_STRUCTLITERAL;
extern erw_ASTNodeType const erw_ASTNODETYPE_ARRAYLITERAL;
extern erw_ASTNodeType const erw_ASTNODETYPE_UNIONLITERAL;
extern erw_ASTNodeType const erw_ASTNODETYPE_ENUMLITERAL;
extern erw_ASTNodeType const erw_ASTNODETYPE_MATCH;
extern erw_ASTNodeType const erw_ASTNODETYPE_CASE;
extern erw_ASTNodeType const erw_ASTNODETYPE_FOREIGN;
extern erw_ASTNodeType const erw_ASTNODETYPE_BUILTINCALL;
extern erw_ASTNodeType const erw_ASTNODETYPE_BREAK;
extern erw_ASTNodeType const erw_ASTNODETYPE_CONTINUE;
extern erw_ASTNodeType const erw_ASTNODETYPE_FOR;
extern erw_ASTNodeType const erw_ASTNODETYPE_FOREACH;
extern erw_ASTNodeType const erw_ASTNODETYPE_FUNCLITERAL;
extern erw_ASTNodeType const erw_ASTNODETYPE_IMPORT;
extern erw_ASTNodeType const erw_ASTNODETYPE_TEMPLATE;
extern erw_ASTNodeType const erw_ASTNODETYPE_TEMPLATECALL;

struct erw_ASTNode
{
	union
	{
		struct
		{
			Vec(struct erw_ASTNode*) children;
			const char* filename;
		} start;

		struct
		{
			Vec(struct erw_ASTNode*) params;
			struct erw_Token* name;
			struct erw_ASTNode* type;
			struct erw_ASTNode* block;
			int isvariadic;
		} funcdef;

		struct
		{
			struct erw_Token* name;
			struct erw_ASTNode* type;
		} typedeclr;

		struct
		{
			struct erw_Token* name;
			struct erw_ASTNode* type;
			struct erw_ASTNode* value;
			int ismut;
			int isref;
			int isrefmut;
			int isvariadic;
		} vardeclr;

		struct
		{
			struct erw_Token* name;
			struct erw_ASTNode* type;
			struct erw_ASTNode* value;
		} structmember;

		struct
		{
			Vec(struct erw_ASTNode*) stmts;
		} block;

		struct
		{
			Vec(struct erw_ASTNode*) elseifs;
			struct erw_ASTNode* expr;
			struct erw_ASTNode* block;
			struct erw_ASTNode* else_;
		} if_;

		struct
		{
			struct erw_ASTNode* expr;
			struct erw_ASTNode* block;
		} elseif;

		struct
		{
			struct erw_ASTNode* block;
		} else_;

		struct
		{
			struct erw_ASTNode* expr;
		} return_;

		struct
		{
			struct erw_ASTNode* assignee;
			struct erw_ASTNode* expr;
		} assignment;

		struct
		{
			struct erw_ASTNode* expr;
			void* literaltype; //XXX: Temporary
			int left; //Used to distinguish reference/dereference //XXX: Ugly
			int mutable; //Used to distinguish mutable referencing
		} unexpr;

		struct
		{
			struct erw_ASTNode* expr1;
			struct erw_ASTNode* expr2;
			Vec(char) result;
			struct erw_Type* accesstype;
		} binexpr;

		struct
		{
			Vec(struct erw_ASTNode*) args;
			struct erw_ASTNode* callee;
		} funccall;

		struct
		{
			struct erw_ASTNode* type;
			struct erw_ASTNode* expr;
			void* casttype; //XXX: Temporary
		} cast;

		struct
		{
			struct erw_ASTNode* block;
		} defer;

		struct
		{
			struct erw_ASTNode* expr;
			struct erw_ASTNode* block;
		} while_;

		struct //Add type?
		{
			Vec(struct erw_ASTNode*) members;
			struct erw_ASTNode* type;
		} enum_;

		struct
		{
			struct erw_Token* name;
			struct erw_Token* str;
			struct erw_ASTNode* value;
		} enummember;

		struct
		{
			Vec(struct erw_ASTNode*) members;
			struct erw_ASTNode* extension;
		} struct_;

		struct
		{
			Vec(struct erw_ASTNode*) members;
		} union_;

		struct
		{
			struct erw_ASTNode* block;
		} unsafe;

		struct
		{
			struct erw_ASTNode* type;
			int mutable;
		} reference;

		struct
		{
			struct erw_ASTNode* type;
			struct erw_ASTNode* size;
		} array;

		struct
		{
			struct erw_ASTNode* type;
			int mutable;
		} slice;

		struct
		{
			Vec(struct erw_ASTNode*) params;
			struct erw_ASTNode* type;
		} functype;

		struct
		{
			struct erw_ASTNode* expr;
			struct erw_ASTNode* index;
			int isslice;
		} access;

		struct
		{
			Vec(struct erw_Token*) names;
			Vec(struct erw_ASTNode*) values;
			void* literaltype; //XXX: Temporary
		} structliteral;

		struct
		{
			Vec(struct erw_ASTNode*) values;
			void* literaltype; //XXX: Temporary
			struct erw_ASTNode* length;
			//Add range?
		} arrayliteral;

		struct
		{
			struct erw_ASTNode* type;
			struct erw_ASTNode* value;
			void* literaltype; //XXX: Temporary
			void* uniontype; //XXX: Temporary
		} unionliteral;

		struct
		{
			struct erw_Token* name;
			struct erw_Type* literaltype; //XXX: Temporary
		} enumliteral;

		struct
		{
			struct erw_ASTNode* expr;
			Vec(struct erw_ASTNode*) cases;
			struct erw_ASTNode* else_;
			struct erw_Type* exprtype;
		} match;

		struct
		{
			struct erw_ASTNode* expr;
			struct erw_ASTNode* block;
		} case_;

		struct
		{
			struct erw_Token* name;
			struct erw_ASTNode* node;
		} foreign;

		struct
		{
			struct erw_ASTNode* param;
			void* builtintype; //XXX: Temporary
		} builtincall;

		struct
		{
			struct erw_ASTNode* var;
			struct erw_ASTNode* expr;
			struct erw_ASTNode* increment;
			struct erw_ASTNode* block;
		} for_;

		struct
		{
			struct erw_ASTNode* var;
			struct erw_ASTNode* expr;
			struct erw_ASTNode* block;
			struct erw_Type* exprtype;
		} foreach;

		struct
		{
			Vec(struct erw_ASTNode*) params;
			struct erw_ASTNode* type;
			struct erw_ASTNode* block;
			int isvariadic;
			struct erw_Type* functype;
		} funcliteral;

		struct //XXX: Ugly
		{
			struct erw_Type* extensiontype;
			struct erw_Type* structtype;
		} extensiontypes;

		struct
		{
			struct erw_Token* file;
			struct erw_Token* name;
			size_t moduleindex;
		} import;

		struct
		{
			Vec(struct erw_Token*) types;
			Vec(struct erw_ASTNode*) generations;
			struct erw_ASTNode* body;
		} template;

		struct
		{
			Vec(struct erw_ASTNode*) types;
			struct erw_ASTNode* body;
		} templatecall;
	};

	erw_ASTNodeType type;
	struct erw_Token* token;
};

struct erw_ASTNode* erw_ast_new(erw_ASTNodeType type, struct erw_Token* token);
void erw_ast_print(struct erw_ASTNode* ast);
void erw_ast_dtor(struct erw_ASTNode* ast);

#endif
