/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ERW_TOKENIZER_H
#define ERW_TOKENIZER_H

#include "str.h"
#include "vec.h"

struct erw_TokenType
{
	const char* const name;
};

typedef const struct erw_TokenType* erw_TokenType;

extern erw_TokenType const erw_TOKENTYPE_KEYWORD_RETURN;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_FUNC;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_LET;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_MUT;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_TYPE;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_IF;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_ELSEIF;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_ELSE;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_CAST;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_DEFER;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_WHILE;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_STRUCT;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_UNION;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_ENUM;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_ARRAY;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_UNSAFE;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_MATCH;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_CASE;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_REF;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_FOREIGN;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_LENGTHOF;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_SIZEOF;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_ENUMVALUE;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_ENUMNAME;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_BREAK;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_CONTINUE;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_EXTEND;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_FOR;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_FOREACH;
extern erw_TokenType const erw_TOKENTYPE_KEYWORD_IMPORT;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_DECLR;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_ADD;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_SUB;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_MUL;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_DIV;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_MOD;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_POW;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_RETURN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_EQUAL;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_NOT;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_NOTEQUAL;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_LESS;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_GREATER;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_LESSOREQUAL;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_GREATEROREQUAL;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_AND;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_OR;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_ASSIGN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_ADDASSIGN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_SUBASSIGN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_MULASSIGN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_DIVASSIGN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_MODASSIGN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_POWASSIGN;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_BITOR;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_BITAND;
extern erw_TokenType const erw_TOKENTYPE_OPERATOR_ACCESS;
extern erw_TokenType const erw_TOKENTYPE_LITERAL_INT;
extern erw_TokenType const erw_TOKENTYPE_LITERAL_FLOAT;
extern erw_TokenType const erw_TOKENTYPE_LITERAL_STRING;
extern erw_TokenType const erw_TOKENTYPE_LITERAL_CHAR;
extern erw_TokenType const erw_TOKENTYPE_LITERAL_BOOL;
extern erw_TokenType const erw_TOKENTYPE_IDENT;
extern erw_TokenType const erw_TOKENTYPE_TYPE;
extern erw_TokenType const erw_TOKENTYPE_END;
extern erw_TokenType const erw_TOKENTYPE_COMMA;
extern erw_TokenType const erw_TOKENTYPE_LPAREN;
extern erw_TokenType const erw_TOKENTYPE_RPAREN;
extern erw_TokenType const erw_TOKENTYPE_LCURLY;
extern erw_TokenType const erw_TOKENTYPE_RCURLY;
extern erw_TokenType const erw_TOKENTYPE_LBRACKET;
extern erw_TokenType const erw_TOKENTYPE_RBRACKET;

struct erw_Token
{
	Vec(char) text;
	erw_TokenType type;
	size_t linenum;
	size_t column;
};

Vec(struct erw_Token) erw_tokenize(
	const char* source, 
	Vec(struct Str) lines,
	const char* filename
);
void erw_tokens_delete(Vec(struct erw_Token) tokens);

#endif
