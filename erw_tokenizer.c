/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "erw_tokenizer.h"
#include "erw_error.h"
#include "log.h"

#include <ctype.h>

//Wall of erw_TokenType initializations
erw_TokenType const erw_TOKENTYPE_KEYWORD_RETURN 	= &(struct erw_TokenType){"return"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_FUNC 		= &(struct erw_TokenType){"func"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_LET 		= &(struct erw_TokenType){"let"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_MUT 		= &(struct erw_TokenType){"mut"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_TYPE 		= &(struct erw_TokenType){"type"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_IF 		= &(struct erw_TokenType){"if"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_ELSEIF 	= &(struct erw_TokenType){"else_if"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_ELSE 		= &(struct erw_TokenType){"else"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_CAST 		= &(struct erw_TokenType){"cast"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_DEFER 	= &(struct erw_TokenType){"defer"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_WHILE 	= &(struct erw_TokenType){"while"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_STRUCT 	= &(struct erw_TokenType){"struct"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_UNION 	= &(struct erw_TokenType){"union"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_ENUM 		= &(struct erw_TokenType){"enum"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_ARRAY 	= &(struct erw_TokenType){"array"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_UNSAFE 	= &(struct erw_TokenType){"unsafe"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_MATCH 	= &(struct erw_TokenType){"match"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_CASE 		= &(struct erw_TokenType){"case"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_REF 		= &(struct erw_TokenType){"ref"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_FOREIGN 	= &(struct erw_TokenType){"foreign"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_LENGTHOF 	= &(struct erw_TokenType){"len_of"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_SIZEOF 	= &(struct erw_TokenType){"size_of"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_ENUMVALUE = &(struct erw_TokenType){"enum_value"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_ENUMNAME 	= &(struct erw_TokenType){"enum_name"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_BREAK 	= &(struct erw_TokenType){"break"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_CONTINUE 	= &(struct erw_TokenType){"continue"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_EXTEND 	= &(struct erw_TokenType){"extend"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_FOR 		= &(struct erw_TokenType){"for"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_FOREACH 	= &(struct erw_TokenType){"for_each"};
erw_TokenType const erw_TOKENTYPE_KEYWORD_IMPORT 	= &(struct erw_TokenType){"import"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_DECLR 	= &(struct erw_TokenType){":"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_ADD 		= &(struct erw_TokenType){"+"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_SUB 		= &(struct erw_TokenType){"-"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_MUL 		= &(struct erw_TokenType){"*"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_DIV 		= &(struct erw_TokenType){"/"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_MOD 		= &(struct erw_TokenType){"%"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_POW 		= &(struct erw_TokenType){"^"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_RETURN 	= &(struct erw_TokenType){"->"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_EQUAL 	= &(struct erw_TokenType){"=="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_NOT 		= &(struct erw_TokenType){"!"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_NOTEQUAL = &(struct erw_TokenType){"!="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_LESS 	= &(struct erw_TokenType){"<"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_GREATER 	= &(struct erw_TokenType){">"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_LESSOREQUAL 		= &(struct erw_TokenType){"<="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_GREATEROREQUAL 	= &(struct erw_TokenType){">="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_AND 		= &(struct erw_TokenType){"and"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_OR 		= &(struct erw_TokenType){"or"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_ASSIGN 	= &(struct erw_TokenType){"="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_ADDASSIGN = &(struct erw_TokenType){"+="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_SUBASSIGN = &(struct erw_TokenType){"-="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_MULASSIGN = &(struct erw_TokenType){"*="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_DIVASSIGN = &(struct erw_TokenType){"/="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_MODASSIGN = &(struct erw_TokenType){"%="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_POWASSIGN = &(struct erw_TokenType){"^="};
erw_TokenType const erw_TOKENTYPE_OPERATOR_BITOR 	= &(struct erw_TokenType){"|"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_BITAND 	= &(struct erw_TokenType){"&"};
erw_TokenType const erw_TOKENTYPE_OPERATOR_ACCESS 	= &(struct erw_TokenType){"."};
erw_TokenType const erw_TOKENTYPE_LITERAL_INT 		= &(struct erw_TokenType){"Literal Int"};
erw_TokenType const erw_TOKENTYPE_LITERAL_FLOAT 	= &(struct erw_TokenType){"Literal Float"};
erw_TokenType const erw_TOKENTYPE_LITERAL_STRING 	= &(struct erw_TokenType){"Literal String"};
erw_TokenType const erw_TOKENTYPE_LITERAL_CHAR 		= &(struct erw_TokenType){"Literal Char"};
erw_TokenType const erw_TOKENTYPE_LITERAL_BOOL 		= &(struct erw_TokenType){"Literal Bool"};
erw_TokenType const erw_TOKENTYPE_IDENT 	= &(struct erw_TokenType){"Identifier"};
erw_TokenType const erw_TOKENTYPE_TYPE 		= &(struct erw_TokenType){"Type"};
erw_TokenType const erw_TOKENTYPE_END 		= &(struct erw_TokenType){";"};
erw_TokenType const erw_TOKENTYPE_COMMA 	= &(struct erw_TokenType){","};
erw_TokenType const erw_TOKENTYPE_LPAREN 	= &(struct erw_TokenType){"("};
erw_TokenType const erw_TOKENTYPE_RPAREN 	= &(struct erw_TokenType){")"};
erw_TokenType const erw_TOKENTYPE_LCURLY 	= &(struct erw_TokenType){"{"};
erw_TokenType const erw_TOKENTYPE_RCURLY 	= &(struct erw_TokenType){"}"};
erw_TokenType const erw_TOKENTYPE_LBRACKET 	= &(struct erw_TokenType){"["};
erw_TokenType const erw_TOKENTYPE_RBRACKET 	= &(struct erw_TokenType){"]"};

static erw_TokenType erw_reserved[] = {
	erw_TOKENTYPE_KEYWORD_LET,
	erw_TOKENTYPE_KEYWORD_MUT,
	erw_TOKENTYPE_KEYWORD_FUNC,
	erw_TOKENTYPE_KEYWORD_TYPE,
	erw_TOKENTYPE_KEYWORD_RETURN,
	erw_TOKENTYPE_KEYWORD_IF,
	erw_TOKENTYPE_KEYWORD_ELSEIF,
	erw_TOKENTYPE_KEYWORD_ELSE,
	erw_TOKENTYPE_KEYWORD_CAST,
	erw_TOKENTYPE_KEYWORD_DEFER,
	erw_TOKENTYPE_KEYWORD_WHILE,
	erw_TOKENTYPE_KEYWORD_STRUCT,
	erw_TOKENTYPE_KEYWORD_UNION,
	erw_TOKENTYPE_KEYWORD_ENUM,
	erw_TOKENTYPE_KEYWORD_ARRAY,
	erw_TOKENTYPE_KEYWORD_UNSAFE,
	erw_TOKENTYPE_KEYWORD_MATCH,
	erw_TOKENTYPE_KEYWORD_CASE,
	erw_TOKENTYPE_KEYWORD_REF,
	erw_TOKENTYPE_KEYWORD_FOREIGN,
	erw_TOKENTYPE_KEYWORD_LENGTHOF,
	erw_TOKENTYPE_KEYWORD_SIZEOF,
	erw_TOKENTYPE_KEYWORD_ENUMVALUE,
	erw_TOKENTYPE_KEYWORD_ENUMNAME,
	erw_TOKENTYPE_KEYWORD_BREAK,
	erw_TOKENTYPE_KEYWORD_CONTINUE,
	erw_TOKENTYPE_KEYWORD_EXTEND,
	erw_TOKENTYPE_KEYWORD_FOR,
	erw_TOKENTYPE_KEYWORD_FOREACH,
	erw_TOKENTYPE_KEYWORD_IMPORT,
	erw_TOKENTYPE_OPERATOR_AND,
	erw_TOKENTYPE_OPERATOR_OR,
	erw_TOKENTYPE_LITERAL_BOOL,
	erw_TOKENTYPE_LITERAL_BOOL,
};

static erw_TokenType erw_singleop[] = {
	erw_TOKENTYPE_OPERATOR_DECLR,
	erw_TOKENTYPE_OPERATOR_ADD,
	erw_TOKENTYPE_OPERATOR_SUB,
	erw_TOKENTYPE_OPERATOR_MUL,
	erw_TOKENTYPE_OPERATOR_DIV,
	erw_TOKENTYPE_OPERATOR_MOD,
	erw_TOKENTYPE_OPERATOR_POW,
	erw_TOKENTYPE_OPERATOR_NOT,
	erw_TOKENTYPE_OPERATOR_LESS,
	erw_TOKENTYPE_OPERATOR_GREATER,
	erw_TOKENTYPE_OPERATOR_ASSIGN,
	erw_TOKENTYPE_OPERATOR_BITOR,
	erw_TOKENTYPE_OPERATOR_BITAND,
	erw_TOKENTYPE_OPERATOR_ACCESS,
	erw_TOKENTYPE_END,
	erw_TOKENTYPE_COMMA,
	erw_TOKENTYPE_LPAREN,
	erw_TOKENTYPE_RPAREN,
	erw_TOKENTYPE_LCURLY,
	erw_TOKENTYPE_RCURLY,
	erw_TOKENTYPE_LBRACKET,
	erw_TOKENTYPE_RBRACKET,
};

static erw_TokenType erw_mulop[] = {
	erw_TOKENTYPE_OPERATOR_RETURN,
	erw_TOKENTYPE_OPERATOR_EQUAL,
	erw_TOKENTYPE_OPERATOR_NOTEQUAL,
	erw_TOKENTYPE_OPERATOR_LESSOREQUAL,
	erw_TOKENTYPE_OPERATOR_GREATEROREQUAL,
	erw_TOKENTYPE_OPERATOR_ADDASSIGN,
	erw_TOKENTYPE_OPERATOR_SUBASSIGN,
	erw_TOKENTYPE_OPERATOR_MULASSIGN,
	erw_TOKENTYPE_OPERATOR_DIVASSIGN,
	erw_TOKENTYPE_OPERATOR_MODASSIGN,
	erw_TOKENTYPE_OPERATOR_POWASSIGN,
};

Vec(struct erw_Token) erw_tokenize(
	const char* source, 
	Vec(struct Str) lines,
	const char* filename)
{
	log_assert(source, "is NULL");
	log_assert(lines, "is NULL");
	log_assert(filename, "is NULL");

	erw_errorfilename = filename;
	Vec(struct erw_Token) tokens = vec_ctor(struct erw_Token, 0);

	size_t pos = 0;
	size_t line = 1;
	size_t column = 1;

	while(source[pos] != '\0')
	{
		if(isblank(source[pos]))
		{
			column++;
			pos++;
			continue;
		}
		else if(source[pos] == '\n')
		{
			column = 1;
			line++;
			pos++;
			continue;
		}
		else if(source[pos] == '@')
		{
			if(source[pos + 1] == '[') //Multiline
			{
				size_t startcolumn = column + 1;
				size_t startline = line;
				size_t nested = 0;
				do {
					pos++;
					if(source[pos] == '@')
					{
						if(source[pos + 1] == '[')
						{
							nested++;
							column++;
							pos++;
						}
						else if(source[pos + 1] == ']')
						{
							if(nested)
							{
								nested--;
							}
							else
							{
								pos += 2;
								column++;
								break;
							}
						}
					}
					else if(source[pos] == '\n')
					{
						column = 1;
						line++;
					}
					else
					{
						column++;
					}
				} while(source[pos] != '\0');

				if(nested || source[pos - 1] != ']')
				{
					struct Str msg;
					str_ctor(
						&msg,
						"No comment ending (expected '@]')"
					);

					erw_error(
						msg.data,
						lines[startline - 1].data,
						startline,
						startcolumn - 1,
						column + 1
					);
					str_dtor(&msg);
				}
			}
			else if(source[pos + 1] == '@') //Multiline
			{
				struct Str msg;
				str_ctor(
					&msg,
					"Unexpected comment ending ('@]')"
				);

				erw_error(
					msg.data,
					lines[line - 1].data,
					line,
					column,
					column + 1
				);
				str_dtor(&msg);
			}
			else //Single line comment
			{
				do {
					column++;
					pos++;
				}
				while(
					source[pos] != '\n' &&
					//source[pos] != ';' &&
					source[pos] != '\0'
				);
			}

			continue;
		}

		struct erw_Token token = {
			.text = vec_ctor(char, 0),
			.linenum = line,
			.column = column
		};

		if(isupper(source[pos]))
		{
			do {
				column++;
				vec_pushback(token.text, source[pos]);
				pos++;
			}
			while(isalnum(source[pos]) || source[pos] == '_');
			token.type = erw_TOKENTYPE_TYPE;
		}
		else if(islower(source[pos]))
		{
			do {
				column++;
				vec_pushback(token.text, source[pos]);
				pos++;
			}
			while(isalnum(source[pos]) || source[pos] == '_');

			for(size_t i = 0; 
				i < sizeof erw_reserved / sizeof erw_reserved[0]; 
				i++)
			{
				size_t len = vec_getsize(token.text);
				if(strlen(erw_reserved[i]->name) == len
					&& !memcmp(erw_reserved[i]->name, token.text, len))
				{
					token.type = erw_reserved[i];
					break;
				}
			}

			if(token.type == NULL)
			{
				token.type = erw_TOKENTYPE_IDENT;
			}
		}
		else if(isdigit(source[pos]) 
			|| (source[pos] == '-' && isdigit(source[pos + 1])))
		{
			if(source[pos] == '-' && isdigit(source[pos + 1])) //-3.14, -10 etc.
			{
				vec_pushback(token.text, source[pos]);
				pos++;
			}

			do {
				column++;
				vec_pushback(token.text, source[pos]);
				pos++;
			}
			while(isdigit(source[pos]));

			if(source[pos] == '.')
			{
				do {
					column++;
					vec_pushback(token.text, source[pos]);
					pos++;
				}
				while(isdigit(source[pos]));
				token.type = erw_TOKENTYPE_LITERAL_FLOAT;
			}
			else
			{
				token.type = erw_TOKENTYPE_LITERAL_INT;
			}
		}
		else
		{
			switch(source[pos])
			{
			case '"':
				{
					size_t startcolumn = column;
					do {
						if(source[pos] == '\\')
						{
							switch(source[pos + 1])
							{
							case '\\':
							case '"':
							case 'a':
							case 'b':
							case 'f': //Should this exist?
							case 'n':
							case 'r':
							case 't':
							case 'v': //Should this exist?
							case '0':
								column += 2;
								vec_pushback(token.text, source[pos]);
								vec_pushback(token.text, source[pos + 1]);
								pos += 2;
								break;

							default:;
								struct Str msg;
								str_ctor(
									&msg,
									"Expected valid character in escape"
										" sequence"
								);

								erw_error(
									msg.data,
									lines[line - 1].data,
									line,
									column + 1,
									column + 1
								);
								str_dtor(&msg);
							}
						}
						else
						{
							column++;
							vec_pushback(token.text, source[pos]);
							pos++;
						}
					}
					while(
						source[pos] != '"' &&
						source[pos] != '\n' &&
						source[pos] != '\0'
					);
					token.type = erw_TOKENTYPE_LITERAL_STRING;

					if(source[pos] != '"')
					{
						struct Str msg;
						str_ctor(
							&msg,
							"Non-terminated string"
						);

						erw_error(
							msg.data,
							lines[line - 1].data,
							line,
							startcolumn,
							column
						);
						str_dtor(&msg);
					}
					break;
				}

			case '\'':
				{
					size_t startcolumn = column;
					int hasescape = 0;
					do {
						column++;
						vec_pushback(token.text, source[pos]);
						pos++;

						if(source[pos] == '\\')
						{
							switch(source[pos + 1])
							{
							case '\\':
							case '\'':
							case 'a':
							case 'b':
							case 'f': //Should this exist?
							case 'n':
							case 'r':
							case 't':
							case 'v': //Should this exist?
							case '0':
								column += 2;
								vec_pushback(token.text, source[pos]);
								vec_pushback(token.text, source[pos + 1]);
								pos += 2;
								hasescape = 1;
								break;

							default:;
								struct Str msg;
								str_ctor(
									&msg,
									"Expected valid character in escape"
										" sequence"
								);

								erw_error(
									msg.data,
									lines[line - 1].data,
									line,
									column + 1,
									column + 1
								);
								str_dtor(&msg);
							}
						}
					}
					while(
						source[pos] != '\'' &&
						source[pos] != '\n' &&
						source[pos] != '\0'
					);
					token.type = erw_TOKENTYPE_LITERAL_CHAR;

					if(source[pos] != '\'' 
						|| vec_getsize(token.text) == 1
						|| (!hasescape && vec_getsize(token.text) > 2)
						|| (hasescape && vec_getsize(token.text) != 3))
					{
						struct Str msg;
						str_ctorfmt(
							&msg,
							"Invalid %s (expected '<single letter or escape"
								" sequence>')",
							erw_TOKENTYPE_LITERAL_CHAR->name
						);

						erw_error(
							msg.data,
							lines[line - 1].data,
							line,
							startcolumn,
							column
						);
						str_dtor(&msg);
					}
					break;
				}

			default:;
				int found = 0;
				for(size_t i = 0; 
					i < sizeof erw_mulop / sizeof erw_mulop[0]; 
					i++)
				{
					//XXX: Only works for length 2 operators
					if(source[pos] == erw_mulop[i]->name[0]
						&& source[pos + 1] == erw_mulop[i]->name[1])
					{
						column++;
						vec_pushback(token.text, source[pos]);
						pos++;
						token.type = erw_mulop[i];

						found = 1;
						break;
					}
				}

				if(!found)
				{
					for(size_t i = 0; 
						i < sizeof erw_singleop / sizeof erw_singleop[0]; 
						i++)
					{
						if(source[pos] == erw_singleop[i]->name[0])
						{
							token.type = erw_singleop[i];
							break;
						}
					}
				}
			}

			vec_pushback(token.text, source[pos]);
			column++;
			pos++;
		}

		vec_pushback(token.text, '\0');
		vec_pushback(tokens, token);
	}

	return tokens;
}

void erw_tokens_delete(Vec(struct erw_Token) tokens)
{
	log_assert(tokens, "is NULL");
	for(size_t i = 0; i < vec_getsize(tokens); i++)
	{
		vec_dtor(tokens[i].text);
	}

	vec_dtor(tokens);
}

