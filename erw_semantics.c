/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "erw_semantics.h"
#include "erw_compiler.h"
#include "erw_error.h"
#include "log.h"
#include <stdlib.h>

static struct erw_ASTNode* erw_getfirstnode(struct erw_ASTNode* expr)
{
	struct erw_ASTNode* firstnode = expr;
	while(firstnode->type == erw_ASTNODETYPE_UNEXPR 
		|| firstnode->type == erw_ASTNODETYPE_BINEXPR
		|| firstnode->type == erw_ASTNODETYPE_ACCESS
		|| firstnode->type == erw_ASTNODETYPE_TEMPLATECALL
		|| firstnode->type == erw_ASTNODETYPE_FUNCCALL)
	{
		if(firstnode->type == erw_ASTNODETYPE_UNEXPR)
		{
			if(firstnode->unexpr.left)
			{
				break;
			}
			else
			{
				firstnode = firstnode->unexpr.expr;
			}
		}
		else if(firstnode->type == erw_ASTNODETYPE_BINEXPR)
		{
			firstnode = firstnode->binexpr.expr1;
		}
		else if(firstnode->type == erw_ASTNODETYPE_ACCESS)
		{
			firstnode = firstnode->access.expr;
		}
		else if(firstnode->type == erw_ASTNODETYPE_FUNCCALL)
		{
			firstnode = firstnode->funccall.callee;
		}
		else if(firstnode->type == erw_ASTNODETYPE_TEMPLATECALL)
		{
			firstnode = erw_getfirstnode(firstnode->templatecall.body);
		}
		else
		{
			log_assert(0, "this shouldn't happen");
		}
	}

	return firstnode;
}

static struct erw_ASTNode* erw_getlastnode(struct erw_ASTNode* expr)
{
	struct erw_ASTNode* lastnode = expr;
	while(lastnode->type == erw_ASTNODETYPE_UNEXPR 
		|| lastnode->type == erw_ASTNODETYPE_ACCESS
		|| lastnode->type == erw_ASTNODETYPE_BINEXPR
		|| lastnode->type == erw_ASTNODETYPE_FUNCCALL
		|| lastnode->type == erw_ASTNODETYPE_STRUCTLITERAL
		|| lastnode->type == erw_ASTNODETYPE_UNIONLITERAL
		|| lastnode->type == erw_ASTNODETYPE_TEMPLATECALL
		|| lastnode->type == erw_ASTNODETYPE_ARRAYLITERAL)
	{
		if(lastnode->type == erw_ASTNODETYPE_UNEXPR)
		{
			if(lastnode->unexpr.left)
			{
				lastnode = lastnode->unexpr.expr;
			}
			else
			{
				break;
			}
		}
		else if(lastnode->type == erw_ASTNODETYPE_BINEXPR)
		{
			lastnode = lastnode->binexpr.expr2;
		}
		else if(lastnode->type == erw_ASTNODETYPE_ACCESS)
		{
			lastnode = lastnode->access.expr;
		}
		else if(lastnode->type == erw_ASTNODETYPE_CAST)
		{
			lastnode = lastnode->cast.expr;
		}
		else if(lastnode->type == erw_ASTNODETYPE_FUNCCALL)
		{
			if(vec_getsize(lastnode->funccall.args))
			{
				lastnode = lastnode->funccall.args[
					vec_getsize(lastnode->funccall.args) - 1
				];
			}
			else
			{
				lastnode = lastnode->funccall.callee;
				break;
			}
		}
		else if(lastnode->type == erw_ASTNODETYPE_STRUCTLITERAL)
		{
			if(vec_getsize(lastnode->funccall.args))
			{
				lastnode = lastnode->structliteral.values[
					vec_getsize(lastnode->structliteral.values) - 1
				];
			}
			else
			{
				break;
			}
		}
		else if(lastnode->type == erw_ASTNODETYPE_UNIONLITERAL)
		{
			if(lastnode->unionliteral.value)
			{
				lastnode = lastnode->unionliteral.value;
			}
			else
			{
				break;
			}
		}
		else if(lastnode->type == erw_ASTNODETYPE_ARRAYLITERAL)
		{
			if(vec_getsize(lastnode->arrayliteral.values))
			{
				lastnode = lastnode->arrayliteral.values[
					vec_getsize(lastnode->arrayliteral.values) - 1
				];
			}
			else
			{
				break;
			}
		}
		else if(lastnode->type == erw_ASTNODETYPE_TEMPLATECALL)
		{
			lastnode = erw_getlastnode(lastnode->templatecall.body);
		}
		else
		{
			log_assert(0, "this shouldn't happen");
		}
	}

	return lastnode;
}

static void erw_checkboolean(
	struct erw_Type* type,
	struct erw_ASTNode* firstnode,
	struct erw_ASTNode* lastnode,
	struct Str* lines)
{
	log_assert(type, "is NULL");
	log_assert(firstnode, "is NULL");
	log_assert(lastnode, "is NULL");
	log_assert(lines, "is NULL");

	struct erw_Type* base = erw_type_getbase(type);
	if(base->info != erw_TYPEINFO_BOOL)
	{
		struct Str typename = erw_type_tostring(type);
		struct Str msg;
		str_ctorfmt(
			&msg, 
			"Expected expression of type '%s', got expression of type '%s'", 
			erw_type_builtins[erw_TYPEBUILTIN_BOOL]->named.name,
			typename.data
		);

		erw_error(
			msg.data, 
			lines[firstnode->token->linenum - 1].data,
			firstnode->token->linenum, 
			firstnode->token->column,
			(lastnode->token->linenum == firstnode->token->linenum) 
				? (size_t)(lastnode->token->column + vec_getsize(
						lastnode->token->text)) - 2
				: lines[firstnode->token->linenum - 1].len
		);
		str_dtor(&msg);
		str_dtor(&typename);
	}
}

static void erw_checknumerical(
	struct erw_Type* type,
	struct erw_ASTNode* firstnode,
	struct erw_ASTNode* lastnode,
	struct Str* lines)
{
	log_assert(type, "is NULL");
	log_assert(firstnode, "is NULL");
	log_assert(lastnode, "is NULL");
	log_assert(lines, "is NULL");
 
	struct erw_Type* base = erw_type_getbase(type);
	if(base->info != erw_TYPEINFO_INT && base->info != erw_TYPEINFO_FLOAT)
	{
		struct Str typename = erw_type_tostring(type);
		struct Str msg;
		str_ctorfmt(
			&msg, 
			"Expected numerical type, got type '%s'", 
			typename.data
		);

		erw_error(
			msg.data, 
			lines[firstnode->token->linenum - 1].data,
			firstnode->token->linenum, 
			firstnode->token->column,
			(lastnode->token->linenum == firstnode->token->linenum) 
				? (size_t)(lastnode->token->column + vec_getsize(
						lastnode->token->text)) - 2
				: lines[firstnode->token->linenum - 1].len
		);
		str_dtor(&msg);
	}
}

static struct erw_Type* erw_getexprtype(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct Str* lines,
	struct erw_Type* hint
);

static struct erw_Type* erw_getaccesstype(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(scope, "is NULL");
	log_assert(node, "is NULL");
	log_assert(lines, "is NULL");

	struct erw_Type* type = erw_getexprtype(
		scope, 
		node->binexpr.expr1, 
		lines, 
		NULL
	);

	struct erw_ASTNode* firstnode = erw_getlastnode(node->binexpr.expr1);
	struct erw_Type* base = erw_type_getbase(type);
	if(base->info != erw_TYPEINFO_STRUCT)
	{
		struct Str typename = erw_type_tostring(type);
		struct Str msg;
		str_ctorfmt(
			&msg, 
			"Attempt to access member of non-struct type ('%s')", 
			typename.data
		);

		erw_error(
			msg.data, 
			lines[firstnode->token->linenum - 1].data,
			firstnode->token->linenum, 
			firstnode->token->column,
			firstnode->token->column + vec_getsize(firstnode->token->text)
				- 2
		);
		str_dtor(&msg);
	}

	struct erw_Type* ret = NULL;
	struct erw_ASTNode* secondnode = erw_getfirstnode(node->binexpr.expr2);
	for(size_t i = 0; i < vec_getsize(base->struct_.members); i++)
	{
		if(!strcmp(base->struct_.members[i].name, secondnode->token->text))
		{
			ret = base->struct_.members[i].type;
			break;
		}
	}

	if(!ret)
	{
		struct Str typename = erw_type_tostring(type);
		struct Str msg;
		str_ctorfmt(
			&msg, 
			"Struct '%s' ('%s') has no member named '%s'", 
			typename.data,
			firstnode->token->text,
			secondnode->token->text
		);

		erw_error(
			msg.data, 
			lines[secondnode->token->linenum - 1].data,
			secondnode->token->linenum, 
			secondnode->token->column,
			secondnode->token->column + vec_getsize(secondnode->token->text) - 2
		);
		str_dtor(&msg);
		str_dtor(&typename);
	}

	node->binexpr.accesstype = type;
	return ret;
}

enum erw_IntLiteralTypeCompat
{
	erw_INTLITERALTYPE_UINT8 	= 1 << 0,
	erw_INTLITERALTYPE_INT8 	= 1 << 1,
	erw_INTLITERALTYPE_UINT16	= 1 << 2,
	erw_INTLITERALTYPE_INT16 	= 1 << 3,
	erw_INTLITERALTYPE_UINT32	= 1 << 4,
	erw_INTLITERALTYPE_INT32 	= 1 << 5,
	erw_INTLITERALTYPE_UINT64	= 1 << 6,
	erw_INTLITERALTYPE_INT64 	= 1 << 7,
	erw_INTLITERALTYPE_BIGINT	= 1 << 8,
};

static int erw_getintliteraltype(char* literal)
{
	int result = erw_INTLITERALTYPE_BIGINT;
	int isnegative = literal[0] == '-';
	
	if(isnegative)
	{	
		size_t len = strlen(literal + 1);
		if(len < sizeof(erw_INT64_MIN) - 2)
		{
			result |= erw_INTLITERALTYPE_INT64;
			if(len < sizeof(erw_INT32_MIN) - 2)
			{
				result |= erw_INTLITERALTYPE_INT32;
				if(len < sizeof(erw_INT16_MIN) - 2)
				{
					result |= erw_INTLITERALTYPE_INT16;
					if(len < sizeof(erw_INT8_MIN) - 2)
					{
						result |= erw_INTLITERALTYPE_INT8;
					}
					else if(len == sizeof(erw_INT8_MIN) - 2)
					{
						int iscompat = 1;
						for(size_t i = 0; i < len; i++)
						{
							if(erw_INT8_MIN[i + 1] < literal[i + 1])
							{
								iscompat = 0;
								break;
							}
							else if(erw_INT8_MIN[i + 1] > literal[i + 1])
							{
								break;
							}
						}

						if(iscompat)
						{
							result |= erw_INTLITERALTYPE_INT8;
						}
					}
				}
				else if(len == sizeof(erw_INT16_MIN) - 2)
				{
					int iscompat = 1;
					for(size_t i = 0; i < len; i++)
					{
						if(erw_INT16_MIN[i + 1] < literal[i + 1])
						{
							iscompat = 0;
							break;
						}
						else if(erw_INT16_MIN[i + 1] > literal[i + 1])
						{
							break;
						}
					}

					if(iscompat)
					{
						result |= erw_INTLITERALTYPE_INT16;
					}
				}
			}
			else if(len == sizeof(erw_INT32_MIN) - 2)
			{
				int iscompat = 1;
				for(size_t i = 0; i < len; i++)
				{
					if(erw_INT32_MIN[i + 1] < literal[i + 1])
					{
						iscompat = 0;
						break;
					}
					else if(erw_INT32_MIN[i + 1] > literal[i + 1])
					{
						break;
					}
				}

				if(iscompat)
				{
					result |= erw_INTLITERALTYPE_INT32;
				}
			}
		}
		else if(len == sizeof(erw_INT64_MIN) - 2)
		{
			int iscompat = 1;
			for(size_t i = 0; i < len; i++)
			{
				if(erw_INT64_MIN[i + 1] < literal[i + 1])
				{
					iscompat = 0;
					break;
				}
				else if(erw_INT64_MIN[i + 1] > literal[i + 1])
				{
					break;
				}
			}

			if(iscompat)
			{
				result |= erw_INTLITERALTYPE_INT64;
			}
		}
	}
	else
	{
		size_t len = strlen(literal);
		if(len < sizeof(erw_INT64_MAX) - 1)
		{
			result |= erw_INTLITERALTYPE_INT64;
			if(len < sizeof(erw_INT32_MAX) - 1)
			{
				result |= erw_INTLITERALTYPE_INT32;
				if(len < sizeof(erw_INT16_MAX) - 1)
				{
					result |= erw_INTLITERALTYPE_INT16;
					if(len < sizeof(erw_INT8_MAX) - 1)
					{
						result |= erw_INTLITERALTYPE_INT8;
					}
					else if(len == sizeof(erw_INT8_MAX) - 1)
					{
						int iscompat = 1;
						for(size_t i = 0; i < len; i++)
						{
							if(erw_INT8_MAX[i] < literal[i])
							{
								iscompat = 0;
								break;
							}
							else if(erw_INT8_MAX[i] > literal[i])
							{
								break;
							}
						}

						if(iscompat)
						{
							result |= erw_INTLITERALTYPE_INT8;
						}
					}
				}
				else if(len == sizeof(erw_INT16_MAX) - 1)
				{
					int iscompat = 1;
					for(size_t i = 0; i < len; i++)
					{
						if(erw_INT16_MAX[i] < literal[i])
						{
							iscompat = 0;
							break;
						}
						else if(erw_INT16_MAX[i] > literal[i])
						{
							break;
						}
					}

					if(iscompat)
					{
						result |= erw_INTLITERALTYPE_INT16;
					}
				}
			}
			else if(len == sizeof(erw_INT32_MAX) - 1)
			{
				int iscompat = 1;
				for(size_t i = 0; i < len; i++)
				{
					if(erw_INT32_MAX[i] < literal[i])
					{
						iscompat = 0;
						break;
					}
					else if(erw_INT32_MAX[i] > literal[i])
					{
						break;
					}
				}

				if(iscompat)
				{
					result |= erw_INTLITERALTYPE_INT32;
				}
			}
		}
		else if(len == sizeof(erw_INT64_MAX) - 1)
		{
			int iscompat = 1;
			for(size_t i = 0; i < len; i++)
			{
				if(erw_INT64_MAX[i] < literal[i])
				{
					iscompat = 0;
					break;
				}
				else if(erw_INT64_MAX[i] > literal[i])
				{
					break;
				}
			}

			if(iscompat)
			{
				result |= erw_INTLITERALTYPE_INT64;
			}
		}

		if(len < sizeof(erw_UINT64_MAX) - 1)
		{
			result |= erw_INTLITERALTYPE_UINT64;
			if(len < sizeof(erw_UINT32_MAX) - 1)
			{
				result |= erw_INTLITERALTYPE_UINT32;
				if(len < sizeof(erw_UINT16_MAX) - 1)
				{
					result |= erw_INTLITERALTYPE_UINT16;
					if(len < sizeof(erw_UINT8_MAX) - 1)
					{
						result |= erw_INTLITERALTYPE_UINT8;
					}
					else if(len == sizeof(erw_UINT8_MAX) - 1)
					{
						int iscompat = 1;
						for(size_t i = 0; i < len; i++)
						{
							if(erw_UINT8_MAX[i] < literal[i])
							{
								iscompat = 0;
								break;
							}
							else if(erw_UINT8_MAX[i] > literal[i])
							{
								break;
							}
						}

						if(iscompat)
						{
							result |= erw_INTLITERALTYPE_UINT8;
						}
					}
				}
				else if(len == sizeof(erw_UINT16_MAX) - 1)
				{
					int iscompat = 1;
					for(size_t i = 0; i < len; i++)
					{
						if(erw_UINT16_MAX[i] < literal[i])
						{
							iscompat = 0;
							break;
						}
						else if(erw_UINT16_MAX[i] > literal[i])
						{
							break;
						}
					}

					if(iscompat)
					{
						result |= erw_INTLITERALTYPE_UINT16;
					}
				}
			}
			else if(len == sizeof(erw_UINT32_MAX) - 1)
			{
				int iscompat = 1;
				for(size_t i = 0; i < len; i++)
				{
					if(erw_UINT32_MAX[i] < literal[i])
					{
						iscompat = 0;
						break;
					}
					else if(erw_UINT32_MAX[i] > literal[i])
					{
						break;
					}
				}

				if(iscompat)
				{
					result |= erw_INTLITERALTYPE_UINT32;
				}
			}
		}
		else if(len == sizeof(erw_UINT64_MAX) - 1)
		{
			int iscompat = 1;
			for(size_t i = 0; i < len; i++)
			{
				if(erw_UINT64_MAX[i] < literal[i])
				{
					iscompat = 0;
					break;
				}
				else if(erw_UINT64_MAX[i] > literal[i])
				{
					break;
				}
			}

			if(iscompat)
			{
				result |= erw_INTLITERALTYPE_UINT64;
			}
		}
	}
	
	return result;
}

static void erw_checkfunccall(
	struct erw_Scope* scope,
	struct erw_ASTNode* node,
	struct Str* lines
);

void erw_checkexprtype( //Used in erw_type.c
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct erw_Type* type,
	struct Str* lines
);

static Vec(struct erw_VarDeclr*) erw_checkblock(
	struct erw_Scope* scope,
	struct Str* lines,
	int allowsetimmut
);

static struct erw_Type* erw_getexprtype(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct Str* lines,
	struct erw_Type* hint)
{
	log_assert(scope, "is NULL");
	log_assert(node, "is NULL");
	log_assert(lines, "is NULL");

	struct erw_Type* ret = NULL;
	if(node->type == erw_ASTNODETYPE_CAST)
	{
		//TODO: Check if types are cast compatible
		ret = erw_type_create(node->cast.type, scope, lines); 
		struct erw_Type* type = erw_getexprtype(
			scope, 
			node->cast.expr, 
			lines,
			ret->info == erw_TYPEINFO_ARRAY ? ret->array.type : ret
		);

		if(erw_type_compatible(ret, type))
		{
			struct Str typename1 = erw_type_tostring(ret);
			struct Str typename2 = erw_type_tostring(type);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Unnecessary type cast; types are compatible ('%s' and '%s')",
				typename1.data,
				typename2.data
			);
			erw_error(
				msg.data, 
				lines[node->token->linenum - 1].data,
				node->token->linenum, 
				node->token->column,
				node->token->column + vec_getsize(node->token->text) - 2
			);

			str_dtor(&msg);
			str_dtor(&typename2);
			str_dtor(&typename1);
		}

		node->cast.casttype = ret;
		if(type)
		{
			erw_type_dtor(type);
		}
	}
	else if(node->type == erw_ASTNODETYPE_BUILTINCALL)
	{
		if(node->token->type == erw_TOKENTYPE_KEYWORD_LENGTHOF)
		{
			struct erw_Type* type = erw_getexprtype(
				scope, 
				node->builtincall.param, 
				lines,
				hint
			);

			struct erw_Type* base = erw_type_getbase(type);
			if(base->info != erw_TYPEINFO_SLICE 
				&& base->info != erw_TYPEINFO_ARRAY)
			{
				struct Str typename = erw_type_tostring(type);
				struct Str basename = erw_type_tostring(type);
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Expected slice or array type. Got '%s' ('%s')",
					typename.data,
					basename.data
				);
				erw_error(
					msg.data, 
					lines[node->token->linenum - 1].data,
					node->token->linenum, 
					node->token->column,
					node->token->column + vec_getsize(node->token->text) - 2
				);

				str_dtor(&msg);
				str_dtor(&basename);
				str_dtor(&typename);
			}

			node->builtincall.builtintype = base;
			ret = erw_type_new(erw_TYPEINFO_INT, NULL);
			ret->int_.size = 2; //Temporary
		}
		else if(node->token->type == erw_TOKENTYPE_KEYWORD_SIZEOF)
		{
			ret = erw_type_new(erw_TYPEINFO_INT, NULL);
			ret->int_.size = 1; //Temporary
			ret->int_.issigned = 1;
		}
		else if(node->token->type == erw_TOKENTYPE_KEYWORD_ENUMVALUE)
		{
			struct erw_Type* type = erw_getexprtype(
				scope, 
				node->builtincall.param, 
				lines,
				hint
			);

			struct erw_Type* base = erw_type_getbase(type);
			if(base->info != erw_TYPEINFO_ENUM)
			{
				struct Str typename = erw_type_tostring(type);
				struct Str basename = erw_type_tostring(base);
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Expected enum type. Got '%s' ('%s')",
					typename.data,
					basename.data
				);
				erw_error(
					msg.data, 
					lines[node->token->linenum - 1].data,
					node->token->linenum, 
					node->token->column,
					node->token->column + vec_getsize(node->token->text) - 2
				);

				str_dtor(&msg);
				str_dtor(&basename);
				str_dtor(&typename);
			}

			if(base->enum_.type)
			{
				ret = base->enum_.type;
			}
			else
			{
				ret = erw_type_builtins[erw_TYPEBUILTIN_INT32];
			}
		}
		else if(node->token->type == erw_TOKENTYPE_KEYWORD_ENUMNAME)
		{
			struct erw_Type* type = erw_getexprtype(
				scope, 
				node->builtincall.param, 
				lines,
				hint
			);

			struct erw_Type* base = erw_type_getbase(type);
			if(base->info != erw_TYPEINFO_ENUM)
			{
				struct Str typename = erw_type_tostring(type);
				struct Str basename = erw_type_tostring(type);
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Non-enum type expected. Got '%s' ('%s')",
					typename.data,
					basename.data
				);
				erw_error(
					msg.data, 
					lines[node->token->linenum - 1].data,
					node->token->linenum, 
					node->token->column,
					node->token->column + vec_getsize(node->token->text) - 2
				);

				str_dtor(&msg);
				str_dtor(&basename);
				str_dtor(&typename);
			}

			ret = erw_type_new(erw_TYPEINFO_SLICE, NULL);
			ret->slice.size = sizeof(void*);
			ret->slice.ismut = 0;
			ret->slice.type = erw_type_new(erw_TYPEINFO_CHAR, ret);
		}
		else
		{
			log_assert(0, "Unhandled type: %s", node->token->type->name);
		}
	}
	else if(node->type == erw_ASTNODETYPE_BINEXPR)
	{
		if(node->binexpr.result)
		{
			int inttype = erw_getintliteraltype(node->binexpr.result);
			if(inttype & erw_INTLITERALTYPE_INT8)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 1;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT8)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_INT16)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 2;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT16)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 2;
			}
			else if(inttype & erw_INTLITERALTYPE_INT32)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 4;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT32)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 4;
			}
			else if(inttype & erw_INTLITERALTYPE_INT64)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 8;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT64)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 8;
			}
			else //BigInt
			{
				ret = erw_type_new(erw_TYPEINFO_BIGINT, NULL);
				ret->size = sizeof(void*);
			}
		}
		else if(node->token->type == erw_TOKENTYPE_OPERATOR_ACCESS)
		{
			if(node->binexpr.expr1->type == erw_ASTNODETYPE_TYPE)
				//Access module
			{
				struct erw_Module* module = erw_scope_getmodule(
					scope, 
					node->binexpr.expr1->token,
					lines
				);

				struct erw_FuncDeclr* func = erw_scope_getfuncdeclr(
					module->result.scope, 
					node->binexpr.expr2->token,
					lines
				);

				ret = func->type;
			}
			else
			{
				ret = erw_getaccesstype(scope, node, lines);
			}
		}
		else
		{
			struct erw_Type* typesym1 = erw_getexprtype(
				scope, 
				node->binexpr.expr1, 
				lines,
				hint
			);

			struct erw_Type* typesym2 = erw_getexprtype(
				scope, 
				node->binexpr.expr2, 
				lines,
				hint
			);

			struct erw_ASTNode* firstnode = erw_getfirstnode(
				node->binexpr.expr1
			);

			struct erw_ASTNode* lastnode = erw_getlastnode(node->binexpr.expr2);
			if(!erw_type_compatible(typesym1, typesym2))
			{
				struct Str typename1 = erw_type_tostring(typesym1);
				struct Str typename2 = erw_type_tostring(typesym2);
				struct Str msg;
				str_ctorfmt(
					&msg,
					"%s expected type '%s', got incompatible type '%s'",
					node->token->type->name,
					typename1.data,
					typename2.data
				);
				erw_error(
					msg.data, 
					lines[firstnode->token->linenum - 1].data,
					firstnode->token->linenum, 
					lastnode->token->column,
					(lastnode->token->linenum == firstnode->token->linenum) 
						? (size_t)(lastnode->token->column + vec_getsize(
								lastnode->token->text)) - 2
						: lines[firstnode->token->linenum - 1].len
				);
				str_dtor(&msg);
				str_dtor(&typename2);
				str_dtor(&typename1);
			}

			if(node->token->type == erw_TOKENTYPE_OPERATOR_LESS 
				|| node->token->type == erw_TOKENTYPE_OPERATOR_LESSOREQUAL 
				|| node->token->type == erw_TOKENTYPE_OPERATOR_GREATER 
				|| node->token->type == erw_TOKENTYPE_OPERATOR_GREATEROREQUAL)
			{
				ret = erw_type_builtins[erw_TYPEBUILTIN_BOOL];
				erw_checknumerical(typesym1, firstnode, lastnode, lines);
			}
			else if(node->token->type == erw_TOKENTYPE_OPERATOR_EQUAL 
				|| node->token->type == erw_TOKENTYPE_OPERATOR_NOTEQUAL)
			{
				ret = erw_type_builtins[erw_TYPEBUILTIN_BOOL];
			}
			else if(node->token->type == erw_TOKENTYPE_OPERATOR_OR 
				|| node->token->type == erw_TOKENTYPE_OPERATOR_AND)
			{
				ret = erw_type_builtins[erw_TYPEBUILTIN_BOOL];
				erw_checkboolean(typesym1, firstnode, firstnode, lines);
				erw_checkboolean(typesym2, lastnode, lastnode, lines);
			}
			else
			{
				ret = typesym1;
				erw_checknumerical(ret, firstnode, lastnode, lines);
			}
		}
	}
	else if(node->type == erw_ASTNODETYPE_UNEXPR)
	{
		struct erw_ASTNode* firstnode = erw_getfirstnode(node->unexpr.expr);
		struct erw_ASTNode* lastnode = erw_getlastnode(node->unexpr.expr);
		if(node->token->type == erw_TOKENTYPE_OPERATOR_BITAND)
		{
			if(node->unexpr.left)
			{
				ret = erw_type_new(erw_TYPEINFO_REFERENCE, NULL);
				ret->reference.ismut = node->unexpr.mutable;
				ret->reference.size = sizeof(void*); //NOTE: Temporary
				ret->reference.type = erw_getexprtype(
					scope, 
					node->unexpr.expr, 
					lines,
					(hint->info == erw_TYPEINFO_REFERENCE 
						|| hint->info == erw_TYPEINFO_SLICE) 
							? hint->reference.type
							: hint
				);

				if(hint->info == erw_TYPEINFO_SLICE)
				{
					node->unexpr.literaltype = hint;
				}

				if(ret->reference.ismut)
				{
					struct erw_VarDeclr* var = NULL;
					int sliceismut = 0;
					if(node->unexpr.expr->token
						&& node->unexpr.expr->token->type 
							== erw_TOKENTYPE_IDENT)
					{
						var = erw_scope_getvardeclr(
							scope, 
							node->unexpr.expr->token, 
							lines
						);
					}
					else if(node->unexpr.expr->type == erw_ASTNODETYPE_BINEXPR)
					{
						var = erw_scope_getvardeclr(
							scope, 
							erw_getfirstnode(node->unexpr.expr)->token, 
							lines
						);
					}
					else if(node->unexpr.expr->type == erw_ASTNODETYPE_ACCESS)
					{
						var = erw_scope_getvardeclr(
							scope, 
							erw_getfirstnode(node->unexpr.expr->access.expr)
								->token, 
							lines
						);

						if(var->type->info == erw_TYPEINFO_SLICE)
						{
							sliceismut = var->type->slice.ismut;
						}
					}

					if(var && !var->ismut && !sliceismut)
					{
						struct Str msg;
						str_ctorfmt(
							&msg, 
							"Cannot take mutable reference of immutable"
								" variable ('%s') declared at line %zu,"
								" column %zu",
							var->node->vardeclr.name->text,
							var->node->vardeclr.name->linenum,
							var->node->vardeclr.name->column
						);
						erw_error(
							msg.data, 
							lines[firstnode->token->linenum - 1].data,
							firstnode->token->linenum, 
							firstnode->token->column,
							(lastnode->token->linenum == 
								firstnode->token->linenum) 
								? (size_t)(lastnode->token->column 
									+ vec_getsize(
										lastnode->token->text)
									) - 2
								: lines[firstnode->token->linenum - 1]
									.len
						);
						str_dtor(&msg);
					}
				}
			}
			else
			{
				ret = erw_getexprtype(scope, node->unexpr.expr, lines, hint);
				if(ret->info != erw_TYPEINFO_REFERENCE)
				{
					struct Str str = erw_type_tostring(ret);
					struct Str msg;
					str_ctorfmt(
						&msg, 
						"Trying to dereference a non-reference type (%s)",
						str.data
					);
					erw_error(
						msg.data, 
						lines[firstnode->token->linenum - 1].data,
						firstnode->token->linenum, 
						firstnode->token->column,
						(lastnode->token->linenum == firstnode->token->linenum) 
							? (size_t)(lastnode->token->column + vec_getsize(
									lastnode->token->text)) - 2
							: lines[firstnode->token->linenum - 1].len
					);
					str_dtor(&msg);
					str_dtor(&str);
				}

				ret = ret->reference.type;
			}
		}
		else
		{
			ret = erw_getexprtype(scope, node->unexpr.expr, lines, hint);
			if(node->token->type == erw_TOKENTYPE_OPERATOR_NOT)
			{
				erw_checkboolean(ret, firstnode, lastnode, lines);
			}
			else if(node->token->type == erw_TOKENTYPE_OPERATOR_SUB)
			{
				erw_checknumerical(ret, firstnode, lastnode, lines);
			}
			else
			{
				log_assert(0, "Unhandled type: %s", node->token->type->name);
			}
		}
	}
	else if(node->type == erw_ASTNODETYPE_ACCESS)
	{
		struct erw_ASTNode* firstnode = erw_getfirstnode(node->access.expr);
		struct erw_ASTNode* lastnode = erw_getlastnode(node->access.expr);
		struct erw_Type* type = erw_getexprtype(
			scope,
			node->access.expr,
			lines,
			hint
		);

		if(type->info != erw_TYPEINFO_ARRAY && type->info != erw_TYPEINFO_SLICE)
		{
			struct Str str = erw_type_tostring(type);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Trying to access a non-array/slice type (%s)",
				str.data
			);
			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum == firstnode->token->linenum) 
					? (size_t)(lastnode->token->column + vec_getsize(
							lastnode->token->text)) - 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
			str_dtor(&str);
		}

		ret = type->array.type;
	}
	else if(node->type == erw_ASTNODETYPE_FUNCCALL)
	{
		erw_checkfunccall(scope, node, lines);
		if(node->funccall.callee->type == erw_ASTNODETYPE_LITERAL)
		{
			if(erw_scope_findvardeclr(scope, node->funccall.callee->token->text))
			{
				ret = erw_getexprtype(
					scope, 
					node->funccall.callee, 
					lines,
					hint
				);

				ret = ret->func.rettype;
			}
			else
			{
				struct erw_FuncDeclr* func = erw_scope_getfuncdeclr(
					scope, 
					node->funccall.callee->token,
					lines
				);

				ret = func->rettype;
			}
		}
		else
		{
			if(node->funccall.callee->binexpr.expr1->type 
				== erw_ASTNODETYPE_TYPE)
			{
				struct erw_Module* module = erw_scope_getmodule(
					scope, 
					node->funccall.callee->binexpr.expr1->token,
					lines
				);

				struct erw_FuncDeclr* func = erw_scope_getfuncdeclr(
					module->result.scope, 
					node->funccall.callee->binexpr.expr2->token,
					lines
				);

				ret = func->rettype;
			}
			else
			{
				struct erw_Type* newtype = erw_getaccesstype(
					scope, 
					node->funccall.callee, 
					lines
				);

				ret = newtype->func.rettype;
			}
		}

		if(!ret)
		{
			struct erw_ASTNode* firstnode = erw_getfirstnode(
				node->funccall.callee
			);

			struct erw_ASTNode* lastnode = erw_getlastnode(
				node->funccall.callee
			);

			struct Str msg;
			str_ctor(&msg, "Void function used in expression");
			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum == firstnode->token->linenum) 
					? (size_t)(lastnode->token->column + vec_getsize(
							lastnode->token->text)) - 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
		}
	}
	else if(node->type == erw_ASTNODETYPE_LITERAL)
	{
		if(node->token->type == erw_TOKENTYPE_LITERAL_BOOL)
		{
			ret = erw_type_new(erw_TYPEINFO_BOOL, NULL);
			ret->size = 1;
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_INT)
		{
			int inttype = erw_getintliteraltype(node->token->text);
			if(inttype & erw_INTLITERALTYPE_INT8)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 1;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT8)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_INT16)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 2;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT16)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 2;
			}
			else if(inttype & erw_INTLITERALTYPE_INT32)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 4;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT32)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 4;
			}
			else if(inttype & erw_INTLITERALTYPE_INT64)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 8;
				ret->int_.issigned = 1;
			}
			else if(inttype & erw_INTLITERALTYPE_UINT64)
			{
				ret = erw_type_new(erw_TYPEINFO_INT, NULL);
				ret->int_.size = 8;
			}
			else //BigInt
			{
				ret = erw_type_new(erw_TYPEINFO_BIGINT, NULL);
				ret->size = sizeof(void*);
			}
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_FLOAT)
		{
			ret = erw_type_new(erw_TYPEINFO_FLOAT, NULL);
			ret->size = sizeof(float);
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_CHAR)
		{
			ret = erw_type_new(erw_TYPEINFO_CHAR, NULL);
			ret->size = 1;
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_STRING)
		{
			ret = erw_type_new(erw_TYPEINFO_SLICE, NULL);
			ret->slice.size = sizeof(void*);
			ret->slice.ismut = 0; //Should this be mutable?
			ret->slice.type = erw_type_new(erw_TYPEINFO_CHAR, ret);
		}
		else if(node->token->type == erw_TOKENTYPE_IDENT)
		{
			struct erw_VarDeclr* var = erw_scope_findvardeclr(
				scope, 
				node->token->text
			);

			struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
				scope, 
				node->token->text,
				1
			);

			if(!var && !func)
			{ 
				struct Str msg;
				str_ctor(
					&msg,
					"Undefined identifier"
				);
				erw_error(
					msg.data, 
					lines[node->token->linenum - 1].data, 
					node->token->linenum, 
					node->token->column,
					node->token->column + vec_getsize(node->token->text) - 2
				);
				str_dtor(&msg);
			}
			else if(var)
			{
				if(!var->hasvalue)
				{
					struct Str msg;
					str_ctorfmt(
						&msg, 
						"Uninitialized variable used in expression. Declared at"
							" line %zu, column %zu",
						var->node->vardeclr.name->linenum,
						var->node->vardeclr.name->column
					);
					erw_error(
						msg.data, 
						lines[node->token->linenum - 1].data,
						node->token->linenum, 
						node->token->column,
						node->token->column + vec_getsize(node->token->text) - 2
					);
					str_dtor(&msg);
				}

				ret = var->type;
				var->isused = 1;

				if(hint)
				{
					struct erw_Type* type1 = erw_type_getnamed(ret);
					struct erw_Type* type2 = erw_type_getnamed(hint);
					if(type1 && type2)
					{
						struct erw_Type* structtype = erw_type_getbase(type1);
						struct erw_Type* extensiontype = erw_type_getbase(
							type2
						);

						if(structtype->info == erw_TYPEINFO_STRUCT 
							&& extensiontype->info == erw_TYPEINFO_STRUCT 
							&& !erw_type_compare(structtype, extensiontype))
						{
							node->extensiontypes.extensiontype = hint;
							node->extensiontypes.structtype = ret;
						}
					}
				}
			}
			else //func
			{
				ret = func->type;
				func->isused = 1;
			}
		}
		else
		{
			log_assert(
				0,
				"this shouldn't happen: %s (%s) (%zu, %zu)", 
				node->token->text, 
				node->token->type->name,
				node->token->linenum,
				node->token->column
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_STRUCTLITERAL)
	{
		struct erw_Type* base = erw_type_getbase(hint);
		if(base->info != erw_TYPEINFO_STRUCT)
		{
			struct Str typename = erw_type_tostring(hint);
			struct Str basename = erw_type_tostring(base);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Unable to deduce type '%s' ('%s') from '%s'",
				typename.data,
				basename.data,
				node->type->name
			);
			
			erw_error(
				msg.data, 
				lines[node->token->linenum - 1].data,
				node->token->linenum,
				node->token->column,
				node->token->column + vec_getsize(node->token->text) - 2
			);

			str_dtor(&msg);
			str_dtor(&basename);
			str_dtor(&typename);
		}

		Vec(struct erw_Token*) members = vec_ctor(struct erw_Token*, 0);
		for(size_t i = 0; i < vec_getsize(node->structliteral.names); i++)
		{
			int found = 0;
			for(size_t j = 0; j < vec_getsize(base->struct_.members); j++)
			{
				if(!strcmp(
					node->structliteral.names[i]->text, 
					base->struct_.members[j].name))
				{
					for(size_t k = 0; k < vec_getsize(members); k++)
					{
						if(!strcmp(
							members[k]->text, 
							node->structliteral.names[i]->text))
						{
							struct Str msg;
							str_ctorfmt(
								&msg, 
								"Member '%s' has already been initialized"
									" at line %zu, column %zu", 
								members[k]->text,
								members[k]->linenum,
								members[k]->column
							);

							erw_error(
								msg.data, 
								lines[
									node->structliteral.names[i]->linenum 
										- 1
								].data,
								node->structliteral.names[i]->linenum, 
								node->structliteral.names[i]->column,
								node->structliteral.names[i]->column 
									+ vec_getsize(
										node->structliteral.names[i]->text
									) - 2
							);

							str_dtor(&msg);
						}
					}

					erw_checkexprtype(
						scope, 
						node->structliteral.values[i], 
						base->struct_.members[j].type,
						lines
					);

					found = 1;
					vec_pushback(members, node->structliteral.names[i]);
					break;
				}
			}
			
			if(!found)
			{
				struct Str typename = erw_type_tostring(hint);
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Struct '%s' has no member named '%s'", 
					typename.data,
					node->structliteral.names[i]->text
				);

				erw_error(
					msg.data, 
					lines[node->structliteral.names[i]->linenum - 1].data,
					node->structliteral.names[i]->linenum, 
					node->structliteral.names[i]->column,
					node->structliteral.names[i]->column 
						+ vec_getsize(node->structliteral.names[i]->text) 
						- 2
				);

				str_dtor(&msg);
				str_dtor(&typename);
			}
		}

		size_t nummembers = vec_getsize(members);
		size_t numstructmembers = vec_getsize(base->struct_.members);
		if(nummembers < numstructmembers)
		{
			struct Str typename = erw_type_tostring(hint);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Struct '%s' has %zu members. Literal only has %zu members",
				typename.data,
				numstructmembers,
				nummembers
			);

			for(size_t i = 0; i < numstructmembers; i++)
			{
				int found = 0;
				for(size_t j = 0; j < nummembers; j++)
				{
					if(!strcmp(
						members[j]->text, 
						base->struct_.members[i].name))
					{
						found = 1;
						break;
					}
				}

				if(!found)
				{
					str_appendfmt(
						&msg, 
						" Missing member '%s'.", 
						base->struct_.members[i].name
					);
				}
			}

			erw_error(
				msg.data, 
				lines[node->token->linenum - 1].data,
				node->token->linenum,
				node->token->column,
				node->token->column + vec_getsize(node->token->text) - 2
			);

			str_dtor(&msg);
			str_dtor(&typename);
		}

		ret = hint;
		node->structliteral.literaltype = ret;
	}
	else if(node->type == erw_ASTNODETYPE_UNIONLITERAL)
	{
		struct erw_Type* base = erw_type_getbase(hint);
		if(base->info != erw_TYPEINFO_UNION)
		{
			struct Str typename = erw_type_tostring(hint);
			struct Str basename = erw_type_tostring(base);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Unable to deduce type '%s' ('%s') from '%s'",
				typename.data,
				basename.data,
				node->type->name
			);
			
			erw_error(
				msg.data, 
				lines[node->token->linenum - 1].data,
				node->token->linenum,
				node->token->column,
				node->token->column + vec_getsize(node->token->text) - 2
			);

			str_dtor(&msg);
			str_dtor(&basename);
			str_dtor(&typename);
		}

		struct erw_Type* literaltype = erw_type_create(
			node->unionliteral.type, 
			scope, 
			lines
		);

		int found = 0;
		for(size_t i = 0; i < vec_getsize(base->union_.members); i++)
		{
			if(erw_type_compare(base->union_.members[i], literaltype))
			{ 
				if(node->unionliteral.value)
				{
					erw_checkexprtype(
						scope, 
						node->unionliteral.value, 
						literaltype,
						lines
					);
				}
				else
				{
					struct erw_Type* basetype = erw_type_getbase(
						base->union_.members[i]
					);

					if(basetype->info != erw_TYPEINFO_EMPTY)
					{
						struct Str name = erw_type_tostring(
							base->union_.members[i]
						);
						struct Str msg;
						str_ctorfmt(
							&msg, 
							"Expected value for type '%s' (type not empty)", 
							name.data
						);

						erw_error(
							msg.data, 
							lines[
								node->unionliteral.type->token->linenum - 1
							].data,
							node->unionliteral.type->token->linenum,
							node->unionliteral.type->token->column,
							node->unionliteral.type->token->column 
								+ vec_getsize(
									node->unionliteral.type->token->text
								) - 2
						);

						str_dtor(&msg);
						str_dtor(&name);
					}
				}

				found = 1;
				break;
			}
		}

		if(!found)
		{
			struct Str literalname = erw_type_tostring(literaltype);
			struct Str typename = erw_type_tostring(hint);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Union '%s' has no '%s' type", 
				typename.data,
				literalname.data
			);

			erw_error(
				msg.data, 
				lines[node->token->linenum - 1].data,
				node->token->linenum,
				node->token->column,
				node->token->column + vec_getsize(node->token->text) - 2
			);

			str_dtor(&msg);
			str_dtor(&typename);
			str_dtor(&literalname);
		}

		ret = hint;
		node->unionliteral.literaltype = ret;
		node->unionliteral.uniontype = literaltype;
	}
	else if(node->type == erw_ASTNODETYPE_ARRAYLITERAL)
	{
		size_t literalnum = vec_getsize(node->arrayliteral.values);
		for(size_t i = 0; i < literalnum; i++)
		{
			erw_checkexprtype(
				scope, 
				node->arrayliteral.values[i], 
				hint,
				lines
			);
		}

		ret = erw_type_new(erw_TYPEINFO_ARRAY, NULL);
		ret->array.type = hint;
		if(node->arrayliteral.length)
		{
			size_t value = 0;
			sscanf( //XXX
				node->arrayliteral.length->token->text, 
				"%zu", 
				&value
			);

			ret->array.numelements = value;
		}
		else
		{
			ret->array.numelements = literalnum;
		}

		node->arrayliteral.literaltype = ret;
	}
	else if(node->type == erw_ASTNODETYPE_ENUMLITERAL)
	{
		struct erw_Type* base = erw_type_getbase(hint);
		if(base->info != erw_TYPEINFO_ENUM)
		{
			struct Str typename = erw_type_tostring(hint);
			struct Str basename = erw_type_tostring(base);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Unable to deduce type '%s' ('%s') from '%s'",
				typename.data,
				basename.data,
				node->type->name
			);
			
			erw_error(
				msg.data, 
				lines[node->token->linenum - 1].data,
				node->token->linenum,
				node->token->column,
				node->token->column + vec_getsize(node->token->text) - 2
			);

			str_dtor(&msg);
			str_dtor(&basename);
			str_dtor(&typename);
		}

		int found = 0;
		for(size_t i = 0; i < vec_getsize(base->enum_.members); i++)
		{
			if(!strcmp(
				base->enum_.members[i].name, 
				node->enumliteral.name->text))
			{
				found = 1;
				break;
			}
		}

		if(!found)
		{
			struct Str typename = erw_type_tostring(hint);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Enum '%s' does not have a member named '%s'",
				typename.data,
				node->enumliteral.name->text
			);

			erw_error(
				msg.data, 
				lines[node->enumliteral.name->linenum - 1].data,
				node->enumliteral.name->linenum, 
				node->enumliteral.name->column,
				node->enumliteral.name->column +
					vec_getsize(node->enumliteral.name->text) - 2
			);
			str_dtor(&msg);
		}

		ret = hint;
		node->enumliteral.literaltype = ret;
	}
	else if(node->type == erw_ASTNODETYPE_FUNCLITERAL)
	{
		ret = erw_type_new(erw_TYPEINFO_FUNC, NULL);
		ret->func.size = sizeof(void(*)(void));
		if(node->funcliteral.type)
		{
			ret->func.rettype = erw_type_create(
				node->funcliteral.type, 
				scope, 
				lines
			);
		}

		struct erw_Scope* newscope = erw_scope_new(
			scope, 
			scope->name, 
			0, 
			node->funcliteral.block
		);

		for(size_t i = 0; i < vec_getsize(node->funcliteral.params); i++)
		{
			struct erw_VarDeclr* param = erw_scope_addvardeclr(
				newscope, 
				node->funcliteral.params[i], 
				lines
			);

			param->hasvalue = 1;
			vec_pushback(ret->func.params, param->type);
		}

		node->funcliteral.functype = ret;
		Vec(struct erw_VarDeclr*) changedvars = erw_checkblock(
			newscope, 
			lines, 
			1
		);

		vec_dtor(changedvars);
	}
	else if(node->type == erw_ASTNODETYPE_TEMPLATECALL)
	{
		if(node->templatecall.body->funccall.callee->type 
			== erw_ASTNODETYPE_LITERAL)
		{
			struct erw_Token* name = erw_callfunctemplate(scope, node, lines);
			erw_checkfunccall(scope, node->templatecall.body, lines);
			struct erw_FuncDeclr* funcdeclr = erw_scope_getfuncdeclr(
				scope, 
				name, 
				lines
			);

			ret = funcdeclr->rettype;
		}
		else
		{
			struct erw_Module* module = erw_scope_getmodule(
				scope, 
				node->templatecall.body->funccall.callee->binexpr.expr1->token,
				lines
			);
			
			struct erw_Token* name = erw_callfunctemplate(
				module->result.scope, 
				node, 
				lines
			);

			erw_checkfunccall(scope, node->templatecall.body, lines);
			struct erw_FuncDeclr* funcdeclr = erw_scope_getfuncdeclr(
				module->result.scope, 
				name,
				lines
			);

			ret = funcdeclr->rettype;
		}

		if(!ret)
		{
			struct erw_ASTNode* firstnode = erw_getfirstnode(
				node->funccall.callee
			);

			struct erw_ASTNode* lastnode = erw_getlastnode(
				node->funccall.callee
			);

			struct Str msg;
			str_ctor(&msg, "Void function used in expression");
			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum == firstnode->token->linenum) 
					? (size_t)(lastnode->token->column + vec_getsize(
							lastnode->token->text)) - 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
		}
	}
	else
	{
		log_assert(0, "Unhandled type: %s", node->type->name);
	}

	return ret;
}

void erw_checkexprtype( //Used in erw_type.c
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct erw_Type* type,
	struct Str* lines)
{
	log_assert(scope, "is NULL");
	log_assert(node, "is NULL");
	log_assert(type, "is NULL");
	log_assert(lines, "is NULL");

	struct erw_Type* type2 = erw_getexprtype(
		scope, 
		node, 
		lines, 
		type->info == erw_TYPEINFO_ARRAY ? type->array.type : type
	);

	if(!erw_type_compatible(type, type2))
	{
		struct erw_ASTNode* firstnode = erw_getfirstnode(node);
		struct erw_ASTNode* lastnode = erw_getlastnode(node);

		struct Str typestr = erw_type_tostring(type);
		struct Str type2str = erw_type_tostring(type2);
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Expected expression of type '%s', got expression of type '%s'",
			typestr.data,
			type2str.data
		);

		erw_error(
			msg.data, 
			lines[firstnode->token->linenum - 1].data,
			firstnode->token->linenum, 
			firstnode->token->column,
			(lastnode->token->linenum == firstnode->token->linenum) 
				? (size_t)(lastnode->token->column + vec_getsize(
						lastnode->token->text)) - 2
				: lines[firstnode->token->linenum - 1].len
		);
		str_dtor(&msg);
	}
}

static void erw_checkfunccall(
	struct erw_Scope* scope,
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(scope, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_FUNCCALL, 
		"invalid type (%s)",
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_ASTNode* firstnode = erw_getfirstnode(node->funccall.callee);
	struct erw_ASTNode* lastnode = erw_getlastnode(node->funccall.callee);

	struct erw_Type* type;
	size_t numparams = 0;
	int isvariadic = 0;
	int isforeign = 0;

	if(erw_scope_findvardeclr(scope, node->funccall.callee->token->text)
		|| node->funccall.callee->type == erw_ASTNODETYPE_BINEXPR)
	{
		type = erw_getexprtype(scope, node->funccall.callee, lines, NULL);
		if(type->info != erw_TYPEINFO_FUNC)
		{
			struct Str str = erw_type_tostring(type);
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Attempt to call non-function type ('%s')",
				str.data
			);
			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum == firstnode->token->linenum) 
					? (size_t)(lastnode->token->column + vec_getsize(
							lastnode->token->text)) - 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
			str_dtor(&str);
		}

		numparams = vec_getsize(type->func.params);
		isvariadic = type->func.isvariadic;
		isforeign = type->func.isforeign;
	}
	else
	{
		struct erw_FuncDeclr* func = erw_scope_getfuncdeclr(
			scope, 
			node->funccall.callee->token, 
			lines
		);

		if(!strcmp(scope->name, node->funccall.callee->token->text)) 
			//Don't flag as used if no extern function calls it
		{
			func->isused = 0;
		}

		numparams = vec_getsize(func->type->func.params);
		isvariadic = func->isvariadic;
		isforeign = func->isforeign;
		type = func->type;
	}

	if(isvariadic)
	{
		if(vec_getsize(node->funccall.args) < numparams - 1)
		{
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Incorrect number of arguments in function call. Expected %zu,"
					" got %zu", 
				numparams,
				vec_getsize(node->funccall.args)
			);

			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum == firstnode->token->linenum) 
					? (size_t)(lastnode->token->column + vec_getsize(
							lastnode->token->text)) - 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
		}

		if(isforeign)
		{
			for(size_t i = 0; i < numparams; i++)
			{
				erw_checkexprtype(
					scope, 
					node->funccall.args[i], 
					type->func.params[i],
					lines
				);
			}
		}
		else
		{
			for(size_t i = 0; i < numparams - 1; i++)
			{
				erw_checkexprtype(
					scope, 
					node->funccall.args[i], 
					type->func.params[i],
					lines
				);
			}

			struct erw_Type* checktype = type->func.params[numparams - 1]
				->slice.type;
			for(size_t i = numparams - 1; 
				i < vec_getsize(node->funccall.args); 
				i++)
			{
				erw_checkexprtype(
					scope, 
					node->funccall.args[i], 
					checktype,
					lines
				);
			}
		}
	}
	else
	{
		if(vec_getsize(node->funccall.args) != numparams)
		{
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Incorrect number of arguments in function call. Expected %zu,"
					" got %zu", 
				numparams,
				vec_getsize(node->funccall.args)
			);

			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum == firstnode->token->linenum) 
					? (size_t)(lastnode->token->column + vec_getsize(
							lastnode->token->text)) - 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
		}

		for(size_t i = 0; i < numparams; i++)
		{
			erw_checkexprtype(
				scope, 
				node->funccall.args[i], 
				type->func.params[i],
				lines
			);
		}
	}
}

static int erw_checkmut(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(scope, "is NULL");
	log_assert(node, "is NULL");
	log_assert(lines, "is NULL");

	if(node->type == erw_ASTNODETYPE_LITERAL)
	{
		struct erw_VarDeclr* var = erw_scope_getvardeclr(
			scope,
			node->token,
			lines
		);

		return var->ismut;
	}
	else if(node->type == erw_ASTNODETYPE_BINEXPR)
	{
		return erw_checkmut(scope, node->binexpr.expr1, lines);
	}
	else if(node->type == erw_ASTNODETYPE_ACCESS)
	{
		struct erw_Type* accesstype = erw_getexprtype(
			scope, 
			node->access.expr,
			lines,
			NULL
		);

		if(accesstype-> info == erw_TYPEINFO_ARRAY)
		{
			return erw_checkmut(scope, node->access.expr, lines);
		}
		else //Slice
		{
			return accesstype->slice.ismut;
		}
	}
	else if(node->type == erw_ASTNODETYPE_UNEXPR)
	{
		struct erw_Type* accesstype = erw_getexprtype(
			scope, 
			node->unexpr.expr,
			lines,
			NULL
		);

		return accesstype->reference.ismut;
	}
	else
	{
		log_assert(0, "this shouldn't happen");
		return 0;
	}
}

static Vec(struct erw_VarDeclr*) erw_checkassignment(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node,
	struct Str* lines,
	int allowsetimmut)
{
	log_assert(scope, "is NULL");
	log_assert(node, "is NULL");
	log_assert(node->type == erw_ASTNODETYPE_ASSIGNMENT, "invalid type");
	log_assert(lines, "is NULL");

	Vec(struct erw_VarDeclr*) changedvars = vec_ctor(struct erw_VarDeclr*, 0);
	struct erw_ASTNode* firstnode = erw_getfirstnode(node->assignment.assignee);
	struct erw_ASTNode* lastnode = erw_getlastnode(node->assignment.assignee);
	struct erw_ASTNode* varnode;
	if(node->assignment.assignee->type == erw_ASTNODETYPE_LITERAL)
	{ 
		varnode = node->assignment.assignee;
	}
	else if(node->assignment.assignee->type == erw_ASTNODETYPE_BINEXPR) 
		//Struct access
	{
		varnode = erw_getfirstnode(node->assignment.assignee);
	}
	else if(node->assignment.assignee->type == erw_ASTNODETYPE_ACCESS) 
		//Array access
	{
		varnode = erw_getfirstnode(node->assignment.assignee->access.expr);
	}
	else if(node->assignment.assignee->type == erw_ASTNODETYPE_UNEXPR) 
		//Dereferencing
	{
		varnode = erw_getfirstnode(node->assignment.assignee->unexpr.expr);
	}
	else
	{
		log_assert(0, "this shouldn't happen'");
		abort(); //Remove warning
	}

	struct erw_VarDeclr* var = erw_scope_getvardeclr(
		scope,
		varnode->token,
		lines
	);

	int islocal = 0;
	for(size_t j = 0; j < vec_getsize(scope->variables); j++)
	{
		if(!strcmp(scope->variables[j].name, var->name))
		{
			islocal = 1;
			break;
		}
	}
	
	int ismut = erw_checkmut(scope, node->assignment.assignee, lines);
	struct erw_Type* accesstype;
	if(node->assignment.assignee->type == erw_ASTNODETYPE_LITERAL)
	{ 
		accesstype = var->type;
	}
	else if(node->assignment.assignee->type == erw_ASTNODETYPE_BINEXPR) 
		//Struct access
	{
		accesstype = erw_getexprtype(
			scope,
			node->assignment.assignee,
			lines,
			NULL
		);
	}
	else if(node->assignment.assignee->type == erw_ASTNODETYPE_ACCESS) 
		//Array access
	{
		accesstype = erw_getexprtype(
			scope, 
			node->assignment.assignee->access.expr,
			lines,
			NULL
		)->slice.type;
	}
	else if(node->assignment.assignee->type == erw_ASTNODETYPE_UNEXPR) 
		//Dereferencing
	{
		accesstype = erw_getexprtype(
			scope, 
			node->assignment.assignee->unexpr.expr,
			lines,
			NULL
		)->reference.type;
	}
	else
	{
		log_assert(0, "this shouldn't happen");
		accesstype = NULL;
	}

	if(node->token->type != erw_TOKENTYPE_OPERATOR_ASSIGN)
	{
		if(!var->hasvalue)
		{
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Operation '%s' is not allowed on an"
					" uninitialized variable, declared at line %zu,"
					" column %zu",
				node->token->text,
				var->node->vardeclr.name->linenum, 
				var->node->vardeclr.name->column
			);

			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum 
					== firstnode->token->linenum) 
					? (size_t)(lastnode->token->column 
						+ vec_getsize(lastnode->token->text)) - 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
		}

		if(!ismut)
		{
			struct Str msg;
			str_ctorfmt(
				&msg, 
				"Operation '%s' is not allowed on an immutable"
					" assignee, declared at line %zu, column %zu",
				node->token->text,
				var->node->vardeclr.name->linenum, 
				var->node->vardeclr.name->column
			);

			erw_error(
				msg.data, 
				lines[firstnode->token->linenum - 1].data,
				firstnode->token->linenum, 
				firstnode->token->column,
				(lastnode->token->linenum 
					== firstnode->token->linenum) 
					? (size_t)(lastnode->token->column 
						+ vec_getsize(lastnode->token->text)) 
						- 2
					: lines[firstnode->token->linenum - 1].len
			);
			str_dtor(&msg);
		}
	}
	else
	{
		if(node->assignment.assignee->type == erw_ASTNODETYPE_LITERAL)
		{ 
			if((var->hasvalue && !ismut) 
				|| (!islocal && !allowsetimmut && !ismut))
			{
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Reassignment of immutable variable, declared at"
						" line %zu, column %zu",
					var->node->vardeclr.name->linenum, 
					var->node->vardeclr.name->column
				);

				erw_error(
					msg.data, 
					lines[firstnode->token->linenum - 1].data,
					firstnode->token->linenum, 
					firstnode->token->column,
					(lastnode->token->linenum == 
						firstnode->token->linenum) 
						? (size_t)(lastnode->token->column 
							+ vec_getsize(lastnode->token->text)) - 2
						: lines[firstnode->token->linenum - 1].len
				);
				str_dtor(&msg);
			}

			var->hasvalue = 1;
			vec_pushback(changedvars, var);
		}
		else if(node->assignment.assignee->type == erw_ASTNODETYPE_BINEXPR) 
			//Struct access
		{
			if(var->hasvalue && !ismut)
			{
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Reassignment of immutable variable member,"
						" declared at line %zu, column %zu",
					var->node->vardeclr.name->linenum, 
					var->node->vardeclr.name->column
				);

				erw_error(
					msg.data, 
					lines[firstnode->token->linenum - 1].data,
					firstnode->token->linenum, 
					firstnode->token->column,
					(lastnode->token->linenum == 
						firstnode->token->linenum) 
						? (size_t)(lastnode->token->column 
							+ vec_getsize(lastnode->token->text)) 
							- 2
						: lines[firstnode->token->linenum - 1].len
				);
				str_dtor(&msg);
			}
		}
		else if(node->assignment.assignee->type == erw_ASTNODETYPE_ACCESS) 
			//Array access
		{
			if(accesstype->info == erw_TYPEINFO_ARRAY)
			{
				if(var->hasvalue && !ismut)
				{
					struct Str msg;
					str_ctorfmt(
						&msg, 
						"Reassignment of element of immutable variable"
							" declared at line %zu, column %zu",
						var->node->vardeclr.name->linenum, 
						var->node->vardeclr.name->column
					);

					erw_error(
						msg.data, 
						lines[firstnode->token->linenum - 1].data,
						firstnode->token->linenum, 
						firstnode->token->column,
						(lastnode->token->linenum == 
							firstnode->token->linenum) 
							? (size_t)(lastnode->token->column 
								+ vec_getsize(lastnode->token->text)) 
								- 2
							: lines[firstnode->token->linenum - 1].len
					);
					str_dtor(&msg);
				}
			}
			else //Slice
			{
				if(!ismut)
				{
					struct Str msg;
					str_ctorfmt(
						&msg, 
						"Assignment to an element of an immutable slice"
							", declared at line %zu, column %zu",
						var->node->vardeclr.name->linenum, 
						var->node->vardeclr.name->column
					);

					erw_error(
						msg.data, 
						lines[firstnode->token->linenum - 1].data,
						firstnode->token->linenum, 
						firstnode->token->column,
						(lastnode->token->linenum == 
							firstnode->token->linenum) 
							? (size_t)(lastnode->token->column 
								+ vec_getsize(lastnode->token->text)) 
								- 2
							: lines[firstnode->token->linenum - 1].len
					);
					str_dtor(&msg);
				}
			}
		}
		else if(node->assignment.assignee->type == erw_ASTNODETYPE_UNEXPR) 
			//Dereferencing
		{
			if(!ismut)
			{
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Assignment to an element of an immutable slice"
						", declared at line %zu, column %zu",
					var->node->vardeclr.name->linenum, 
					var->node->vardeclr.name->column
				);

				erw_error(
					msg.data, 
					lines[firstnode->token->linenum - 1].data,
					firstnode->token->linenum, 
					firstnode->token->column,
					(lastnode->token->linenum == 
						firstnode->token->linenum) 
						? (size_t)(lastnode->token->column 
							+ vec_getsize(lastnode->token->text)) 
							- 2
						: lines[firstnode->token->linenum - 1].len
				);
				str_dtor(&msg);
			}
		}
	}

	erw_checkexprtype(scope, node->assignment.expr, accesstype, lines);
	return changedvars;
}

void erw_checkfuncdeclr(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct Str* lines
);

static Vec(struct erw_VarDeclr*) erw_checkblock(
	struct erw_Scope* scope,
	struct Str* lines,
	int allowsetimmut)
{
	log_assert(scope, "is NULL");
	log_assert(lines, "is NULL");
	
	Vec(struct erw_VarDeclr*) changedvars = vec_ctor(struct erw_VarDeclr*, 0);
	struct erw_ASTNode* blocknode = scope->node;
	for(size_t i = 0; i < vec_getsize(blocknode->block.stmts); i++)
	{
		if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_FUNCDEF)
		{
			erw_checkfuncdeclr(scope, blocknode->block.stmts[i], lines);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_TYPEDECLR)
		{
			erw_scope_addtypedeclr(scope, blocknode->block.stmts[i], lines);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_FOREIGN)
		{
			if(blocknode->block.stmts[i]->foreign.node->type 
				== erw_ASTNODETYPE_FUNCDEF)
			{
				erw_checkfuncdeclr(scope, blocknode->block.stmts[i], lines);
			}
			else if(blocknode->block.stmts[i]->foreign.node->type 
				== erw_ASTNODETYPE_TYPEDECLR)
			{
				erw_scope_addtypedeclr(scope, blocknode->block.stmts[i], lines);
			}
			else
			{
				log_assert(
					0,
					"Unhandled type: %s", 
					blocknode->block.stmts[i]->foreign.node->type->name
				);
			}
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_VARDECLR)
		{
			struct erw_VarDeclr* var = erw_scope_addvardeclr(
				scope, 
				blocknode->block.stmts[i], 
				lines
			);

			if(blocknode->block.stmts[i]->vardeclr.value)
			{
				erw_checkexprtype(
					scope, 
					blocknode->block.stmts[i]->vardeclr.value,
					var->type,
					lines
				);
			}
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_IF)
		{
			struct erw_Type* iftype = erw_getexprtype(
				scope, 
				blocknode->block.stmts[i]->if_.expr, 
				lines,
				NULL
			);
			
			struct erw_ASTNode* firstnode = erw_getfirstnode(
				blocknode->block.stmts[i]->if_.expr
			);

			struct erw_ASTNode* lastnode = erw_getlastnode(
				blocknode->block.stmts[i]->if_.expr
			);

			erw_checkboolean(iftype, firstnode, lastnode, lines);
			struct erw_Scope* newscope = erw_scope_new(
				scope, 
				scope->name,
				0,
				blocknode->block.stmts[i]->if_.block
			);

			Vec(struct erw_VarDeclr*) changedvars_if = erw_checkblock(
				newscope, 
				lines,
				1
			);

			for(size_t j = 0; j < vec_getsize(changedvars_if); j++)
			{
				changedvars_if[j]->hasvalue = 0;
			}

			Vec(Vec(struct erw_VarDeclr*)) changedvars_elseifs = vec_ctor(
				Vec(struct erw_VarDeclr*),
				vec_getsize(blocknode->block.stmts[i]->if_.elseifs)
			);

			Vec(struct erw_VarDeclr*) changedvars_else = NULL;
			for(size_t j = 0; 
				j < vec_getsize(blocknode->block.stmts[i]->if_.elseifs); 
				j++)
			{ 
				struct erw_Type* elseiftype = erw_getexprtype(
					scope,
					blocknode->block.stmts[i]->if_.elseifs[j]->elseif.expr, 
					lines,
					NULL
				);

				firstnode = erw_getfirstnode(
					blocknode->block.stmts[i]->if_.elseifs[j]->elseif.expr
				);

				lastnode = erw_getlastnode(
					blocknode->block.stmts[i]->if_.elseifs[j]->elseif .expr
				);

				erw_checkboolean(elseiftype, firstnode, lastnode, lines);
				erw_type_dtor(elseiftype);
				newscope = erw_scope_new(
					scope, 
					scope->name,
					0,
					blocknode->block.stmts[i]->if_.elseifs[j]->elseif.block
				);

				vec_pushback(
					changedvars_elseifs, 
					erw_checkblock(
						newscope, 
						lines,
						1
					)
				);

				for(size_t k = 0;
					k < vec_getsize(changedvars_elseifs[j]);
					k++)
				{
					changedvars_elseifs[j][k]->hasvalue = 0;
				}
			}

			if(blocknode->block.stmts[i]->if_.else_)
			{ 
				newscope = erw_scope_new(
					scope, 
					scope->name,
					0,
					blocknode->block.stmts[i]->if_.else_->else_.block
				);

				changedvars_else = erw_checkblock(newscope, lines, 1);
			}

			if(!changedvars_else)
			{
				for(size_t j = 0; j < vec_getsize(changedvars_elseifs); j++)
				{
					if(vec_getsize(changedvars_elseifs[j]))
					{
						goto error;
					}
				}

				if(vec_getsize(changedvars_if))
				{
					goto error;
				}
			}
			else
			{
				for(size_t j = 0; j < vec_getsize(changedvars_if); j++)
				{
					size_t elseifsfound = 0;
					for(size_t k = 0; k < vec_getsize(changedvars_elseifs); k++)
					{
						int elseiffound = 0;
						for(size_t l = 0; 
							l < vec_getsize(changedvars_elseifs[k]); 
							l++)
						{
							if(changedvars_if[j] == changedvars_elseifs[k][l])
							{
								elseiffound = 1;
								break;
							}
						}

						if(elseiffound)
						{
							elseifsfound++;
						}
					}

					int elsefound = 0;
					for(size_t k = 0; k < vec_getsize(changedvars_else); k++)
					{
						if(changedvars_if[j] == changedvars_else[k])
						{
							elsefound = 1;
							break;
						}
					}

					if(elseifsfound != vec_getsize(changedvars_elseifs) 
						|| !elsefound)
					{
						goto error;
					}
				}

				for(size_t j = 0; j < vec_getsize(changedvars_elseifs); j++)
				{

					for(size_t k = 0; 
						k < vec_getsize(changedvars_elseifs[j]); 
						k++)
					{
						int iffound = 0;
						for(size_t l = 0; l < vec_getsize(changedvars_if); l++)
						{
							if(changedvars_elseifs[j][k] == changedvars_if[l])
							{
								iffound = 1;
								break;
							}
						}

						int elsefound = 0;
						for(size_t l = 0; 
							l < vec_getsize(changedvars_else); 
							l++)
						{
							if(changedvars_elseifs[j][k] == changedvars_else[l])
							{
								elsefound = 1;
								break;
							}
						}

						if(!iffound || !elsefound)
						{
							goto error;
						}
					}
				}

				for(size_t j = 0; j < vec_getsize(changedvars_else); j++)
				{
					size_t elseifsfound = 0;
					for(size_t k = 0; k < vec_getsize(changedvars_elseifs); k++)
					{
						int elseiffound = 0;
						for(size_t l = 0; 
							l < vec_getsize(changedvars_elseifs[k]); 
							l++)
						{
							if(changedvars_else[j] == changedvars_elseifs[k][l])
							{
								elseiffound = 1;
								break;
							}
						}

						if(elseiffound)
						{
							elseifsfound++;
						}
					}

					int iffound = 0;
					for(size_t k = 0; k < vec_getsize(changedvars_if); k++)
					{
						if(changedvars_else[j] == changedvars_if[k])
						{
							iffound = 1;
							break;
						}
					}

					if(elseifsfound != vec_getsize(changedvars_elseifs) 
						|| !iffound)
					{
						goto error;
					}
				}

				goto done;

			error:;
				struct Str msg;
				//TODO: Improve error message
				str_ctor(
					&msg,
					"Variables may be uninitialized after 'if' branch"
				);

				erw_error(
					msg.data, 
					lines[blocknode->block.stmts[i]->token->linenum - 1]
						.data, 
					blocknode->block.stmts[i]->token->linenum, 
					blocknode->block.stmts[i]->token->column,
					blocknode->block.stmts[i]->token->column + 
						vec_getsize(blocknode->block.stmts[i]->token->text)
						- 2
				);
				str_dtor(&msg);

			done:;
			}

			if(changedvars_else)
			{
				vec_dtor(changedvars_else);
			}

			for(size_t j = 0; j < vec_getsize(changedvars_elseifs); j++)
			{
				vec_dtor(changedvars_elseifs[j]);
			}
			
			vec_dtor(changedvars_elseifs);
			vec_dtor(changedvars_if);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_RETURN)
		{
			struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
				scope, 
				scope->name, 
				0
			);

			if(func->rettype)
			{
				if(!blocknode->block.stmts[i]->return_.expr) 
				{
					struct Str typename = erw_type_tostring(func->type);
					struct Str msg;
					str_ctorfmt(
						&msg,
						"Function ('%s') should return a value of type "
							"'%s'.",
						func->node->funcdef.name->text,
						typename.data
					);

					erw_error(
						msg.data, 
						lines[blocknode->block.stmts[i]->token->linenum - 1]
							.data, 
						blocknode->block.stmts[i]->token->linenum, 
						blocknode->block.stmts[i]->token->column,
						blocknode->block.stmts[i]->token->column + 
							vec_getsize(blocknode->block.stmts[i]->token->text)
							- 2
					);
					str_dtor(&msg);
				}

				erw_checkexprtype(
					scope, 
					blocknode->block.stmts[i]->return_.expr, 
					func->rettype, 
					lines
				);
			}
			else
			{
				if(blocknode->block.stmts[i]->return_.expr) 
				{
					struct Str msg;
					str_ctorfmt(
						&msg,
						"Function ('%s') should not return anything.",
						func->node->funcdef.name->text
					);

					erw_error(
						msg.data, 
						lines[blocknode->block.stmts[i]->token->linenum - 1]
							.data, 
						blocknode->block.stmts[i]->token->linenum, 
						blocknode->block.stmts[i]->token->column,
						blocknode->block.stmts[i]->token->column + 
							vec_getsize(blocknode->block.stmts[i]->token->text)
							- 2
					);
					str_dtor(&msg);
				}
			}
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_WHILE)
		{
			struct erw_Type* exprtype = erw_getexprtype(
				scope, 
				blocknode->block.stmts[i]->while_.expr, 
				lines,
				NULL
			);

			struct erw_ASTNode* firstnode = erw_getfirstnode(
				blocknode->block.stmts[i]->while_ .expr
			);

			struct erw_ASTNode* lastnode = erw_getlastnode(
				blocknode->block.stmts[i]->while_.expr
			);

			erw_checkboolean(exprtype, firstnode, lastnode, lines);
			struct erw_Scope* newscope = erw_scope_new(
				scope, 
				scope->name,
				0,
				blocknode->block.stmts[i]->while_.block
			);

			Vec(struct erw_VarDeclr*) changedvars1 = erw_checkblock(
				newscope, 
				lines,
				0
			);
			vec_dtor(changedvars1);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_ASSIGNMENT)
		{
			vec_dtor(changedvars);
			changedvars = erw_checkassignment(
				scope, 
				blocknode->block.stmts[i], 
				lines, 
				allowsetimmut
			);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_FUNCCALL)
		{
			erw_checkfunccall(scope, blocknode->block.stmts[i], lines);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_MATCH)
		{
			struct erw_Type* type = erw_getexprtype(
				scope,
				blocknode->block.stmts[i]->match.expr,
				lines,
				NULL
			);
			blocknode->block.stmts[i]->match.exprtype = type;

			struct erw_Type* basetype = erw_type_getbase(type);
			Vec(struct erw_Type*) checkedtypes = vec_ctor(struct erw_Type*, 0);
			Vec(struct erw_Token*) checkedenums = vec_ctor(
				struct erw_Token*, 
				0
			);
			
			for(size_t j = 0; 
				j < vec_getsize(blocknode->block.stmts[i]->match.cases);
				j++)
			{
				struct erw_Scope* newscope = erw_scope_new(
					scope,
					scope->name,
					0,
					blocknode->block.stmts[i]->match.cases[j]->case_.block
				);
				
				if(blocknode->block.stmts[i]->match.cases[j]->case_.expr->type
					== erw_ASTNODETYPE_VARDECLR)
				{
					struct erw_Token* token = blocknode->block.stmts[i]
						->match.cases[j]->case_.expr->vardeclr.name;
					if(basetype->info != erw_TYPEINFO_UNION)
					{
						struct Str typename = erw_type_tostring(type);
						struct Str msg;
						str_ctorfmt(
							&msg, 
							"Expected expression of type '%s' (not matching a"
								" union type)",
							typename.data
						);

						erw_error(
							msg.data, 
							lines[token->linenum - 1].data,
							token->linenum, 
							token->column,
							token->column + vec_getsize(token ->text) - 2
						);
						str_dtor(&msg);
						str_dtor(&typename);
					}

					struct erw_VarDeclr* var = erw_scope_addvardeclr(
						newscope, 
						blocknode->block.stmts[i]->match.cases[j]->case_.expr,
						lines
					);

					for(size_t k = 0; k < vec_getsize(checkedtypes); k++)
					{
						if(erw_type_compare(var->type, checkedtypes[k]))
						{
							//TODO: Improve error message
							struct Str msg;
							str_ctor(&msg, "Duplicate case");

							erw_error(
								msg.data, 
								lines[token->linenum - 1].data,
								token->linenum, 
								token->column,
								token->column + vec_getsize(token ->text) - 2
							);
							str_dtor(&msg);
							break;
						}
					}

					struct erw_Type* comparetype;
					if(var->isref)
					{
						comparetype = var->type->reference.type;
					}
					else
					{
						comparetype = var->type;
					}

					vec_pushback(checkedtypes, var->type);
					int found = 0;
					for(size_t k = 0; 
						k < vec_getsize(basetype->union_.members);
						k++)
					{
						if(erw_type_compare(
							basetype->union_.members[k], 
							comparetype))
						{
							found = 1;
							break;
						}
					}

					if(!found)
					{
						struct Str typename = erw_type_tostring(type);
						struct Str casetypename = erw_type_tostring(
							comparetype
						);

						struct Str msg;
						str_ctorfmt(
							&msg, 
							"Cannot match type (union '%s' has no '%s'"
								" type)", 
							typename.data,
							casetypename.data
						);

						erw_error(
							msg.data, 
							lines[token->linenum - 1].data,
							token->linenum, 
							token->column,
							token->column + vec_getsize(token ->text) - 2
						);
						str_dtor(&msg);
						str_dtor(&casetypename);
						str_dtor(&typename);
					}

					if(blocknode->block.stmts[i]->match.cases[j]->case_.expr
						->vardeclr.isrefmut)
					{
						if(blocknode->block.stmts[i]->match.expr->token)
						{
							struct erw_VarDeclr* matchvar = 
								erw_scope_findvardeclr(
									scope, 
									blocknode->block.stmts[i]->match.expr->token
										->text
								);

							if(matchvar)
							{
								if(!matchvar->ismut)
								{
									struct Str msg;
									str_ctorfmt(
										&msg, 
										"Cannot take mutable reference of"
											" immutable variable ('%s')"
											" declared at line %zu,"
											" column %zu",
										var->node->vardeclr.name->text,
										var->node->vardeclr.name->linenum,
										var->node->vardeclr.name->column
									);

									erw_error(
										msg.data, 
										lines[token->linenum - 1].data,
										token->linenum, 
										token->column,
										token->column 
											+ vec_getsize(token ->text) - 2
									);
									str_dtor(&msg);
								}
							}
						}
					}
				}
				else
				{
					if(blocknode->block.stmts[i]->match.cases[j]->case_.expr
						->type == erw_ASTNODETYPE_ENUMLITERAL)
					{
						struct erw_Token* token = blocknode->block.stmts[i]
							->match.cases[j]->case_.expr->enumliteral.name;
						for(size_t k = 0; k < vec_getsize(checkedenums); k++)
						{
							if(!strcmp(checkedenums[k]->text, token->text))
							{
								//TODO: Improve error message
								struct Str msg;
								str_ctor(&msg, "Duplicate case");

								erw_error(
									msg.data, 
									lines[token->linenum - 1].data,
									token->linenum, 
									token->column,
									token->column + vec_getsize(token ->text) - 2
								);
								str_dtor(&msg);
								break;
							}
						}
						
						vec_pushback(checkedenums, token);
					}

					erw_checkexprtype(
						scope, 
						blocknode->block.stmts[i]->match.cases[j]->case_.expr,
						type,
						lines
					);

					//TODO: Check for duplicates
				}

				Vec(struct erw_VarDeclr*) changedvars1 = erw_checkblock(
					newscope, 
					lines,
					1
				);
				vec_dtor(changedvars1);
			}

			if(blocknode->block.stmts[i]->match.else_)
			{
				struct erw_Scope* newscope = erw_scope_new(
					scope,
					scope->name,
					0,
					blocknode->block.stmts[i]->match.else_->else_.block
				);

				Vec(struct erw_VarDeclr*) changedvars1 = erw_checkblock(
					newscope, 
					lines,
					1
				);
				vec_dtor(changedvars1);
			}

			//Check if all possible values are handled for exprs?
			//TODO: Check for duplicate cases

			if(basetype->info == erw_TYPEINFO_UNION)
			{
				if(vec_getsize(checkedtypes) 
					== vec_getsize(basetype->union_.members)
					&& blocknode->block.stmts[i]->match.else_)
				{
					struct Str msg;
					str_ctor(
						&msg, 
						"Unnecessary 'else' statement (all cases already"
							" handled)"
					);

					erw_error(
						msg.data, 
						lines[blocknode->block.stmts[i]->match.else_->token
							->linenum - 1].data,
						blocknode->block.stmts[i]->match.else_->token->linenum, 
						blocknode->block.stmts[i]->match.else_->token->column,
						blocknode->block.stmts[i]->match.else_->token->column
							+ vec_getsize(
								blocknode->block.stmts[i]->match.else_->token
									->text
							) - 2
					);
					str_dtor(&msg);
				}
				else if(vec_getsize(checkedtypes) 
					!= vec_getsize(basetype->union_.members)
					&& !blocknode->block.stmts[i]->match.else_)
				{
					struct Str typename = erw_type_tostring(type);
					struct Str msg;
					str_ctorfmt(
						&msg, 
						"Not all '%s' cases handled. ",
						typename.data
					);

					for(size_t j = 0; 
						j < vec_getsize(basetype->union_.members);
						j++)
					{
						int found = 0;
						for(size_t k = 0; 
							k < vec_getsize(checkedtypes); 
							k++)
						{
							if(erw_type_compare(
								basetype->union_.members[j], 
								checkedtypes[k]))
							{
								found = 1;
								break;
							}
						}

						if(!found)
						{
							struct Str membername = erw_type_tostring(
								basetype->union_.members[j]
							);
							str_appendfmt(
								&msg, 
								"Missing case '%s'. ",
								membername.data
							);
							str_dtor(&membername);
						}
					}

					erw_warning(
						msg.data, 
						lines[blocknode->block.stmts[i]->token->linenum - 1]
							.data,
						blocknode->block.stmts[i]->token->linenum, 
						blocknode->block.stmts[i]->token->column,
						blocknode->block.stmts[i]->token
							->column + vec_getsize(
								blocknode->block.stmts[i]->token->text
							) - 2
					);
					str_dtor(&msg);
					str_dtor(&typename);
				}
			}
			else if(basetype->info == erw_TYPEINFO_ENUM)
			{
				if(vec_getsize(checkedenums) 
					== vec_getsize(basetype->enum_.members) 
					&& blocknode->block.stmts[i]->match.else_)
				{
					struct Str msg;
					str_ctor(
						&msg, 
						"Unnecessary 'else' statement (all cases already"
							" handled)"
					);

					erw_error(
						msg.data, 
						lines[blocknode->block.stmts[i]->match.else_->token
							->linenum - 1].data,
						blocknode->block.stmts[i]->match.else_->token
							->linenum, 
						blocknode->block.stmts[i]->match.else_->token
							->column,
						blocknode->block.stmts[i]->match.else_->token
							->column + vec_getsize(
								blocknode->block.stmts[i]->match.else_
									->token->text
							) - 2
					);
					str_dtor(&msg);
				}
				else if(vec_getsize(checkedenums) 
					!= vec_getsize(basetype->enum_.members)
					&& !blocknode->block.stmts[i]->match.else_)
				{
					struct Str typename = erw_type_tostring(type);
					struct Str msg;
					str_ctorfmt(
						&msg, 
						"Not all '%s' cases handled. ",
						typename.data
					);

					for(size_t j = 0; 
						j < vec_getsize(basetype->enum_.members);
						j++)
					{
						int found = 0;
						for(size_t k = 0; 
							k < vec_getsize(checkedenums); 
							k++)
						{
							if(!strcmp(
								basetype->enum_.members[j].name, 
								checkedenums[k]->text))
							{
								found = 1;
								break;
							}
						}

						if(!found)
						{
							str_appendfmt(
								&msg, 
								"Missing case '%s'. ",
								basetype->enum_.members[j].name
							);
						}
					}

					erw_warning(
						msg.data, 
						lines[blocknode->block.stmts[i]->token->linenum - 1]
							.data,
						blocknode->block.stmts[i]->token->linenum, 
						blocknode->block.stmts[i]->token->column,
						blocknode->block.stmts[i]->token
							->column + vec_getsize(
								blocknode->block.stmts[i]->token->text
							) - 2
					);
					str_dtor(&msg);
					str_dtor(&typename);
				}
			}

			vec_dtor(checkedenums);
			vec_dtor(checkedtypes);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_BREAK)
		{
			//Do nothing
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_CONTINUE)
		{
			//Do nothing
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_FOREACH)
		{
			struct erw_Scope* newscope = erw_scope_new(
				scope,
				scope->name,
				0,
				blocknode->block.stmts[i]->foreach.block
			);
			
			struct erw_VarDeclr* var = erw_scope_addvardeclr(
				newscope, 
				blocknode->block.stmts[i]->foreach.var, 
				lines
			);
			var->hasvalue = 1;

			struct erw_Type* type = erw_getexprtype(
				scope, 
				blocknode->block.stmts[i]->foreach.expr, 
				lines,
				var->isref ? var->type->reference.type : var->type
			);

			struct erw_Type* checktype = erw_type_new(
				erw_TYPEINFO_SLICE,
				NULL
			);

			checktype->slice.type = var->isref
				? var->type->reference.type
				: var->type;
			if(type->info == erw_TYPEINFO_REFERENCE)
			{
				blocknode->block.stmts[i]->foreach.expr->unexpr.literaltype 
					= checktype;
			}

			if(!erw_type_compatible(checktype, type))
			{
				struct Str typename1 = erw_type_tostring(checktype);
				struct Str typename = erw_type_tostring(type);
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Expected slice type ('%s'). Got '%s'",
					typename1.data,
					typename.data
				);
				erw_error(
					msg.data, 
					lines[
						blocknode->block.stmts[i]->foreach.expr->token->linenum 
							- 1
						].data,
					blocknode->block.stmts[i]->foreach.expr->token->linenum, 
					blocknode->block.stmts[i]->foreach.expr->token->column,
					blocknode->block.stmts[i]->foreach.expr->token->column 
						+ vec_getsize(
							blocknode->block.stmts[i]->foreach.expr->token->text
						) - 2
				);

				str_dtor(&msg);
				str_dtor(&typename);
				str_dtor(&typename1);
			}

			blocknode->block.stmts[i]->foreach.exprtype = checktype;
			struct erw_Token* token = blocknode->block.stmts[i]->foreach.var
				->vardeclr.name;

			struct erw_Type* comparetype;
			if(var->isref)
			{
				comparetype = var->type->reference.type;
			}
			else
			{
				comparetype = var->type;
			}

			if(!erw_type_compare(checktype->slice.type, comparetype))
			{
				struct Str typename = erw_type_tostring(type->slice.type);
				struct Str casetypename = erw_type_tostring(comparetype);
				struct Str msg;
				str_ctorfmt(
					&msg, 
					"Expected type '%s', got type '%s'", 
					typename.data,
					casetypename.data
				);

				erw_error(
					msg.data, 
					lines[token->linenum - 1].data,
					token->linenum, 
					token->column,
					token->column + vec_getsize(token ->text) - 2
				);
				str_dtor(&msg);
				str_dtor(&casetypename);
				str_dtor(&typename);
			}

			if(blocknode->block.stmts[i]->foreach.var->vardeclr.isrefmut)
			{
				if(blocknode->block.stmts[i]->match.expr->token)
				{
					struct erw_VarDeclr* matchvar = 
						erw_scope_findvardeclr(
							scope, 
							blocknode->block.stmts[i]->match.expr->token
								->text
						);

					if(matchvar)
					{
						if(!matchvar->ismut)
						{
							struct Str msg;
							str_ctorfmt(
								&msg, 
								"Cannot take mutable reference of"
									" immutable variable ('%s')"
									" declared at line %zu,"
									" column %zu",
								var->node->vardeclr.name->text,
								var->node->vardeclr.name->linenum,
								var->node->vardeclr.name->column
							);

							erw_error(
								msg.data, 
								lines[token->linenum - 1].data,
								token->linenum, 
								token->column,
								token->column 
									+ vec_getsize(token ->text) - 2
							);
							str_dtor(&msg);
						}
					}
				}
			}

			Vec(struct erw_VarDeclr*) changedvars1 = erw_checkblock(
				newscope, 
				lines,
				0
			);
			vec_dtor(changedvars1);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_FOR)
		{
			struct erw_Scope* newscope = erw_scope_new(
				scope,
				scope->name,
				0,
				blocknode->block.stmts[i]->for_.block
			);

			erw_scope_addvardeclr(
				newscope, 
				blocknode->block.stmts[i]->for_.var, 
				lines
			);

			struct erw_Type* exprtype = erw_getexprtype(
				newscope, 
				blocknode->block.stmts[i]->for_.expr, 
				lines,
				NULL
			);

			struct erw_ASTNode* firstnode = erw_getfirstnode(
				blocknode->block.stmts[i]->for_.expr
			);

			struct erw_ASTNode* lastnode = erw_getlastnode(
				blocknode->block.stmts[i]->for_.expr
			);

			erw_checkboolean(exprtype, firstnode, lastnode, lines);
			vec_dtor(changedvars);
			changedvars = erw_checkassignment(
				newscope, 
				blocknode->block.stmts[i]->for_.increment, 
				lines, 
				allowsetimmut
			);

			Vec(struct erw_VarDeclr*) changedvars1 = erw_checkblock(
				newscope, 
				lines,
				0
			);
			vec_dtor(changedvars1);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_BLOCK)
		{
			struct erw_Scope* newscope = erw_scope_new(
				scope, 
				scope->name, 
				0, 
				blocknode->block.stmts[i]
			);

			Vec(struct erw_VarDeclr*) changedvars1 = erw_checkblock(
				newscope, 
				lines, 
				1
			);

			vec_dtor(changedvars1);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_LITERAL)
		{
			struct Str msg;
			str_ctor(&msg, "Unexpected literal");
			erw_error(
				msg.data, 
				lines[blocknode->block.stmts[i]->token->linenum - 1].data,
				blocknode->block.stmts[i]->token->linenum, 
				blocknode->block.stmts[i]->token->column,
				blocknode->block.stmts[i]->token->column +
					vec_getsize(blocknode->block.stmts[i]->token->text) - 2
			);
			str_dtor(&msg);
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_TEMPLATE) 
		{
			if(blocknode->block.stmts[i]->template.body->type 
				== erw_ASTNODETYPE_TYPEDECLR)
			{
				erw_scope_addtypetemplate(
					scope, 
					blocknode->block.stmts[i], 
					lines
				);
			}
			else
			{
				erw_scope_addfunctemplate(
					scope, 
					blocknode->block.stmts[i], 
					lines
				);
			}
		}
		else if(blocknode->block.stmts[i]->type == erw_ASTNODETYPE_TEMPLATECALL) 
		{
			struct erw_Scope* callscope;
			if(blocknode->block.stmts[i]->templatecall.body->funccall.callee
				->type == erw_ASTNODETYPE_BINEXPR)
			{
				callscope = erw_scope_getmodule(
					scope, 
					blocknode->block.stmts[i]->templatecall.body->funccall
						.callee->binexpr.expr1->token,
					lines
				)->result.scope;
			}
			else
			{
				callscope = scope;
			}

			erw_callfunctemplate(callscope, blocknode->block.stmts[i], lines);
			erw_checkfunccall(
				scope, 
				blocknode->block.stmts[i]->templatecall.body, 
				lines
			);
		}
		else
		{
			log_assert(
				0,
				"Unhandled type: %s", 
				blocknode->block.stmts[i]->type->name
			);
		}
	}

	return changedvars;
}

static int erw_checkfuncifret(struct erw_ASTNode* ifnode) //XXX: Horrible name
{
	log_assert(ifnode, "is NULL");
	log_assert(
		ifnode->type == erw_ASTNODETYPE_IF, 
		"invalid size (%s)", 
		ifnode->type->name
	);

	if(!ifnode->if_.else_)
	{
		return 0;
	}

	if(!vec_getsize(ifnode->if_.block->block.stmts))
	{
		return 0;
	}

	struct erw_ASTNode* laststatement = ifnode->if_.block->block.stmts[
		vec_getsize(ifnode->if_.block->block.stmts) - 1
	];

	if(laststatement->type == erw_ASTNODETYPE_RETURN) {}
	else if(laststatement->type == erw_ASTNODETYPE_IF)
	{
		if(!erw_checkfuncifret(laststatement))
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	for(size_t i = 0; i < vec_getsize(ifnode->if_.elseifs); i++)
	{
		if(!vec_getsize(ifnode->if_.elseifs[i]->elseif.block->block.stmts))
		{
			return 0;
		}

		laststatement = ifnode->if_.elseifs[i]->elseif.block->block.stmts[
			vec_getsize(ifnode->if_.elseifs[i]->elseif.block->block.stmts) - 1
		];

		if(laststatement->type == erw_ASTNODETYPE_RETURN) {}
		else if(laststatement->type == erw_ASTNODETYPE_IF)
		{
			if(!erw_checkfuncifret(laststatement))
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}

	if(ifnode->if_.else_)
	{
		if(!vec_getsize(ifnode->if_.else_->else_.block->block.stmts))
		{
			return 0;
		}

		laststatement = ifnode->if_.else_->else_.block->block.stmts[
			vec_getsize(ifnode->if_.else_->else_.block->block.stmts) - 1
		];

		if(laststatement->type == erw_ASTNODETYPE_RETURN) {}
		else if(laststatement->type == erw_ASTNODETYPE_IF)
		{
			if(!erw_checkfuncifret(laststatement))
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	return 1;
}

//Used in erw_scope.c
void erw_checkfuncdeclr(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(scope, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_FUNCDEF 
			|| node->type == erw_ASTNODETYPE_FOREIGN, 
		"invalid type (%s)",
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_FuncDeclr* func = erw_scope_addfuncdeclr(scope, node, lines);
	if(!func->isprot)
	{
		Vec(struct erw_VarDeclr*) changedvars = erw_checkblock(
			func->scope, 
			lines, 
			1
		);

		vec_dtor(changedvars);
		if(func->rettype)
		{
			struct erw_ASTNode* funcnode;
			if(node->type == erw_ASTNODETYPE_FOREIGN)
			{
				log_assert(
					node->foreign.node->type == erw_ASTNODETYPE_FUNCDEF, 
					"invalid type (%s)", 
					node->foreign.node->type->name
				);

				funcnode = node->foreign.node;
			}
			else
			{
				funcnode = node;
			}

			int hasreturn = 0;
			if(vec_getsize(funcnode->funcdef.block->block.stmts))
			{
				struct erw_ASTNode* laststatement = funcnode->funcdef.block
					->block.stmts[
					vec_getsize(funcnode->funcdef.block->block.stmts) - 1];

				if(laststatement->type == erw_ASTNODETYPE_RETURN)
				{
					hasreturn = 1;
				}
				else if(laststatement->type == erw_ASTNODETYPE_IF)
				{
					hasreturn = erw_checkfuncifret(laststatement);
				}
			}

			if(!hasreturn)
			{
				struct Str msg;
				str_ctor(&msg, "Function expects return at the end");
				erw_error(
					msg.data, 
					lines[funcnode->funcdef.name->linenum - 1].data,
					funcnode->funcdef.name->linenum, 
					funcnode->funcdef.name->column,
					funcnode->funcdef.name->column +
						vec_getsize(funcnode->funcdef.name->text) - 2
				);
				str_dtor(&msg);
			}
		}
	}
}

static void erw_checkunused(struct erw_Scope* scope, struct Str* lines)
{
	log_assert(scope, "is NULL");
	log_assert(lines, "is NULL");

	for(size_t i = 0; i < vec_getsize(scope->variables); i++)
	{
		struct erw_VarDeclr* var = &scope->variables[i];
		if(!var->isused)
		{
			struct Str msg;
			str_ctor(&msg, "Unused variable");
			erw_warning(
				msg.data, 
				lines[var->node->vardeclr.name->linenum - 1].data,
				var->node->vardeclr.name->linenum, 
				var->node->vardeclr.name->column,
				var->node->vardeclr.name->column + 
					vec_getsize(var->node->vardeclr.name->text) - 2
			);
			str_dtor(&msg);
		}
	}

/*
	for(size_t i = 0; i < vec_getsize(scope->functions); i++)
	{
		struct erw_FuncDeclr* func = &scope->functions[i];
		if(!func->isused)
		{
			struct Str msg;
			str_ctor(&msg, "Unused function");
			if(func->isforeign)
			{
				erw_warning(
					msg.data, 
					lines[func->node->foreign.node->funcdef.name->linenum - 1]
						.data,
					func->node->foreign.node->funcdef.name->linenum, 
					func->node->foreign.node->funcdef.name->column,
					func->node->foreign.node->funcdef.name->column +
						vec_getsize(
							func->node->foreign.node->funcdef.name->text) - 2
				);
			}
			else
			{
				erw_warning(
					msg.data, 
					lines[func->node->funcdef.name->linenum - 1].data,
					func->node->funcdef.name->linenum, 
					func->node->funcdef.name->column,
					func->node->funcdef.name->column +
						vec_getsize(func->node->funcdef.name->text) - 2
				);
			}

			str_dtor(&msg);
		}
	}

	for(size_t i = 0; i < vec_getsize(scope->types); i++)
	{
		struct erw_TypeDeclr* type = &scope->types[i];
		if(!type->isused)
		{
			struct Str msg;
			str_ctor(&msg, "Unused type");
			if(type->isforeign)
			{
				erw_warning(
					msg.data,
					lines[type->node->foreign.node->typedeclr.name->linenum - 1]
						.data,
					type->node->foreign.node->typedeclr.name->linenum,
					type->node->foreign.node->typedeclr.name->column,
					type->node->foreign.node->typedeclr.name->column +
						vec_getsize(
							type->node->foreign.node->typedeclr.name->text) - 2
				);
			}
			else
			{
				erw_warning(
					msg.data,
					lines[type->node->typedeclr.name->linenum - 1].data,
					type->node->typedeclr.name->linenum,
					type->node->typedeclr.name->column,
					type->node->typedeclr.name->column +
						vec_getsize(type->node->typedeclr.name->text) - 2
				);
			}

			str_dtor(&msg);
		}
	}
	*/

	for(size_t i = 0; i < vec_getsize(scope->children); i++)
	{
		erw_checkunused(scope->children[i], lines);
	}
}

static void erw_checkmain(struct erw_Scope* scope, struct Str* lines)
{
	log_assert(scope, "is NULL");
	log_assert(lines, "is NULL");

	struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(scope, "main", 0);
	if(!func)
	{
		erw_error("No main function found", NULL, 0, 0, 0);
	}
	
	if(!func->rettype 
		|| !erw_type_compare(
			func->rettype, 
			erw_type_builtins[erw_TYPEBUILTIN_INT32]))
	{
		erw_error(
			"Expected main to return 'Int32'",
			lines[func->node->funcdef.name->linenum - 1].data,
			func->node->funcdef.name->linenum,
			func->node->funcdef.name->column,
			func->node->funcdef.name->column +
				vec_getsize(func->node->funcdef.name->text) - 2
		);
	}

	//TODO: Check parameter types
	func->isused = 1;
}

struct erw_Scope* erw_checksemantics(
	struct erw_ASTNode* ast, 
	struct Str* lines,
	const char* filename)
{
	log_assert(ast, "is NULL");
	log_assert(
		ast->type == erw_ASTNODETYPE_START, 
		"invalid type (%s)", 
		ast->type->name
	);
	log_assert(lines, "is NULL");
	log_assert(filename, "is NULL");

	erw_errorfilename = filename;
	
	//NOTE: Global scope is temporarily named NULL
	struct erw_Scope* globalscope = erw_scope_new(NULL, filename, 0, ast); 

	//Add all builtin types
	for(size_t i = 0; i < erw_TYPEBUILTIN_COUNT; i++)
	{  
		//Should this use erw_scope_addtypedeclr?
		struct erw_TypeDeclr symbol = {
			.type = erw_type_builtins[i],
			.isused = 1,
			.name = erw_type_builtins[i]->named.name
		};

		vec_pushback(globalscope->types, symbol);
	}

	for(size_t i = 0; i < vec_getsize(ast->start.children); i++)
	{
		if(ast->start.children[i]->type == erw_ASTNODETYPE_FUNCDEF)
		{
			erw_checkfuncdeclr(globalscope, ast->start.children[i], lines);
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_TYPEDECLR)
		{
			erw_scope_addtypedeclr(globalscope, ast->start.children[i], lines);
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_FOREIGN)
		{
			if(ast->start.children[i]->foreign.node->type 
				== erw_ASTNODETYPE_FUNCDEF)
			{
				erw_checkfuncdeclr(globalscope, ast->start.children[i], lines);
			}
			else if(ast->start.children[i]->foreign.node->type 
				== erw_ASTNODETYPE_TYPEDECLR)
			{
				erw_scope_addtypedeclr(
					globalscope, 
					ast->start.children[i], 
					lines
				);
			}
			else if(ast->start.children[i]->foreign.node->type 
				== erw_ASTNODETYPE_IMPORT)
			{
				erw_scope_addmodule(globalscope, ast->start.children[i], lines);
				erw_errorfilename = filename;
			}
			else
			{
				log_assert(
					0,
					"Unhandled type: %s", 
					ast->start.children[i]->foreign.node->type->name
				);
			}
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_IMPORT)
		{
			erw_scope_addmodule(globalscope, ast->start.children[i], lines);
			erw_errorfilename = filename;
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_TEMPLATE) 
		{
			if(ast->start.children[i]->template.body->type 
				== erw_ASTNODETYPE_TYPEDECLR)
			{
				erw_scope_addtypetemplate(
					globalscope, 
					ast->start.children[i], 
					lines
				);
			}
			else
			{
				erw_scope_addfunctemplate(
					globalscope, 
					ast->start.children[i], 
					lines
				);
			}
		}
		else
		{
			log_assert(
				0,
				"Unhandled type: %s", 
				ast->start.children[i]->type->name
			);
		}
	}

	//erw_checkmain(globalscope, lines);
	erw_checkunused(globalscope, lines);
	//TODO: erw_checkfuncprots();
	return globalscope;
}

