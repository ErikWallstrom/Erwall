/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "erw_scope.h"
#include "erw_error.h"
#include "log.h"
#include <stdlib.h>

struct erw_Scope* erw_scope_new(
	struct erw_Scope* parent, 
	const char* name, 
	int isfunc,
	struct erw_ASTNode* node)
{
	log_assert(node, "is NULL");

	struct erw_Scope* self = malloc(sizeof(struct erw_Scope));
	if(!self)
	{
		log_error("malloc failed in <%s>", __func__);
	}

	self->functions = vec_ctor(struct erw_FuncDeclr, 0);
	self->variables = vec_ctor(struct erw_VarDeclr, 0);
	self->types = vec_ctor(struct erw_TypeDeclr, 0);
	self->children = vec_ctor(struct erw_Scope*, 0);
	self->modules = vec_ctor(struct erw_Module, 0);
	self->typetemplates = vec_ctor(struct erw_TypeTemplate, 0);
	self->functemplates = vec_ctor(struct erw_FuncTemplate, 0);
	self->parent = parent;
	self->isfunc= isfunc;
	self->name = name;
	self->node = node;

	if(parent)
	{
		self->index = vec_getsize(parent->children);
		vec_pushback(parent->children, self);
	}
	else
	{
		self->index = 0;
	}

	return self;
}

struct erw_VarDeclr* erw_scope_findvardeclr(
	struct erw_Scope* self, 
	const char* name)
{
	log_assert(self, "is NULL");
	log_assert(name, "is NULL");

	struct erw_Scope* scope = self;
	while(scope)
	{
		for(size_t i = 0; i < vec_getsize(scope->variables); i++)
		{
			if(!strcmp(scope->variables[i].name, name))
			{
				return &scope->variables[i];
			}
		}

		scope = scope->parent;
		if(scope && scope->name)
		{
			if(strcmp(scope->name, self->name))
			{
				break;
			}
		}
		else
		{
			break;
		}
	}

	return NULL;
}

struct erw_FuncDeclr* erw_scope_findfuncdeclr(
	struct erw_Scope* self, 
	const char* name, 
	int acceptprot)
{
	log_assert(self, "is NULL");
	log_assert(name, "is NULL");

	struct erw_Scope* scope = self;
	while(scope)
	{
		for(size_t i = 0; i < vec_getsize(scope->functions); i++)
		{
			if(!strcmp(scope->functions[i].name, name))
			{
				if(!acceptprot && scope->functions[i].isprot)
				{
					continue;
				}

				return &scope->functions[i];
			}
		}

		scope = scope->parent;
	}

	return NULL;
}

struct erw_TypeDeclr* erw_scope_findtypedeclr(
	struct erw_Scope* self, 
	const char* name)
{
	log_assert(self, "is NULL");
	log_assert(name, "is NULL");

	struct erw_Scope* scope = self;
	while(scope)
	{
		for(size_t i = 0; i < vec_getsize(scope->types); i++)
		{
			if(!strcmp(scope->types[i].name, name))
			{
				return &scope->types[i];
			}
		}

		scope = scope->parent;
	}

	return NULL;
}

struct erw_Module* erw_scope_findmodule(
	struct erw_Scope* self, 
	const char* name)
{
	log_assert(self, "is NULL");
	log_assert(name, "is NULL");

	struct erw_Scope* scope = self;
	while(scope)
	{
		for(size_t i = 0; i < vec_getsize(scope->modules); i++)
		{
			if(scope->modules[i].name)
			{
				if(!strcmp(scope->modules[i].name, name))
				{
					return &scope->modules[i];
				}
			}
		}

		scope = scope->parent;
	}

	return NULL;
}

struct erw_TypeTemplate* erw_scope_findtypetemplate(
	struct erw_Scope* self, 
	const char* name)
{
	log_assert(self, "is NULL");
	log_assert(name, "is NULL");

	struct erw_Scope* scope = self;
	while(scope)
	{
		for(size_t i = 0; i < vec_getsize(scope->typetemplates); i++)
		{
			if(!strcmp(scope->typetemplates[i].name, name))
			{
				return &scope->typetemplates[i];
			}
		}

		scope = scope->parent;
	}

	return NULL;
}

struct erw_FuncTemplate* erw_scope_findfunctemplate(
	struct erw_Scope* self, 
	const char* name)
{
	log_assert(self, "is NULL");
	log_assert(name, "is NULL");

	struct erw_Scope* scope = self;
	while(scope)
	{
		for(size_t i = 0; i < vec_getsize(scope->functemplates); i++)
		{
			if(!strcmp(scope->functemplates[i].name, name))
			{
				return &scope->functemplates[i];
			}
		}

		scope = scope->parent;
	}

	return NULL;
}

struct erw_VarDeclr* erw_scope_getvardeclr(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(token, "is NULL");
	log_assert(
		token->type == erw_TOKENTYPE_IDENT, 
		"invalid type (%s)", 
		token->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_VarDeclr* ret = erw_scope_findvardeclr(self, token->text);
	if(!ret)
	{ 
		struct Str msg;
		str_ctor(
			&msg,
			"Undefined variable"
		);

		erw_error(
			msg.data, 
			lines[token->linenum - 1].data, 
			token->linenum, 
			token->column,
			token->column + vec_getsize(token->text) - 2
		);
		str_dtor(&msg);
	}
	else
	{
		ret->isused = 1;
	}

	return ret;
}
struct erw_FuncDeclr* erw_scope_getfuncdeclr(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(token, "is NULL");
	log_assert(
		token->type == erw_TOKENTYPE_IDENT, 
		"invalid type (%s)", 
		token->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_FuncDeclr* ret = erw_scope_findfuncdeclr(self, token->text, 1);
	if(!ret)
	{ 
		struct Str msg;
		str_ctor(&msg, "Undefined function");
		erw_error(
			msg.data, 
			lines[token->linenum - 1].data, 
			token->linenum, 
			token->column,
			token->column + vec_getsize(token->text) - 2
		);
		str_dtor(&msg);
	}
	else
	{
		ret->isused = 1;
	}

	return ret;
}

struct erw_TypeDeclr* erw_scope_gettypedeclr(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(token, "is NULL");
	log_assert(
		token->type == erw_TOKENTYPE_TYPE, 
		"invalid type (%s)", 
		token->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_TypeDeclr* ret = erw_scope_findtypedeclr(self, token->text);
	if(!ret)
	{ 
		struct Str msg;
		str_ctor(&msg, "Undefined type");
		erw_error(
			msg.data, 
			lines[token->linenum - 1].data, 
			token->linenum, 
			token->column,
			token->column + vec_getsize(token->text) - 2
		);
		str_dtor(&msg);
		return NULL; //Prevent warning
	}

	ret->isused = 1;
	return ret;
}

struct erw_Module* erw_scope_getmodule(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(token, "is NULL");
	log_assert(
		token->type == erw_TOKENTYPE_TYPE, 
		"invalid type (%s)", 
		token->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_Module* ret = erw_scope_findmodule(self, token->text);
	if(!ret)
	{ 
		struct Str msg;
		str_ctor(&msg, "Undefined module");
		erw_error(
			msg.data, 
			lines[token->linenum - 1].data, 
			token->linenum, 
			token->column,
			token->column + vec_getsize(token->text) - 2
		);
		str_dtor(&msg);
		return NULL; //Prevent warning
	}

	ret->isused = 1;
	return ret;
}

struct erw_TypeTemplate* erw_scope_gettypetemplate(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(token, "is NULL");
	log_assert(
		token->type == erw_TOKENTYPE_TYPE, 
		"invalid type (%s)", 
		token->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_TypeTemplate* ret = erw_scope_findtypetemplate(	
		self, 
		token->text
	);

	if(!ret)
	{ 
		struct Str msg;
		str_ctor(&msg, "Undefined template");
		erw_error(
			msg.data, 
			lines[token->linenum - 1].data, 
			token->linenum, 
			token->column,
			token->column + vec_getsize(token->text) - 2
		);
		str_dtor(&msg);
		return NULL; //Prevent warning
	}

	return ret;
}

struct erw_FuncTemplate* erw_scope_getfunctemplate(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(token, "is NULL");
	log_assert(
		token->type == erw_TOKENTYPE_IDENT, 
		"invalid type (%s)", 
		token->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_FuncTemplate* ret = erw_scope_findfunctemplate(	
		self, 
		token->text
	);

	if(!ret)
	{ 
		struct Str msg;
		str_ctor(&msg, "Undefined template");
		erw_error(
			msg.data, 
			lines[token->linenum - 1].data, 
			token->linenum, 
			token->column,
			token->column + vec_getsize(token->text) - 2
		);
		str_dtor(&msg);
	}

	return ret;
}

static void erw_scope_checkvardeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(node->type == erw_ASTNODETYPE_VARDECLR, "invalid type");
	log_assert(lines, "is NULL");

	struct erw_VarDeclr* var = erw_scope_findvardeclr(
		self, 
		node->vardeclr.name->text
	);

	if(var)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Redefinition of variable ('%s') declared at line %zu, column %zu", 
			node->vardeclr.name->text,
			var->node->vardeclr.name->linenum,
			var->node->vardeclr.name->column
		);

		erw_error(
			msg.data, 
			lines[node->vardeclr.name->linenum - 1].data, 
			node->vardeclr.name->linenum, 
			node->vardeclr.name->column,
			node->vardeclr.name->column + 
				vec_getsize(node->vardeclr.name->text) - 2
		);
		str_dtor(&msg);
	}

	struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
		self, 
		node->vardeclr.name->text,
		1
	);

	if(func)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Redefinition of function ('%s') declared at line %zu, column %zu", 
			node->vardeclr.name->text,
			func->node->funcdef.name->linenum,
			func->node->funcdef.name->column
		);

		erw_error(
			msg.data, 
			lines[node->vardeclr.name->linenum - 1].data, 
			node->vardeclr.name->linenum, 
			node->vardeclr.name->column,
			node->vardeclr.name->column + 
				vec_getsize(node->vardeclr.name->text) - 2
		);
		str_dtor(&msg);
	}
}

struct erw_VarDeclr* erw_scope_addvardeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_VARDECLR 
			|| node->type == erw_ASTNODETYPE_FOREIGN, 
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");
	
	struct erw_ASTNode* foreign = NULL;
	if(node->type == erw_ASTNODETYPE_FOREIGN)
	{
		log_assert(
			node->foreign.node->type == erw_ASTNODETYPE_VARDECLR, 
			"invalid type (%s)", 
			node->foreign.node->type->name
		);

		foreign = node;
		node = node->foreign.node;
	}

	erw_scope_checkvardeclr(self, node, lines);

	struct erw_VarDeclr symbol;
	symbol.node = foreign ? foreign : node;
	symbol.isforeign = foreign ? 1 : 0;
	symbol.isused = 0;
	symbol.isconst = 0; //TODO
	symbol.ismut = node->vardeclr.ismut;
	symbol.isref = node->vardeclr.isref;
	symbol.isrefmut = node->vardeclr.isrefmut;
	symbol.isvariadic = node->vardeclr.isvariadic;
	symbol.name = node->vardeclr.name->text;
	symbol.type = erw_type_create(node->vardeclr.type, self, lines);

	if(node->vardeclr.isref)
	{
		struct erw_Type* type = erw_type_new(erw_TYPEINFO_REFERENCE, NULL);
		type->reference.ismut = node->vardeclr.isrefmut;
		type->reference.size = sizeof(void*); //NOTE: Temporary
		type->reference.type = symbol.type;

		symbol.type = type;
	}
	else if(node->vardeclr.isvariadic)
	{
		struct erw_Type* type = erw_type_new(erw_TYPEINFO_SLICE, NULL);
		type->slice.ismut = node->vardeclr.isrefmut;
		type->slice.size = sizeof(void*); //NOTE: Temporary
		type->slice.type = symbol.type;

		symbol.type = type;
	}

	if(node->vardeclr.value)
	{
		symbol.hasvalue = 1;
	}
	else
	{
		symbol.hasvalue = 0;
	}

	vec_pushback(self->variables, symbol);
	return &self->variables[vec_getsize(self->variables) - 1];
}

static void erw_scope_checkfuncdeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_FUNCDEF, 
		"invalid type: %s",
		node->type->name
	);
	log_assert(lines, "is NULL");
	
	struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
		self, 
		node->funcdef.name->text,
		1
	);

	if(func && !node->funcdef.block)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Unnecessary function prototype ('%s')", 
			node->funcdef.name->text
		);

		erw_error(
			msg.data, 
			lines[node->funcdef.name->linenum - 1].data, 
			node->funcdef.name->linenum, 
			node->funcdef.name->column,
			node->funcdef.name->column + 
				vec_getsize(node->funcdef.name->text) - 2
		);
		str_dtor(&msg);
	}
	
	func = erw_scope_findfuncdeclr(
		self, 
		node->funcdef.name->text,
		0
	);

	if(func)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Redefinition of function ('%s') declared at line %zu, column"
				" %zu", 
			node->funcdef.name->text,
			func->node->funcdef.name->linenum,
			func->node->funcdef.name->column
		);

		erw_error(
			msg.data, 
			lines[node->funcdef.name->linenum - 1].data, 
			node->funcdef.name->linenum, 
			node->funcdef.name->column,
			node->funcdef.name->column + 
				vec_getsize(node->funcdef.name->text) - 2
		);
		str_dtor(&msg);
	}

	struct erw_VarDeclr* var = erw_scope_findvardeclr(
		self, 
		node->funcdef.name->text
	);

	if(var)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Redefinition of variable ('%s') declared at line %zu, column %zu", 
			node->funcdef.name->text,
			var->node->vardeclr.name->linenum,
			var->node->vardeclr.name->column
		);

		erw_error(
			msg.data, 
			lines[node->funcdef.name->linenum - 1].data, 
			node->funcdef.name->linenum, 
			node->funcdef.name->column,
			node->funcdef.name->column + 
				vec_getsize(node->funcdef.name->text) - 2
		);
		str_dtor(&msg);
	}
}

struct erw_FuncDeclr* erw_scope_addfuncdeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_FUNCDEF
			|| node->type == erw_ASTNODETYPE_FOREIGN, 
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_ASTNode* foreign = NULL;
	if(node->type == erw_ASTNODETYPE_FOREIGN)
	{
		log_assert(
			node->foreign.node->type == erw_ASTNODETYPE_FUNCDEF, 
			"invalid type (%s)", 
			node->foreign.node->type->name
		);

		foreign = node;
		node = node->foreign.node;
	}

	erw_scope_checkfuncdeclr(self, node, lines);

	struct erw_FuncDeclr symbol;
	symbol.node = foreign ? foreign : node;
	symbol.isused = 0;
	symbol.isvariadic = node->funcdef.isvariadic;
	symbol.name = node->funcdef.name->text;
	symbol.module = NULL;
	symbol.params = vec_ctor(
		struct erw_VarDeclr*, 
		vec_getsize(node->funcdef.params)
	);

	if(foreign)
	{
		symbol.isforeign = 1;
		symbol.isprot = 1;
	}
	else
	{
		symbol.isforeign = 0;
		symbol.isprot = node->funcdef.block ? 0 : 1;
	}

	if(node->funcdef.type)
	{
		symbol.rettype = erw_type_create(node->funcdef.type, self, lines);
	}
	else
	{
		symbol.rettype = NULL;
	}

	symbol.type = erw_type_new(erw_TYPEINFO_FUNC, NULL);
	symbol.type->func.size = sizeof(void(*)(void));
	symbol.type->func.rettype = symbol.rettype;
	symbol.type->func.isvariadic = symbol.isvariadic;
	symbol.type->func.isforeign = symbol.isforeign;

	vec_pushback(self->functions, symbol);
	struct erw_FuncDeclr* func = &self->functions[
		vec_getsize(self->functions) - 1
	];

	if(!symbol.isprot)
	{
		func->scope = erw_scope_new(self, symbol.name, 1, node->funcdef.block);
		for(size_t i = 0; i < vec_getsize(node->funcdef.params); i++)
		{
			struct erw_VarDeclr* param = erw_scope_addvardeclr(
				func->scope, 
				node->funcdef.params[i], 
				lines
			);

			param->hasvalue = 1;
			struct erw_VarDeclr* copy = malloc(sizeof(struct erw_VarDeclr));
			if(!copy)
			{
				log_error("malloc failed");
			}

			*copy = *param;
			vec_pushback(func->params, copy);
			vec_pushback(func->type->func.params, copy->type);
		}
	}
	else
	{
		func->scope = NULL;
		for(size_t i = 0; i < vec_getsize(node->funcdef.params); i++)
		{
			struct erw_Type* paramtype = erw_type_create(
				node->funcdef.params[i]->vardeclr.type, 
				self, 
				lines
			);

			vec_pushback(func->type->func.params, paramtype);
		}
	}

	return func;
}

static void erw_scope_checktypedeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(node->type == erw_ASTNODETYPE_TYPEDECLR, "invalid type");
	log_assert(lines, "is NULL");

	struct erw_TypeDeclr* type = erw_scope_findtypedeclr(
		self, 
		node->typedeclr.name->text
	);

	if(type)
	{
		struct Str msg;
		int found = 0;
		for(size_t i = 0; i < erw_TYPEBUILTIN_COUNT; i++)
		{
			if(!strcmp(
				node->typedeclr.name->text, 
				erw_type_builtins[i]->named.name))
			{
				found = 1;	
			}
		}

		if(!found)
		{
			str_ctorfmt(
				&msg,
				"Redefinition of type '%s', declared at line %zu, column %zu", 
				node->typedeclr.name->text,
				type->node->token->linenum,
				type->node->token->column
			);
		}
		else
		{
			str_ctorfmt(
				&msg,
				"Redefinition of builtin type '%s'",
				node->typedeclr.name->text
			);
		}

		erw_error(
			msg.data, 
			lines[node->typedeclr.name->linenum - 1].data, 
			node->typedeclr.name->linenum, 
			node->typedeclr.name->column,
			node->typedeclr.name->column + 
				vec_getsize(node->typedeclr.name->text) - 2
		);
		str_dtor(&msg);
	}

/*
	struct erw_Module* module = erw_scope_findmodule(
		self,
		node->typedeclr.name->text
	);

	if(module)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Redefinition of module '%s', declared at line %zu, column %zu", 
			node->typedeclr.name->text,
			module->node->token->linenum,
			module->node->token->column
		);

		erw_error(
			msg.data, 
			lines[node->typedeclr.name->linenum - 1].data, 
			node->typedeclr.name->linenum, 
			node->typedeclr.name->column,
			node->typedeclr.name->column + 
				vec_getsize(node->typedeclr.name->text) - 2
		);
		str_dtor(&msg);
	}
*/
}

struct erw_TypeDeclr* erw_scope_addtypedeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_TYPEDECLR
			|| node->type == erw_ASTNODETYPE_FOREIGN, 
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_ASTNode* foreign = NULL;
	if(node->type == erw_ASTNODETYPE_FOREIGN)
	{
		log_assert(
			node->foreign.node->type == erw_ASTNODETYPE_TYPEDECLR, 
			"invalid type (%s)", 
			node->foreign.node->type->name
		);

		foreign = node;
		node = node->foreign.node;
	}

	erw_scope_checktypedeclr(self, node, lines);

	struct erw_TypeDeclr symbol;
	symbol.node = foreign ? foreign : node;
	symbol.isforeign = foreign ? 1 : 0;
	symbol.isused = 0;
	symbol.name = node->typedeclr.name->text;
	symbol.type = erw_type_new(erw_TYPEINFO_NAMED, NULL);
	symbol.type->named.name = symbol.name;
	symbol.module = NULL;

	vec_pushback(self->types, symbol);
	struct erw_TypeDeclr* ret = &self->types[vec_getsize(self->types) - 1];
	if(node->typedeclr.type)
	{
		ret->type->named.type = erw_type_create(
			node->typedeclr.type, 
			self, 
			lines
		);
		
		if(ret->type->named.type->info == erw_TYPEINFO_STRUCT)
		{
			for(size_t i = 0; 
				i < vec_getsize(ret->type->named.type->struct_.members); 
				i++)
			{
				if((ret->type->named.type->struct_.members[i].type->info 
					== erw_TYPEINFO_NAMED 
					&& !strcmp(
						ret->type->named.type->struct_.members[i].type->named
							.name,
						symbol.name
					)))
				{
					struct Str msg;
					str_ctorfmt(
						&msg,
						"Recursive definition of type '%s', declared at line"
							" %zu, column %zu", 
						symbol.name,
						symbol.node->typedeclr.name->linenum,
						symbol.node->typedeclr.name->column
					);

					erw_error(
						msg.data, 
						lines[symbol.node->typedeclr.name->linenum - 1].data, 
						symbol.node->typedeclr.name->linenum, 
						symbol.node->typedeclr.name->column,
						symbol.node->typedeclr.name->column + 
							vec_getsize(symbol.node->typedeclr.name->text) - 2
					);
					str_dtor(&msg);
				}
			}
		}
		else if(ret->type->named.type->info == erw_TYPEINFO_UNION)
		{
			for(size_t i = 0; 
				i < vec_getsize(ret->type->named.type->union_.members); 
				i++)
			{
				if((ret->type->named.type->union_.members[i]->info 
					== erw_TYPEINFO_NAMED 
					&& !strcmp(
						ret->type->named.type->union_.members[i]->named.name,
						symbol.name
					)))
				{
					struct Str msg;
					str_ctorfmt(
						&msg,
						"Recursive definition of type '%s', declared at line"
							" %zu, column %zu", 
						symbol.name,
						symbol.node->typedeclr.name->linenum,
						symbol.node->typedeclr.name->column
					);

					erw_error(
						msg.data, 
						lines[symbol.node->typedeclr.name->linenum - 1].data, 
						symbol.node->typedeclr.name->linenum, 
						symbol.node->typedeclr.name->column,
						symbol.node->typedeclr.name->column + 
							vec_getsize(symbol.node->typedeclr.name->text) - 2
					);
					str_dtor(&msg);
				}
			}
		}
	}
	else
	{
		ret->type->named.type = erw_type_new(erw_TYPEINFO_EMPTY, NULL);
		ret->type->named.size = 0; //Should this be 1?
	}

	return ret;
}

static void erw_scope_checkmodule(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_IMPORT, 
		"invalid type: %s",
		node->type->name
	);
	log_assert(lines, "is NULL");

/*
	if(node->import.name)
	{
		struct erw_Module* module = erw_scope_findmodule(
			self,
			node->import.name->text
		);

		if(module)
		{
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Redefinition of module '%s', declared at line %zu, column %zu", 
				node->import.name->text,
				module->node->token->linenum,
				module->node->token->column
			);

			erw_error(
				msg.data, 
				lines[node->import.name->linenum - 1].data, 
				node->import.name->linenum, 
				node->import.name->column,
				node->import.name->column + 
					vec_getsize(node->import.name->text) - 2
			);
			str_dtor(&msg);
		}

		struct erw_TypeDeclr* type = erw_scope_findtypedeclr(
			self, 
			node->typedeclr.name->text
		);

		if(type)
		{
			struct Str msg;
			int found = 0;
			for(size_t i = 0; i < erw_TYPEBUILTIN_COUNT; i++)
			{
				if(!strcmp(
					node->import.name->text, 
					erw_type_builtins[i]->named.name))
				{
					found = 1;	
				}
			}

			if(!found)
			{
				str_ctorfmt(
					&msg,
					"Redefinition of type '%s', declared at line %zu, column "
						"%zu", 
					node->import.name->text,
					type->node->token->linenum,
					type->node->token->column
				);
			}
			else
			{
				str_ctorfmt(
					&msg,
					"Redefinition of builtin type '%s'",
					node->import.name->text
				);
			}

			erw_error(
				msg.data, 
				lines[node->import.name->linenum - 1].data, 
				node->import.name->linenum, 
				node->import.name->column,
				node->import.name->column + 
					vec_getsize(node->import.name->text) - 2
			);
			str_dtor(&msg);
		}
	}
*/
}

static void erw_scope_checktypetemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines
);

static void erw_scope_checkfunctemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines
);

struct erw_Module* erw_scope_addmodule(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_IMPORT
			|| node->type == erw_ASTNODETYPE_FOREIGN, 
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_ASTNode* foreign = NULL;
	if(node->type == erw_ASTNODETYPE_FOREIGN)
	{
		log_assert(
			node->foreign.node->type == erw_ASTNODETYPE_IMPORT, 
			"invalid type (%s)", 
			node->foreign.node->type->name
		);

		foreign = node;
		node = node->foreign.node;
	}

	erw_scope_checkmodule(self, node, lines);

	struct erw_Module symbol;
	symbol.filename = node->import.file->text;
	symbol.node = foreign ? foreign : node;
	symbol.isforeign = foreign ? 1 : 0;
	symbol.name = node->import.name ? node->import.name->text : NULL;

	if(!symbol.isforeign)
	{
		struct Str str;
		char* pathend = strrchr(erw_errorfilename, '/');
		if(pathend)
		{
			str_ctorfmt(
				&str, 
				"%.*s%.*s", 
				(int)(pathend - erw_errorfilename + 1),
				erw_errorfilename,
				(int)vec_getsize(symbol.filename) - 3, 
				symbol.filename + 1
			);
		}
		else
		{
			str_ctorfmt(
				&str, 
				"%.*s", 
				(int)vec_getsize(symbol.filename) - 3, 
				symbol.filename + 1
			);
		}

		symbol.result = erw_compile(str.data);
		if(!symbol.name)
		{
			for(size_t i = 0; 
				i < vec_getsize(symbol.result.scope->functions); 
				i++)
			{
				struct erw_FuncDeclr* found;
				if((found = erw_scope_findfuncdeclr(
					self, 
					symbol.result.scope->functions[i].isforeign 
						? symbol.result.scope->functions[i].node->foreign
							.node->funcdef.name->text 
						: symbol.result.scope->functions[i].node->funcdef
							.name->text,
						1))
					&& symbol.result.scope->functions[i].module 
						&& found->module 
						&& !strcmp(
							symbol.result.scope->functions[i].module,
							found->module)) 
				{ 
					//Duplicate import, do nothing
				}
				else
				{
					erw_scope_checkfuncdeclr(
						self,
						symbol.result.scope->functions[i].isforeign 
							? symbol.result.scope->functions[i].node->foreign
								.node 
							: symbol.result.scope->functions[i].node,
						lines
					);

					if(!symbol.result.scope->functions[i].module)
					{
						symbol.result.scope->functions[i].module 
							= symbol.filename;
					}

					vec_pushback(
						self->functions, 
						symbol.result.scope->functions[i]
					);
				}
			}

			for(size_t i = 0; 
				i < vec_getsize(symbol.result.scope->functemplates); 
				i++)
			{
				struct erw_FuncTemplate* found;
				if((found = erw_scope_findfunctemplate(
					self, 
					symbol.result.scope->functemplates[i].node->template.body
						->funcdef.name->text
					))
					&& symbol.result.scope->functemplates[i].module 
						&& found->module 
						&& !strcmp(
							symbol.result.scope->functemplates[i].module,
							found->module)) 
				{ 
					//Duplicate import, do nothing
				}
				else
				{
					erw_scope_checkfunctemplate(
						self,
						symbol.result.scope->functemplates[i].node,
						lines
					);

					if(!symbol.result.scope->functemplates[i].module)
					{
						symbol.result.scope->functemplates[i].module 
							= symbol.filename;
					}

					symbol.result.scope->functemplates[i].parent = self;
					vec_pushback(
						self->functemplates, 
						symbol.result.scope->functemplates[i]
					);
				}
			}

			for(size_t i = erw_TYPEBUILTIN_COUNT; 
				i < vec_getsize(symbol.result.scope->types); 
				i++)
			{
				struct erw_TypeDeclr* found;
				if((found = erw_scope_findtypedeclr(
					self, 
					symbol.result.scope->types[i].isforeign 
						? symbol.result.scope->types[i].node->foreign
							.node->typedeclr.name->text 
						: symbol.result.scope->types[i].node->typedeclr
							.name->text))
					&& symbol.result.scope->types[i].module 
						&& found->module 
						&& !strcmp(
							symbol.result.scope->types[i].module,
							found->module)) 
				{ 
					//Duplicate import, do nothing
				}
				else
				{
					erw_scope_checktypedeclr(
						self,
						symbol.result.scope->types[i].isforeign 
							? symbol.result.scope->types[i].node->foreign.node 
							: symbol.result.scope->types[i].node, 
						lines
					);

					if(!symbol.result.scope->types[i].module)
					{
						symbol.result.scope->types[i].module = symbol.filename;
					}

					vec_pushback(self->types, symbol.result.scope->types[i]);
				}
			}

			for(size_t i = 0; 
				i < vec_getsize(symbol.result.scope->typetemplates); 
				i++)
			{
				struct erw_TypeTemplate* found;
				if((found = erw_scope_findtypetemplate(
					self, 
					symbol.result.scope->typetemplates[i].node->template.body
						->typedeclr.name->text))
					&& symbol.result.scope->typetemplates[i].module 
						&& found->module 
						&& !strcmp(
							symbol.result.scope->typetemplates[i].module,
							found->module)) 
				{ 
					//Duplicate import, do nothing
				}
				else
				{
					erw_scope_checktypetemplate(
						self,
						symbol.result.scope->typetemplates[i].node, 
						lines
					);

					if(!symbol.result.scope->typetemplates[i].module)
					{
						symbol.result.scope->typetemplates[i].module 
							= symbol.filename;
					}

					symbol.result.scope->typetemplates[i].parent = self;
					vec_pushback(
						self->typetemplates, 
						symbol.result.scope->typetemplates[i]
					);
				}
			}

			for(size_t i = 0; 
				i < vec_getsize(symbol.result.scope->modules); 
				i++)
			{
				erw_scope_checkmodule(
					self,
					symbol.result.scope->modules[i].isforeign
						? symbol.result.scope->modules[i].node->foreign.node
						: symbol.result.scope->modules[i].node,
					lines
				);

				int alreadyimported = 0;
				for(size_t j = 0; j < vec_getsize(self->modules); j++)
				{
					if(!strcmp(
							symbol.result.scope->modules[i].filename,
							self->modules[j].filename) 
						&& !self->modules[j].name)
					{
						alreadyimported = 1;
						break;
					}
				}

				if(!alreadyimported)
				{
					vec_pushback(
						self->modules, 
						symbol.result.scope->modules[i]
					);
				}
			}

			symbol.isused = 1;
		}
		else
		{
			struct erw_Module* module = erw_scope_findmodule(self, symbol.name);
			if(module)
			{
				struct Str msg;
				str_ctorfmt(
					&msg,
					"Redefinition of module ('%s') declared at line %zu,"
						" column %zu", 
					symbol.name,
					symbol.node->token->linenum,
					symbol.node->token->column
				);

				erw_error(
					msg.data, 
					lines[symbol.node->token->linenum - 1].data, 
					symbol.node->token->linenum, 
					symbol.node->token->column,
					symbol.node->token->column + 
						vec_getsize(symbol.node->token->text) - 2
				);
				str_dtor(&msg);
			}
		}

		//str_dtor(&str);
	}
	else
	{
		symbol.isused = 1;
		symbol.result.ast = NULL;
		symbol.result.lines = NULL;
		symbol.result.scope = NULL;
	}

	node->import.moduleindex = vec_getsize(self->modules);
	vec_pushback(self->modules, symbol);
	return &self->modules[vec_getsize(self->modules) - 1];
}

static void erw_scope_checktypetemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_TEMPLATE &&
			node->template.body->type == erw_ASTNODETYPE_TYPEDECLR,
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_TypeTemplate* template = erw_scope_findtypetemplate(
		self,
		node->template.body->typedeclr.name->text
	);

	if(template)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Redefinition of type template ('%s') declared at line %zu, column"
				" %zu", 
			node->template.body->typedeclr.name->text,
			node->template.body->typedeclr.name->linenum,
			node->template.body->typedeclr.name->column
		);

		erw_error(
			msg.data, 
			lines[node->template.body->typedeclr.name->linenum - 1].data, 
			node->template.body->typedeclr.name->linenum, 
			node->template.body->typedeclr.name->column,
			node->template.body->typedeclr.name->column + 
				vec_getsize(node->template.body->typedeclr.name->text) - 2
		);
		str_dtor(&msg);
	}
}

struct erw_TypeTemplate* erw_scope_addtypetemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_TEMPLATE &&
			node->template.body->type == erw_ASTNODETYPE_TYPEDECLR,
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	erw_scope_checktypetemplate(self, node, lines);
	
	struct erw_TypeTemplate template;
	template.generatedtypes = vec_ctor(Vec(struct erw_Type*), 0);
	template.node = node;
	template.name = node->template.body->typedeclr.name->text;
	template.parent = self;
	template.module = NULL;

	vec_pushback(self->typetemplates, template);
	return &self->typetemplates[vec_getsize(self->typetemplates) - 1];
}

static void erw_scope_checkfunctemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node, 
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_TEMPLATE &&
			node->template.body->type == erw_ASTNODETYPE_FUNCDEF,
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_FuncTemplate* template = erw_scope_findfunctemplate(
		self,
		node->template.body->funcdef.name->text
	);

	if(template)
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Redefinition of function template ('%s') declared at line %zu,"
				" column %zu", 
			node->template.body->funcdef.name->text,
			node->template.body->funcdef.name->linenum,
			node->template.body->funcdef.name->column
		);

		erw_error(
			msg.data, 
			lines[node->template.body->funcdef.name->linenum - 1].data, 
			node->template.body->funcdef.name->linenum, 
			node->template.body->funcdef.name->column,
			node->template.body->funcdef.name->column + 
				vec_getsize(node->template.body->funcdef.name->text) - 2
		);
		str_dtor(&msg);
	}
}

struct erw_FuncTemplate* erw_scope_addfunctemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_TEMPLATE &&
			node->template.body->type == erw_ASTNODETYPE_FUNCDEF,
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");
	
	erw_scope_checkfunctemplate(self, node, lines);

	struct erw_FuncTemplate template;
	template.generatedtypes = vec_ctor(Vec(struct erw_Type*), 0);
	template.node = node;
	template.name = node->template.body->funcdef.name->text;
	template.parent = self;
	template.module = NULL;

	vec_pushback(self->functemplates, template);
	return &self->functemplates[vec_getsize(self->functemplates) - 1];
}

static struct erw_ASTNode* erw_internalcalltemplate(
	struct erw_ASTNode* node,
	Vec(struct erw_Token*) types,
	Vec(struct erw_ASTNode*) replacements)
{
	log_assert(node, "is NULL");
	log_assert(types, "is NULL");
	log_assert(replacements, "is NULL");

	struct erw_ASTNode* copy = erw_ast_new(node->type, node->token);
	if(node->type == erw_ASTNODETYPE_FUNCDEF)
	{
		for(size_t i = 0; i < vec_getsize(node->funcdef.params); i++)
		{
			vec_pushback(
				copy->funcdef.params, 
				erw_internalcalltemplate(
					node->funcdef.params[i], 
					types, 
					replacements
				)
			);
		}

		copy->funcdef.isvariadic = node->funcdef.isvariadic;
		copy->funcdef.name = node->funcdef.name; //Copy?
		copy->funcdef.type = erw_internalcalltemplate(
			node->funcdef.type, 
			types, 
			replacements
		);

		copy->funcdef.block = erw_internalcalltemplate(
			node->funcdef.block,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_TYPEDECLR)
	{
		copy->typedeclr.name = node->typedeclr.name;
		copy->typedeclr.type = erw_internalcalltemplate(
			node->typedeclr.type,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_VARDECLR)
	{
		copy->vardeclr.ismut = node->vardeclr.ismut;
		copy->vardeclr.isref = node->vardeclr.isref;
		copy->vardeclr.isrefmut = node->vardeclr.isrefmut;
		copy->vardeclr.isvariadic = node->vardeclr.isvariadic;
		copy->vardeclr.name = node->vardeclr.name;
		copy->vardeclr.type = erw_internalcalltemplate(
			node->vardeclr.type,
			types,
			replacements
		);

		if(node->vardeclr.value)
		{
			copy->vardeclr.value = erw_internalcalltemplate(
				node->vardeclr.value,
				types,
				replacements
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_BLOCK)
	{
		for(size_t i = 0; i < vec_getsize(node->block.stmts); i++)
		{
			vec_pushback(
				copy->block.stmts,
				erw_internalcalltemplate(
					node->block.stmts[i],
					types,
					replacements
				)
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_IF)
	{
		for(size_t i = 0; i < vec_getsize(node->if_.elseifs); i++)
		{
			vec_pushback(
				copy->if_.elseifs,
				erw_internalcalltemplate(
					node->if_.elseifs[i],
					types,
					replacements
				)
			);
		}

		copy->if_.expr = erw_internalcalltemplate(
			node->if_.expr,
			types,
			replacements
		);

		copy->if_.block = erw_internalcalltemplate(
			node->if_.block,
			types,
			replacements
		);

		if(copy->if_.else_)
		{
			copy->if_.else_ = erw_internalcalltemplate(
				node->if_.else_,
				types,
				replacements
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_ELSEIF)
	{
		copy->elseif.expr = erw_internalcalltemplate(
			node->elseif.expr,
			types,
			replacements
		);

		copy->elseif.block = erw_internalcalltemplate(
			node->elseif.block,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_ELSE)
	{
		copy->else_.block = erw_internalcalltemplate(
			node->else_.block,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_RETURN)
	{
		copy->return_.expr = erw_internalcalltemplate(
			node->return_.expr,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_ASSIGNMENT)
	{
		copy->assignment.assignee = erw_internalcalltemplate(
			node->assignment.assignee,
			types,
			replacements
		);

		copy->assignment.expr = erw_internalcalltemplate(
			node->assignment.expr,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_UNEXPR)
	{
		copy->unexpr.mutable = node->unexpr.mutable;
		copy->unexpr.left = node->unexpr.left;
		copy->unexpr.literaltype = node->unexpr.literaltype;
		copy->unexpr.expr = erw_internalcalltemplate(
			node->unexpr.expr,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_BINEXPR)
	{
		copy->binexpr.accesstype = node->binexpr.accesstype;
		copy->binexpr.result = node->binexpr.result;
		copy->binexpr.expr1 = erw_internalcalltemplate(
			node->binexpr.expr1,
			types,
			replacements
		);

		copy->binexpr.expr2 = erw_internalcalltemplate(
			node->binexpr.expr2,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_FUNCCALL)
	{
		copy->funccall.callee = erw_internalcalltemplate(
			node->funccall.callee,
			types,
			replacements
		);

		for(size_t i = 0; i < vec_getsize(node->funccall.args); i++)
		{
			vec_pushback(
				copy->funccall.args, 
				erw_internalcalltemplate(
					node->funccall.args[i],
					types,
					replacements
				)
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_DEFER)
	{
		copy->defer.block = erw_internalcalltemplate(
			node->defer.block,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_UNSAFE)
	{
		copy->unsafe.block = erw_internalcalltemplate(
			node->unsafe.block,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_CAST)
	{
		copy->cast.casttype = node->cast.casttype;
		copy->cast.type = erw_internalcalltemplate(
			node->cast.type,
			types,
			replacements
		);

		copy->cast.expr = erw_internalcalltemplate(
			node->cast.expr,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_WHILE)
	{
		copy->while_.block = erw_internalcalltemplate(
			node->while_.block,
			types,
			replacements
		);

		copy->while_.expr = erw_internalcalltemplate(
			node->while_.expr,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_ENUM)
	{
		for(size_t i = 0; i < vec_getsize(node->enum_.members); i++)
		{
			vec_pushback(
				copy->enum_.members, 
				erw_internalcalltemplate(
					node->enum_.members[i],
					types,
					replacements
				)
			);
		}

		copy->enum_.type = erw_internalcalltemplate(
			node->enum_.type, 
			types, 
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_ENUMMEMBER)
	{
		copy->enummember.name = node->enummember.name;
		copy->enummember.str = node->enummember.str;
		copy->enummember.value = erw_internalcalltemplate(
			node->enummember.value,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_STRUCTMEMBER)
	{
		copy->structmember.name = node->structmember.name;
		copy->structmember.type = erw_internalcalltemplate(
			node->structmember.type,
			types,
			replacements
		);

		copy->structmember.value = erw_internalcalltemplate(
			node->structmember.value,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_STRUCT)
	{
		for(size_t i = 0; i < vec_getsize(node->struct_.members); i++)
		{
			vec_pushback(
				copy->struct_.members, 
				erw_internalcalltemplate(
					node->struct_.members[i],
					types,
					replacements
				)
			);
		}

		copy->struct_.extension = erw_internalcalltemplate(
			node->struct_.extension,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_UNION)
	{
		for(size_t i = 0; i < vec_getsize(node->union_.members); i++)
		{
			vec_pushback(
				copy->union_.members, 
				erw_internalcalltemplate(
					node->union_.members[i],
					types,
					replacements
				)
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_REFERENCE)
	{
		copy->reference.mutable = node->reference.mutable;
		copy->reference.type = erw_internalcalltemplate(
			node->reference.type,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_ARRAY)
	{
		copy->array.size = erw_internalcalltemplate(
			node->array.size,
			types,
			replacements
		);

		copy->array.type = erw_internalcalltemplate(
			node->array.type,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_SLICE)
	{
		copy->slice.mutable = node->slice.mutable;
		copy->slice.type = erw_internalcalltemplate(
			node->slice.type,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_FUNCTYPE)
	{
		for(size_t i = 0; i < vec_getsize(node->functype.params); i++)
		{
			vec_pushback(
				copy->functype.params, 
				erw_internalcalltemplate(
					node->functype.params[i],
					types,
					replacements
				)
			);
		}

		copy->functype.type = erw_internalcalltemplate(
			node->functype.type,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_TYPE) 
	{ 
		for(size_t i = 0; i < vec_getsize(types); i++)
		{
			if(!strcmp(node->token->text, types[i]->text))
			{
				erw_ast_dtor(copy);
				copy = replacements[i];
				break;
			}
		}
	}
	else if(node->type == erw_ASTNODETYPE_LITERAL) { }
	else if(node->type == erw_ASTNODETYPE_ACCESS) 
	{ 
		copy->access.isslice = node->access.isslice;
		copy->access.expr = erw_internalcalltemplate(
			node->access.expr,
			types,
			replacements
		);

		copy->access.index = erw_internalcalltemplate(
			node->access.index,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_STRUCTLITERAL)
	{
		//Assume equal number of names and values
		for(size_t i = 0; i < vec_getsize(node->structliteral.names); i++)
		{
			vec_pushback(
				copy->structliteral.names, 
				node->structliteral.names[i]
			);

			vec_pushback(
				copy->structliteral.values, 
				erw_internalcalltemplate(
					node->structliteral.values[i],
					types,
					replacements
				)
			);
		}

		copy->structliteral.literaltype = node->structliteral.literaltype;
	}
	else if(node->type == erw_ASTNODETYPE_ARRAYLITERAL)
	{
		copy->arrayliteral.literaltype = node->arrayliteral.literaltype;
		copy->arrayliteral.length = erw_internalcalltemplate(
			node->arrayliteral.length,
			types,
			replacements
		);

		for(size_t i = 0; i < vec_getsize(node->arrayliteral.values); i++)
		{
			vec_pushback(
				copy->arrayliteral.values, 
				erw_internalcalltemplate(
					node->arrayliteral.values[i],
					types,
					replacements
				)
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_UNIONLITERAL)
	{
		copy->unionliteral.literaltype = node->unionliteral.literaltype;
		copy->unionliteral.uniontype = node->unionliteral.uniontype;
		copy->unionliteral.type = erw_internalcalltemplate(
			node->unionliteral.type,
			types,
			replacements
		);

		copy->unionliteral.value = erw_internalcalltemplate(
			node->unionliteral.value,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_ENUMLITERAL)
	{
		copy->enumliteral.literaltype = node->enumliteral.literaltype;
		copy->enumliteral.name = node->enumliteral.name;
	}
	else if(node->type == erw_ASTNODETYPE_MATCH)
	{
		copy->match.exprtype = node->match.exprtype;
		copy->match.else_ = erw_internalcalltemplate(
			node->match.else_,
			types,
			replacements
		);

		copy->match.expr = erw_internalcalltemplate(
			node->match.expr,
			types,
			replacements
		);

		for(size_t i = 0; i < vec_getsize(node->match.cases); i++)
		{
			vec_pushback(
				copy->match.cases, 
				erw_internalcalltemplate(
					node->match.cases[i],
					types,
					replacements
				)
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_CASE)
	{
		copy->case_.block = erw_internalcalltemplate(
			node->case_.block,
			types,
			replacements
		);

		copy->case_.expr = erw_internalcalltemplate(
			node->case_.expr,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_FOREIGN)
	{
		copy->foreign.name = node->foreign.name;
		copy->foreign.node = erw_internalcalltemplate(
			node->foreign.node,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_BUILTINCALL)
	{
		copy->builtincall.builtintype = node->builtincall.builtintype;
		copy->builtincall.param = erw_internalcalltemplate(
			node->builtincall.param,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_BREAK) { }
	else if(node->type == erw_ASTNODETYPE_CONTINUE) { }
	else if(node->type == erw_ASTNODETYPE_FUNCLITERAL)
	{
		for(size_t i = 0; i < vec_getsize(node->funcliteral.params); i++)
		{
			vec_pushback(
				copy->funcliteral.params, 
				erw_internalcalltemplate(
					node->funcliteral.params[i],
					types,
					replacements
				)
			);
		}

		copy->funcliteral.isvariadic = node->funcliteral.isvariadic;
		copy->funcliteral.functype = node->funcliteral.functype;
		copy->funcliteral.block = erw_internalcalltemplate(
			node->funcliteral.block,
			types,
			replacements
		);

		copy->funcliteral.type = erw_internalcalltemplate(
			node->funcliteral.type,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_FOREACH)
	{
		copy->foreach.exprtype = node->foreach.exprtype;
		copy->foreach.var = erw_internalcalltemplate(
			node->foreach.var,
			types,
			replacements
		);

		copy->foreach.block = erw_internalcalltemplate(
			node->foreach.block,
			types,
			replacements
		);

		copy->foreach.expr = erw_internalcalltemplate(
			node->foreach.expr,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_FOR)
	{
		copy->for_.block = erw_internalcalltemplate(
			node->for_.block,
			types,
			replacements
		);

		copy->for_.expr = erw_internalcalltemplate(
			node->for_.expr,
			types,
			replacements
		);

		copy->for_.increment = erw_internalcalltemplate(
			node->for_.increment,
			types,
			replacements
		);

		copy->for_.var = erw_internalcalltemplate(
			node->for_.var,
			types,
			replacements
		);
	}
	else if(node->type == erw_ASTNODETYPE_IMPORT)
	{
		copy->import.file = node->import.file;
		copy->import.moduleindex = node->import.moduleindex;
		copy->import.name = node->import.name;
	}
	else if(node->type == erw_ASTNODETYPE_TEMPLATE)
	{
		copy->template.body = erw_internalcalltemplate(
			node->template.body,
			types,
			replacements
		);

		for(size_t i = 0; i < vec_getsize(node->template.types); i++)
		{
			vec_pushback(copy->template.types, node->template.types[i]);
		}
	}
	else if(node->type == erw_ASTNODETYPE_TEMPLATECALL)
	{
		copy->templatecall.body = erw_internalcalltemplate(
			node->templatecall.body,
			types,
			replacements
		);

		for(size_t i = 0; i < vec_getsize(node->templatecall.types); i++)
		{
			vec_pushback(
				copy->templatecall.types, 
				erw_internalcalltemplate(
					node->templatecall.types[i],
					types,
					replacements
				)
			);
		}
	}
	else
	{
		log_assert(0, "This shouldn't happen (%s)'", node->type->name);
		return NULL;
	}

	return copy;
}

static struct erw_ASTNode* erw_calltemplate(
	struct erw_ASTNode* template,
	struct erw_ASTNode* templatecall)
{
	log_assert(template, "is NULL");
	log_assert(templatecall, "is NULL");

	return erw_internalcalltemplate(
		template->template.body, 
		template->template.types,
		templatecall->templatecall.types
	);
}

struct erw_Token* erw_calltypetemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_TEMPLATECALL,
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_Token* nametoken = node->templatecall.body->type
		== erw_ASTNODETYPE_BINEXPR 
			? node->templatecall.body->binexpr.expr2->token
			: node->templatecall.body->token;

	struct erw_TypeTemplate* template = erw_scope_gettypetemplate(
		self,
		nametoken,
		lines
	);

	size_t numargs = vec_getsize(node->templatecall.types);
	if(numargs != vec_getsize(template->node->template.types))
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Expected %zu type arguments to template, got %zu.", 
			vec_getsize(template->node->template.types),
			numargs
		);

		erw_error(
			msg.data, 
			lines[nametoken->linenum - 1].data, 
			nametoken->linenum, 
			nametoken->column,
			nametoken->column + vec_getsize(nametoken->text) - 2
		);
		str_dtor(&msg);
	}

	Vec(struct erw_Type*) types = vec_ctor(struct erw_Type*, numargs);
	for(size_t i = 0; i < numargs; i++)
	{
		vec_pushback(
			types, 
			erw_type_create(node->templatecall.types[i], self, lines)
		);
	}

	int generated = 0;
	for(size_t i = 0; i < vec_getsize(template->generatedtypes); i++)
	{
		int same = 1;
		for(size_t j = 0; j < numargs; j++)
		{
			if(!erw_type_compare(types[j], template->generatedtypes[i][j]))
			{
				same = 0;
				break;
			}
		}

		if(same)
		{
			generated = 1;
			break;
		}
	}

	struct erw_Token* token = malloc(sizeof(struct erw_Token));
	if(!token)
	{
		log_error("malloc failed in <%s>", __FILE__);
	}

	struct Str str;
	str_ctorfmt(&str, "_%s", nametoken->text);

	for(size_t i = 0; i < vec_getsize(types); i++)
	{
		struct Str tmp;
		str_ctor(&tmp, "");
		erw_generatetypename(&tmp, types[i]);
		str_appendfmt(&str, "_%s", tmp.data);
		str_dtor(&tmp);
	}

	token->column = nametoken->column;
	token->linenum = nametoken->linenum;
	token->type = nametoken->type;
	token->text = vec_ctor(char, str.len);
	vec_pushbackwitharr(token->text, str.data, str.len + 1);
	str_dtor(&str);

	if(node->templatecall.body->type == erw_ASTNODETYPE_BINEXPR)
	{
		node->templatecall.body->binexpr.expr2->token = token;
	}
	else
	{
		node->templatecall.body->token = token;
	}

	if(!generated)
	{
		vec_pushback(template->generatedtypes, types);
		struct erw_ASTNode* generation = erw_calltemplate(template->node, node);
		generation->typedeclr.name = token;
		erw_scope_addtypedeclr(template->parent, generation, lines);
		vec_pushback(template->node->template.generations, generation);
		return token;
	}
	else
	{
		return token;
	}
}

void erw_checkfuncdeclr(
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct Str* lines
);

struct erw_Token* erw_callfunctemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_TEMPLATECALL,
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(lines, "is NULL");

	struct erw_Token* nametoken = node->templatecall.body->funccall.callee->type
		== erw_ASTNODETYPE_BINEXPR 
			? node->templatecall.body->funccall.callee->binexpr.expr2->token
			: node->templatecall.body->funccall.callee->token;

	struct erw_FuncTemplate* template = erw_scope_getfunctemplate(
		self,
		nametoken,
		lines
	);

	size_t numargs = vec_getsize(node->templatecall.types);
	if(numargs != vec_getsize(template->node->template.types))
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Expected %zu type arguments to template, got %zu.", 
			vec_getsize(template->node->template.types),
			numargs
		);

		erw_error(
			msg.data, 
			lines[nametoken->linenum - 1].data, 
			nametoken->linenum, 
			nametoken->column,
			nametoken->column + vec_getsize(nametoken->text) - 2
		);
		str_dtor(&msg);
	}

	Vec(struct erw_Type*) types = vec_ctor(struct erw_Type*, numargs);
	for(size_t i = 0; i < numargs; i++)
	{
		vec_pushback(
			types, 
			erw_type_create(node->templatecall.types[i], self, lines)
		);
	}

	int generated = 0;
	for(size_t i = 0; i < vec_getsize(template->generatedtypes); i++)
	{
		int same = 1;
		for(size_t j = 0; j < numargs; j++)
		{
			if(!erw_type_compare(types[j], template->generatedtypes[i][j]))
			{
				same = 0;
				break;
			}
		}

		if(same)
		{
			generated = 1;
			break;
		}
	}

	struct erw_Token* token = malloc(sizeof(struct erw_Token));
	if(!token)
	{
		log_error("malloc failed in <%s>", __FILE__);
	}

	struct Str str;
	str_ctorfmt(&str, "_%s", nametoken->text);

	for(size_t i = 0; i < vec_getsize(types); i++)
	{
		struct Str tmp;
		str_ctor(&tmp, "");
		erw_generatetypename(&tmp, types[i]);
		str_appendfmt(&str, "_%s", tmp.data);
		str_dtor(&tmp);
	}

	token->column = nametoken->column;
	token->linenum = nametoken->linenum;
	token->type = nametoken->type;
	token->text = vec_ctor(char, str.len);
	vec_pushbackwitharr(token->text, str.data, str.len + 1);
	str_dtor(&str);

	if(node->templatecall.body->funccall.callee->type 
		== erw_ASTNODETYPE_BINEXPR)
	{
		node->templatecall.body->funccall.callee->binexpr.expr2->token = token;
	}
	else
	{
		node->templatecall.body->funccall.callee->token = token;
	}

	if(!generated)
	{
		vec_pushback(template->generatedtypes, types);
		struct erw_ASTNode* generation = erw_calltemplate(template->node, node);
		generation->funcdef.name = token;
		erw_checkfuncdeclr(template->parent, generation, lines);
		vec_pushback(template->node->template.generations, generation);
		return token;
	}
	else
	{
		return token;
	}
}

static void erw_scope_printinternal(
	struct erw_Scope* self, 
	size_t level, 
	struct Str* lines)
{
	for(size_t i = 0; i < level; i++)
	{
		printf("    ");
		printf("│");
	}

	if(self->isfunc)
	{
		printf(
			"─ Scope [%zu]: Function [%s]\n", 
			self->index, 
			self->name ? self->name : "null"
		); 
	}
	else
	{
		printf("─ Scope [%zu]: \n", self->index);
	}

	for(size_t i = 0; i < vec_getsize(self->modules); i++)
	{
		for(size_t j = 0; j < level + 1; j++)
		{
			printf("    ");
			printf("│");
		}

		if(self->modules[i].name)
		{
			printf(
				"─ Module: %s (%s)\n", 
				self->modules[i].filename, 
				self->modules[i].name
			);
		}
		else
		{
			printf("─ Module: %s\n", self->modules[i].filename);
		}

		if(!self->modules[i].isforeign)
		{
			erw_scope_printinternal(
				self->modules[i].result.scope, 
				level + 2, 
				lines
			);
		}
	}

	for(size_t i = 0; i < vec_getsize(self->typetemplates); i++)
	{
		for(size_t j = 0; j < level + 1; j++)
		{
			printf("    ");
			printf("│");
		}

		printf("─ Type Template: %s\n", self->typetemplates[i].name);
	}

	for(size_t i = erw_TYPEBUILTIN_COUNT; i < vec_getsize(self->types); i++)
	{
		//if(!self->types[i].module)
		{
			for(size_t j = 0; j < level + 1; j++)
			{
				printf("    ");
				printf("│");
			}

			struct Str str = erw_type_tostring(self->types[i].type->named.type);
			printf(
				"─ Type: %s (%s)\n", 
				self->types[i].type->named.name, 
				str.data
			);

			if(self->types[i].type->named.type->info == erw_TYPEINFO_STRUCT)
			{
				for(size_t j = 0; 
					j < vec_getsize(
						self->types[i].type->named.type->struct_.members
					);
					j++)
				{
					for(size_t k = 0; k < level + 2; k++)
					{
						printf("    ");
						printf("│");
					}

					struct Str str1 = erw_type_tostring(
						self->types[i].type->named.type->struct_.members[j].type
					);
					printf(
						"─ Struct member: %s (%s)\n", 
						self->types[i].type->named.type->struct_.members[j]
							.name,
						str1.data
					);
					str_dtor(&str1);
				}
			}
			else if(self->types[i].type->named.type->info == erw_TYPEINFO_UNION)
			{
				for(size_t j = 0; 
					j < vec_getsize(
						self->types[i].type->named.type->union_.members
					);
					j++)
				{
					for(size_t k = 0; k < level + 2; k++)
					{
						printf("    ");
						printf("│");
					}

					struct Str str1 = erw_type_tostring(
						self->types[i].type->named.type->union_.members[j]
					);

					printf("─ Union member: %s\n", str1.data);
					str_dtor(&str1);
				}
			}
			else if(self->types[i].type->named.type->info == erw_TYPEINFO_ENUM)
			{
				for(size_t j = 0; 
					j < vec_getsize(self->types[i].type->named.type->enum_
						.members);
					j++)
				{
					for(size_t k = 0; k < level + 2; k++)
					{
						printf("    ");
						printf("│");
					}

					
					printf(
						"─ Enum member: %s (%zu)\n", 
						self->types[i].type->named.type->enum_.members[j].name,
						self->types[i].type->named.type->enum_.members[j].value
					);
				}
			}

			str_dtor(&str);
		}
	}

	for(size_t i = 0; i < vec_getsize(self->functemplates); i++)
	{
		for(size_t j = 0; j < level + 1; j++)
		{
			printf("    ");
			printf("│");
		}

		printf("─ Function Template: %s\n", self->functemplates[i].name);
	}

	for(size_t i = 0; i < vec_getsize(self->functions); i++)
	{
		//if(!self->functions[i].module)
		{
			for(size_t j = 0; j < level + 1; j++)
			{
				printf("    ");
				printf("│");
			}

			if(self->functions[i].type)
			{
				struct Str str = erw_type_tostring(self->functions[i].type);
				printf(
					"─ Function: %s (%s)\n", 
					self->functions[i].name, str.data
				);

				str_dtor(&str);
			}
			else
			{
				printf("─ Function: %s\n", self->functions[i].name);
			}
		}
	}

	for(size_t i = 0; i < vec_getsize(self->variables); i++)
	{
		for(size_t j = 0; j < level + 1; j++)
		{
			printf("    ");
			printf("│");
		}

		struct Str str = erw_type_tostring(self->variables[i].type);
		printf("─ Variable: %s (%s)\n", self->variables[i].name, str.data);
		str_dtor(&str);
	}

	for(size_t i = 0; i < vec_getsize(self->children); i++)
	{
		erw_scope_printinternal(self->children[i], level + 1, lines);
	}
}

void erw_scope_print(struct erw_Scope* self, struct Str* lines)
{
	log_assert(self, "is NULL");
	log_assert(lines, "is NULL");

	erw_scope_printinternal(self, 0, lines);
}

void erw_scope_dtor(struct erw_Scope* self)
{
	log_assert(self, "is NULL");

	for(size_t i = 0; i < vec_getsize(self->types); i++)
	{
		if(self->types[i].node) //Check if type is builtin
		{
			erw_type_dtor(self->types[i].type);
		}
	}

	vec_dtor(self->types);
	for(size_t i = 0; i < vec_getsize(self->functions); i++)
	{
		erw_type_dtor(self->functions[i].type);
		if(self->functions[i].rettype)
		{
			erw_type_dtor(self->functions[i].rettype);
		}

		for(size_t j = 0; j < vec_getsize(self->functions[i].params); j++)
		{
			free(self->functions[i].params[j]);
		}

		vec_dtor(self->functions[i].params);
	}

	vec_dtor(self->functions);
	for(size_t i = 0; i < vec_getsize(self->variables); i++)
	{
		erw_type_dtor(self->variables[i].type);
	}

	vec_dtor(self->variables);
	for(size_t i = 0; i < vec_getsize(self->children); i++)
	{
		erw_scope_dtor(self->children[i]);
	}

	for(size_t i = 0; i < vec_getsize(self->typetemplates); i++)
	{
		vec_dtor(self->typetemplates[i].generatedtypes);
	}

	vec_dtor(self->typetemplates);
	for(size_t i = 0; i < vec_getsize(self->functemplates); i++)
	{
		vec_dtor(self->functemplates[i].generatedtypes);
	}

	vec_dtor(self->functemplates);
	vec_dtor(self->children);
	free(self);
}

