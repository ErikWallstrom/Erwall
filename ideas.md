* Nested comments

```erw
#[
	This too
	#[
		# This is a comment
		Also a comment
	#]
#]
```

* Nested functions

```erw
func test: ()
{
	func nested: ()
	{
		
	}
}
```

* Tagged unions

```erw
type Result: union[&Int32, None];
let result: Result = union[None];
match(result)
{
	case(let r: &Int32)
	{
		
	}
	else
	{
		
	}
}
```

* Strong typing

```erw
type Int: Int32;
let x: Int32 = 12;
let y: Int = x; # Error
```

* First class arrays

```erw
func test: () -> [5]Int32
{
	return array[5, 10, 15, 15, 10];
}

let numbers: [5]Int32 = test();
```

* Sized arrays/slices

```erw
let arr: [3]Int32 = array[0, 2, 13];
let slice: []Int32 = &arr;
let size: Int32 = len_of(slice);
```

* Non-null pointers (references)

```erw
let x: Int32 = 10;
let x_ref: &Int32 = &x;
```

* Mutable reference

```erw
mut x: Int32 = 10;
let x_ref: &mut Int32 = &mut x;
```

* Switch for all types

```erw
match("hello")
{
	case("hi")
	{
		
	}
	case("hello")
	{
		
	}
	case("hey")
	{
		
	}
}
```

* Named enums

```erw
type MyEnum: enum[monday("Monday") = 1];
let x: []Char = enum_name(MyEnum.monday);
```

* Enum of different type

```erw
type MyEnum: enum([]Char)[monday("Monday") = "Montag"];
let x: []Char = enum_value(MyEnum.monday);
```

* Anonymous functions

```erw
let f: func() = func(){puts("Hello World");};
set_callback(
	func(let value: Int32)
	{
		print_int32(value);
	}
);
```

* Struct composition

```erw
type A: struct[x: Int32, y: Int32];
type B: struct[extend A, w: Int32, h: Int32];

let b: B = struct[x: 42, y: 10, w: 2, h: 33];
let a: A = b;
```

* Anonymous structs and unions

```erw
let s: struct[a: Float32, b: Float32] = struct[a: 42.0, b: 3.14];
let s2: struct[a: Float32, b: Float32] = s;
```

* Builtin BigInt support

```erw
let num1: BigInt = 12312731298371298371209371209381203987120983712098312312312;
let num2: BigInt = 123012837123^1231;

let num3 = num1 + num2;
```

* Syntax sugar for unions

```erw
type Rank: union[
	type Jack, 
	type Queen, 
	type King, 
	type Numeric: struct[n: Int32]
];

@ Same as
type Jack;
type Queen;
type King;
type Numeric: struct[n: Int32];

type Rank: union[Jack, Queen, King, Numeric];
```

* Defer block
* Temorary storage
* Unsafe block

