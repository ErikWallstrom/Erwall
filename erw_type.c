#include "erw_type.h"
#include "erw_error.h"
#include "erw_scope.h"
#include "log.h"
#include <stdlib.h>
#include <limits.h>

struct erw_Type* const erw_type_builtins[] = {
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "None",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_EMPTY
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Char",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_CHAR
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Bool",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_BOOL
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Int8",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 1,
			.int_.issigned = 1
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Int16",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 2,
			.int_.issigned = 1
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Int32",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 4,
			.int_.issigned = 1
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Int64",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 8,
			.int_.issigned = 1
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "UInt8",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 1,
			.int_.issigned = 0
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "UInt16",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 2,
			.int_.issigned = 0
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "UInt32",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 4,
			.int_.issigned = 0
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "UInt64",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_INT,
			.int_.size = 8,
			.int_.issigned = 0
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Float32",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_FLOAT,
			.size = 4
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "Float64",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_FLOAT,
			.size = 8
		},
	},
	&(struct erw_Type){
		.info = erw_TYPEINFO_NAMED,
		.named.name = "BigInt",
		.named.module = NULL,
		.named.type = &(struct erw_Type){
			.info = erw_TYPEINFO_BIGINT,
			.size = sizeof(void*)
		},
	},
};

struct erw_Type* erw_type_new(enum erw_TypeInfo info, struct erw_Type* parent)
{
	log_assert(info < erw_TYPEINFO_COUNT, "invalid info (%i)", info);

	struct erw_Type* self = calloc(1, sizeof(struct erw_Type));
	if(!self)
	{
		log_error("calloc failed, in <%s>", __func__);
	}

	if(info == erw_TYPEINFO_STRUCT)
	{
		self->struct_.members = vec_ctor(struct erw_TypeStructMember, 0);
	}
	else if(info == erw_TYPEINFO_ENUM)
	{
		self->enum_.members = vec_ctor(struct erw_TypeEnumMember, 0);
	}
	else if(info == erw_TYPEINFO_UNION)
	{
		self->union_.members = vec_ctor(struct erw_Type*, 0);
	}
	else if(info == erw_TYPEINFO_FUNC)
	{
		self->func.params = vec_ctor(struct erw_Type*, 0);
	}

	self->parent = parent;
	self->info = info;

	return self;
}

void erw_checkexprtype( //From erw_semantics.c
	struct erw_Scope* scope, 
	struct erw_ASTNode* node, 
	struct erw_Type* type,
	struct Str* lines
);

struct erw_Type* erw_type_create(
	struct erw_ASTNode* node, 
	struct erw_Scope* scope,
	struct Str* lines)
{
	log_assert(node, "is NULL");
	log_assert(scope, "is NULL");

	struct erw_Type* type = NULL;
	if(node->type == erw_ASTNODETYPE_TYPE)
	{
		type = erw_type_new(erw_TYPEINFO_NAMED, NULL);
		struct erw_TypeDeclr* typedeclr = erw_scope_gettypedeclr(
			scope, 
			node->token, 
			lines
		);

		type->named.name = node->token->text;
		type->named.type = typedeclr->type;
		type->named.size = typedeclr->type->size;
	}
	else if(node->type == erw_ASTNODETYPE_REFERENCE)
	{
		type = erw_type_new(erw_TYPEINFO_REFERENCE, NULL);
		type->reference.ismut = node->reference.mutable;
		type->reference.size = sizeof(void*);
		type->reference.type = erw_type_create(
			node->reference.type, 
			scope, 
			lines
		);
	}
	else if(node->type == erw_ASTNODETYPE_SLICE)
	{
		type = erw_type_new(erw_TYPEINFO_SLICE, NULL);
		type->slice.ismut = node->slice.mutable;
		type->slice.size = sizeof(void*);
		type->slice.type = erw_type_create(
			node->slice.type, 
			scope, 
			lines
		);
	}
	else if(node->type == erw_ASTNODETYPE_STRUCT)
	{
		type = erw_type_new(erw_TYPEINFO_STRUCT, NULL);
		if(node->struct_.extension)
		{
			type->struct_.extensiontype = erw_type_create(
				node->struct_.extension, 
				scope, 
				lines
			);

			struct erw_Type* base = erw_type_getbase(
				type->struct_.extensiontype
			);

			if(base->info != erw_TYPEINFO_STRUCT)
			{
				struct Str msg;
				str_ctor(&msg, "Extension must be of struct type");
				erw_error(
					msg.data, 
					lines[node->struct_.extension->token->linenum - 1].data, 
					node->struct_.extension->token->linenum, 
					node->struct_.extension->token->column,
					node->struct_.extension->token->column 
						+ vec_getsize(node->struct_.extension->token->text) - 2
				);
				str_dtor(&msg);
			}

			for(size_t i = 0; i < vec_getsize(base->struct_.members); i++)
			{
				struct erw_TypeStructMember member = {
					.isextension = 1,
					.name = base->struct_.members[i].name,
					.type = base->struct_.members[i].type
				};

				vec_pushback(type->struct_.members, member);
			}
		}

		for(size_t i = 0; i < vec_getsize(node->struct_.members); i++)
		{
			struct erw_TypeStructMember member = {
				.name = node->struct_.members[i]->vardeclr.name->text,
				.type = erw_type_create(
					node->struct_.members[i]->vardeclr.type,
					scope,
					lines
				)
			};

			for(size_t j = 0; j < vec_getsize(type->struct_.members); j++)
			{
				if(!strcmp(type->struct_.members[j].name, member.name))
				{
					struct Str msg;
					if(!type->struct_.members[j].isextension)
					{
						str_ctorfmt(
							&msg,
							"Redefinition of struct member ('%s') declared at"
								" line %zu, column %zu", 
							member.name,
							node->struct_.members[j]->vardeclr.name->linenum,
							node->struct_.members[j]->vardeclr.name->column
						);
					}
					else
					{
						str_ctorfmt(
							&msg,
							"Redefinition of struct member ('%s') inherited"
								" from extension",
							member.name
						);
					}

					erw_error(
						msg.data, 
						lines[
							node->struct_.members[i]->vardeclr.name->linenum - 1
						].data, 
						node->struct_.members[i]->vardeclr.name->linenum, 
						node->struct_.members[i]->vardeclr.name->column,
						node->struct_.members[i]->vardeclr.name->column 
							+ vec_getsize(
								node->struct_.members[i]->vardeclr.name->text) 
							- 2
					);
					str_dtor(&msg);
				}
			}
			
			type->struct_.size += member.type->size; //Not accurate (no padding)
			vec_pushback(type->struct_.members, member);
		}
	}
	else if(node->type == erw_ASTNODETYPE_ENUM)
	{
		type = erw_type_new(erw_TYPEINFO_ENUM, NULL);
		if(node->enum_.type)
		{
			type->enum_.type = erw_type_create(node->enum_.type, scope, lines);
		}

		type->enum_.size = type->enum_.type 
			? type->enum_.type->size 
			: sizeof(int); //Maybe Int32?
		
		size_t defaultvalue = 0;
		for(size_t i = 0; i < vec_getsize(node->enum_.members); i++)
		{
			struct erw_TypeEnumMember member;
			member.name = node->enum_.members[i]->enummember.name->text;

			for(size_t j = 0; j < vec_getsize(type->enum_.members); j++)
			{
				if(!strcmp(type->enum_.members[j].name, member.name))
				{
					struct Str msg;
					str_ctorfmt(
						&msg,
						"Redefinition of enum member ('%s') declared at line"
							" %zu, column %zu", 
						member.name,
						node->enum_.members[j]->enummember.name->linenum,
						node->enum_.members[j]->enummember.name->column
					);

					erw_error(
						msg.data, 
						lines[
							node->enum_.members[i]->enummember.name->linenum - 1
						].data, 
						node->enum_.members[i]->enummember.name->linenum, 
						node->enum_.members[i]->enummember.name->column, 
						node->enum_.members[i]->enummember.name
							->column + vec_getsize(
								node->enum_.members[i]->enummember.name->text) 
							- 2
					);

					str_dtor(&msg);
				}

				//TODO: Check for duplicate values
			}

			if(type->enum_.type)
			{
				erw_checkexprtype(
					scope, 
					node->enum_.members[i]->enummember.value,
					type->enum_.type,
					lines
				);
			}
			else
			{
				if(node->enum_.members[i]->enummember.value)
				{
					size_t value = 0;
					sscanf(
						node->enum_.members[i]->enummember.value->token->text, 
						"%zu", 
						&value
					);

					if(value < defaultvalue) //Should be allowed happen?
					{
						struct Str msg;
						str_ctorfmt(
							&msg,
							"Too small value given to enum member '%s'."
								" Expected at least '%zu'",
							member.name,
							defaultvalue
						);

						erw_error(
							msg.data, 
							lines[node->enum_.members[i]
								->enummember.name->linenum - 1].data, 
							node->enum_.members[i]->enummember.name->linenum, 
							node->enum_.members[i]->enummember.name->column, 
							node->enum_.members[i]->enummember.name->column 
								+ vec_getsize(
									node->enum_.members[i]->enummember.name
										->text) - 2
						);

						str_dtor(&msg);
					}

					defaultvalue = value;
				}

				member.value = defaultvalue;
				defaultvalue++;
			}

			vec_pushback(type->enum_.members, member);
		}
	}
	else if(node->type == erw_ASTNODETYPE_UNION)
	{
		type = erw_type_new(erw_TYPEINFO_UNION, NULL);
		size_t largestsize = 0;
		for(size_t i = 0; i < vec_getsize(node->union_.members); i++)
		{
			struct erw_Type* membertype = erw_type_create(
				node->union_.members[i],
				scope,
				lines
			);

			for(size_t j = 0; j < vec_getsize(type->union_.members); j++)
			{
				if(erw_type_compare(membertype, type->union_.members[j]))
				{
					struct Str msg;
					struct Str str = erw_type_tostring(membertype);
					str_ctorfmt(
						&msg,
						"Redefinition of union member ('%s') declared at line"
							" %zu, column %zu", 
						str.data,
						node->union_.members[j]->token->linenum,
						node->union_.members[j]->token->column
					);

					erw_error(
						msg.data, 
						lines[node->union_.members[i]->token->linenum - 1].data, 
						node->union_.members[i]->token->linenum, 
						node->union_.members[i]->token->column,
						node->union_.members[i]->token->column 
							+ vec_getsize(
								node->union_.members[i]->token->text) - 2
					);

					str_dtor(&str);
					str_dtor(&msg);
				}
			}

			if(membertype->size > largestsize)
			{
				largestsize = membertype->size;
			}

			vec_pushback(type->union_.members, membertype);
		}
	}
	else if(node->type == erw_ASTNODETYPE_ARRAY)
	{
		type = erw_type_new(erw_TYPEINFO_ARRAY, NULL);
		type->array.type = erw_type_create(
			node->array.type, 
			scope, 
			lines
		);

		char* endptr = node->array.size->token->text;
		unsigned long len = strtoul(node->array.size->token->text, &endptr, 10);
		if(*endptr != '\0')
		{
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Length cannot exceed %lu",
				LONG_MAX
			);

			erw_error(
				msg.data, 
				lines[node->array.size->token->linenum - 1].data,
				node->array.size->token->linenum, 
				node->array.size->token->column,
				node->array.size->token->column + 
					vec_getsize(node->array.size->token->text) - 2
			);
			str_dtor(&msg);
		}

		type->array.numelements = len;
		type->array.size = len * type->array.type->size;
	}
	else if(node->type == erw_ASTNODETYPE_FUNCTYPE)
	{
		type = erw_type_new(erw_TYPEINFO_FUNC, NULL);
		type->func.size = sizeof(void(*)(void));
		type->func.rettype = node->functype.type 
			? erw_type_create(node->functype.type, scope, lines)
			: NULL;

		for(size_t i = 0; i < vec_getsize(node->functype.params); i++)
		{
			vec_pushback(
				type->func.params, 
				erw_type_create(node->functype.params[i], scope, lines)
			);
		}
	}
	else if(node->type == erw_ASTNODETYPE_BINEXPR)
		//Module access
	{
		struct erw_Module* module = erw_scope_getmodule(
			scope, 
			node->binexpr.expr1->token, 
			lines
		);

		type = erw_type_new(erw_TYPEINFO_NAMED, NULL);
		struct erw_TypeDeclr* typedeclr = erw_scope_gettypedeclr(
			module->result.scope, 
			node->binexpr.expr2->token, 
			lines
		);

		type->named.name = node->binexpr.expr2->token->text;
		type->named.type = typedeclr->type;
		type->named.size = typedeclr->type->size;
		type->named.module = module->name;
	}
	else if(node->type == erw_ASTNODETYPE_TEMPLATECALL)
	{
		type = erw_type_new(erw_TYPEINFO_NAMED, NULL);

		struct erw_Scope* callscope;
		if(node->templatecall.body->type == erw_ASTNODETYPE_BINEXPR)
		{
			struct erw_Module* module = erw_scope_getmodule(
				scope, 
				node->templatecall.body->binexpr.expr1->token,
				lines
			);

			callscope = module->result.scope;
			type->named.module = module->name;
		}
		else
		{
			callscope = scope;
		}

		struct erw_Token* name = erw_calltypetemplate(callscope, node, lines);
		struct erw_TypeDeclr* typedeclr = erw_scope_gettypedeclr(
			callscope, 
			name, 
			lines
		);

		type->named.name = name->text;
		type->named.type = typedeclr->type;
		type->named.size = typedeclr->type->size;
	}
	else
	{
		log_assert(0, "Unhandled type: %s", node->type->name);
	}

	return type;
}

struct Str erw_type_tostring(struct erw_Type* type)
{
	log_assert(type, "is NULL");

	struct Str str;
	str_ctor(&str, "");
	while(type->info != erw_TYPEINFO_NAMED 
		&& type->info != erw_TYPEINFO_STRUCT
		&& type->info != erw_TYPEINFO_UNION
		&& type->info != erw_TYPEINFO_ENUM
		&& type->info != erw_TYPEINFO_CHAR
		&& type->info != erw_TYPEINFO_BOOL
		&& type->info != erw_TYPEINFO_INT
		&& type->info != erw_TYPEINFO_BIGINT
		&& type->info != erw_TYPEINFO_FLOAT
		&& type->info != erw_TYPEINFO_EMPTY)
	{
		if(type->info == erw_TYPEINFO_REFERENCE)
		{
			str_append(&str, "&");
			if(type->reference.ismut)
			{
				str_append(&str, "mut ");
			}

			type = type->reference.type;
		}
		else if(type->info == erw_TYPEINFO_SLICE)
		{
			str_append(&str, "[]");
			if(type->slice.ismut)
			{
				str_append(&str, "mut ");
			}

			type = type->slice.type;
		}
		else if(type->info == erw_TYPEINFO_ARRAY)
		{
			str_appendfmt(&str, "[%zu]", type->array.numelements);
			type = type->array.type;
		}
		else if(type->info == erw_TYPEINFO_FUNC)
		{
			str_append(&str, "func(");
			for(size_t i = 0; i < vec_getsize(type->func.params); i++)
			{	
				struct Str tmp = erw_type_tostring(type->func.params[i]);
				str_append(&str, tmp.data);
				str_dtor(&tmp);

				if(i < vec_getsize(type->func.params) - 1)
				{
					str_append(&str, ", ");
				}
			}

			str_append(&str, ")");
			
			if(type->func.rettype)
			{
				struct Str tmp = erw_type_tostring(type->func.rettype);
				str_appendfmt(&str, " -> %s", tmp.data);
				str_dtor(&tmp);
			}
			break;
		}
		else
		{
			log_assert(0, "Unhandled type: %i", type->info);
		}
	}

	if(type->info == erw_TYPEINFO_NAMED)
	{
		str_append(&str, type->named.name);
	}
	else if(type->info == erw_TYPEINFO_STRUCT)
	{
		str_append(&str, "struct[");
		if(type->struct_.extensiontype)
		{
			struct Str extname = erw_type_tostring(type->struct_.extensiontype);
			str_appendfmt(&str, "extends %s, ", extname.data);
			str_dtor(&extname);
		}

		for(size_t i = 0; i < vec_getsize(type->struct_.members); i++)
		{
			if(!type->struct_.members[i].isextension)
			{
				struct Str typename = erw_type_tostring(
					type->struct_.members[i].type
				);

				str_appendfmt(
					&str, 
					"%s: %s", 
					type->struct_.members[i].name, 
					typename.data
				);
				str_dtor(&typename);

				if(i < vec_getsize(type->struct_.members) - 1)
				{
					str_append(&str, ", ");
				}
			}
		}

		str_append(&str, "]");
	}
	else if(type->info == erw_TYPEINFO_ENUM)
	{
		str_append(&str, "enum"); //TODO: Improve this
	}
	else if(type->info == erw_TYPEINFO_UNION)
	{
		str_append(&str, "union"); //TODO: Improve this
	}
	else if(type->info == erw_TYPEINFO_FUNC) { }
	else if(type->info == erw_TYPEINFO_CHAR)
	{
		str_append(&str, "Builtin Char");
	}
	else if(type->info == erw_TYPEINFO_BOOL)
	{
		str_append(&str, "Builtin Bool");
	}
	else if(type->info == erw_TYPEINFO_INT)
	{
		str_append(&str, "Builtin ");
		if(!type->int_.issigned)
		{
			str_append(&str, "U");
		}

		str_appendfmt(&str, "Int%zu", type->int_.size * 8);
	}
	else if(type->info == erw_TYPEINFO_FLOAT)
	{
		str_append(&str, "Builtin Float");
	}
	else if(type->info == erw_TYPEINFO_EMPTY)
	{
		str_append(&str, "Builtin Empty");
	}
	else if(type->info == erw_TYPEINFO_BIGINT)
	{
		str_append(&str, "Builtin BigInt");
	}
	else
	{
		log_assert(0, "Unhandled type: %i", type->info);
	}

	return str;
}

int erw_type_compare(struct erw_Type* type1, struct erw_Type* type2)
{
	log_assert(type1, "is NULL");
	log_assert(type2, "is NULL");

	if(type1->info != type2->info)
	{
		return 0;
	}
	else if(type1->info == erw_TYPEINFO_NAMED)
	{
		if(!strcmp(type1->named.name, type2->named.name) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else if(type1->info == erw_TYPEINFO_REFERENCE)
	{
		if(type1->reference.ismut != type2->reference.ismut)
		{
			return 0;
		}
		else
		{
			return erw_type_compare(
				type1->reference.type, 
				type2->reference.type
			);
		}
	}
	else if(type1->info == erw_TYPEINFO_SLICE)
	{
		if(type1->slice.ismut != type2->slice.ismut)
		{
			return 0;
		}
		else
		{
			return erw_type_compare(type1->slice.type, type2->slice.type);
		}
	}
	else if(type1->info == erw_TYPEINFO_STRUCT)
	{
		size_t nummemb1 = vec_getsize(type1->struct_.members);
		size_t nummemb2 = vec_getsize(type2->struct_.members);
		if(nummemb1 != nummemb2)
		{
			return 0;
		}

		struct erw_Type* ext1 = type1->struct_.extensiontype;
		struct erw_Type* ext2 = type2->struct_.extensiontype;

		if(ext1 && ext2)
		{
			if(!erw_type_compare(ext1, ext2))
			{
				return 0;
			}
		}
		else if(ext1 || ext2)
		{
			return 0;
		}

		for(size_t i = 0; i < nummemb1; i++)
		{
			int result1 = erw_type_compare(
				type1->struct_.members[i].type, 
				type2->struct_.members[i].type
			);

			int result2 = !strcmp(
				type1->struct_.members[i].name, 
				type2->struct_.members[i].name
			);

			if(!result1 || !result2)
			{
				return 0;
			}
		}

		return 1;
	}
	else if(type1->info == erw_TYPEINFO_UNION)
	{
		size_t nummemb1 = vec_getsize(type1->union_.members);
		size_t nummemb2 = vec_getsize(type2->union_.members);
		if(nummemb1 != nummemb2)
		{
			return 0;
		}
		
		for(size_t i = 0; i < nummemb1; i++)
		{
			int result = erw_type_compare(
				type1->union_.members[i], 
				type2->union_.members[i]
			);

			if(!result)
			{
				return 0;
			}
		}

		return 1;
	}
	else if(type1->info == erw_TYPEINFO_ARRAY)
	{
		if(type1->array.numelements != type2->array.numelements)
		{
			return 0;
		}
		else
		{
			return erw_type_compare(type1->array.type, type2->array.type);
		}
	}
	else if(type1->info == erw_TYPEINFO_FUNC)
	{
		size_t numparams1 = vec_getsize(type1->func.params);
		size_t numparams2 = vec_getsize(type2->func.params);
		if(numparams1 != numparams2)
		{
			return 0;
		}

		for(size_t i = 0; i < numparams1; i++)
		{
			int result = erw_type_compare(
				type1->func.params[i], 
				type2->func.params[i]
			);

			if(!result)
			{
				return 0;
			}
		}

		if(type1->func.rettype || type2->func.rettype)
		{
			if(type1->func.rettype && type2->func.rettype)
			{
				return erw_type_compare(
					type1->func.rettype, 
					type2->func.rettype
				);
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 1;
		}
	}
	else
	{
		log_assert(0, "Unhandled type: %s", erw_type_tostring(type1).data);
		return 0;
	}
}

int erw_type_compatible(struct erw_Type* type1, struct erw_Type* type2)
{
	log_assert(type1, "is NULL");
	log_assert(type2, "is NULL");

	struct erw_Type* base1 = erw_type_getbase(type1);
	struct erw_Type* base2 = erw_type_getbase(type2);
	if(type1->info == erw_TYPEINFO_SLICE && type2->info 
		== erw_TYPEINFO_REFERENCE)
	{
		if(type2->reference.type->info == erw_TYPEINFO_ARRAY)
		{
			if(erw_type_compatible(
				type1->slice.type, 
				type2->reference.type->array.type))
			{
				return 1;
			}
		}
	}
	else if(type1->info == erw_TYPEINFO_NAMED 
		&& type2->info == erw_TYPEINFO_NAMED 
		&& base2->info == erw_TYPEINFO_STRUCT)
	{
		if(base2->struct_.extensiontype)
		{
			if(erw_type_compatible(type1, base2->struct_.extensiontype))
			{
				return 1;
			}
		}
	}
	/*
	else if(type1->info == erw_TYPEINFO_NAMED
		&& type2->info == erw_TYPEINFO_STRUCT)
	{
		return erw_type_compatible(erw_type_getbase(type1), type2);
	}
	else if(type1->info == erw_TYPEINFO_NAMED
		&& type2->info == erw_TYPEINFO_UNION)
	{
		return erw_type_compatible(erw_type_getbase(type1), type2);
	}
	*/
	else if(type1->info == erw_TYPEINFO_REFERENCE 
		&& type2->info == erw_TYPEINFO_REFERENCE)
	{
		if(erw_type_compatible(type1->reference.type, type2->reference.type))
		{
			if(type1->reference.ismut && !type2->reference.ismut)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	else if(type1->info == erw_TYPEINFO_SLICE
		&& type2->info == erw_TYPEINFO_SLICE)
	{
		if(erw_type_compatible(type1->slice.type, type2->slice.type))
		{
			if(type1->slice.ismut && !type2->slice.ismut)
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
	}
	else if(base1->info == erw_TYPEINFO_BOOL 
		&& type2->info == erw_TYPEINFO_BOOL)
	{
		return 1;
	}
	else if(base1->info == erw_TYPEINFO_FLOAT 
		&& type2->info == erw_TYPEINFO_FLOAT)
	{
		return 1;
	}
	else if(base1->info == erw_TYPEINFO_CHAR 
		&& type2->info == erw_TYPEINFO_CHAR)
	{
		return 1;
	}
	else if(base1->info == erw_TYPEINFO_BIGINT 
		&& type2->info == erw_TYPEINFO_BIGINT)
	{
		return 1;
	}
	else if(base1->info == erw_TYPEINFO_INT 
		&& type2->info == erw_TYPEINFO_INT)
	{
		if(base1->int_.size > type2->int_.size)
		{
			return 1;
		}
		else if(base1->int_.size == type2->int_.size 
			&& base1->int_.issigned <= type2->int_.issigned)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	return erw_type_compare(type1, type2);
}

struct erw_Type* erw_type_getbase(struct erw_Type* type)
{
	log_assert(type, "is NULL");

	struct erw_Type* base = type;
	while(base->info == erw_TYPEINFO_NAMED)
	{
		base = base->named.type;
	}

	return base;
}

struct erw_Type* erw_type_getnamed(struct erw_Type* type)
{
	log_assert(type, "is NULL");

	if(type->info == erw_TYPEINFO_NAMED)
	{
		return type;
	}
	else if(type->info == erw_TYPEINFO_REFERENCE)
	{
		return erw_type_getnamed(type->reference.type);
	}
	else if(type->info == erw_TYPEINFO_SLICE)
	{
		return erw_type_getnamed(type->slice.type);
	}
	else if(type->info == erw_TYPEINFO_ARRAY)
	{
		return erw_type_getnamed(type->array.type);
	}
	else
	{
		return NULL;
	}
}

void erw_generatetypename(struct Str* str, struct erw_Type* type)
{
	log_assert(str, "is NULL");
	log_assert(type, "is NULL");
	
	if(type->info == erw_TYPEINFO_NAMED)
	{
		str_append(str, type->named.name);
	}
	else if(type->info == erw_TYPEINFO_ARRAY)
	{
		erw_generatetypename(str, type->array.type);
		str_appendfmt(str, "Array%zu", type->array.numelements);
	}
	else if(type->info == erw_TYPEINFO_SLICE)
	{
		erw_generatetypename(str, type->slice.type);
		str_appendfmt(str, "%sSlice", type->slice.ismut? "Mut" : "");
	}
	else if(type->info == erw_TYPEINFO_REFERENCE)
	{
		erw_generatetypename(str, type->reference.type);
		str_appendfmt(str, "%sReference", type->reference.ismut ? "Mut" : "");
	}
	else if(type->info == erw_TYPEINFO_FUNC)
	{
		if(type->func.rettype)
		{
			erw_generatetypename(str, type->func.rettype);
		}
		else
		{
			str_append(str, "Void");
		}

		str_append(str, "Function");
		for(size_t i = 0; i < vec_getsize(type->func.params); i++)
		{
			erw_generatetypename(str, type->func.params[i]);
		}
		
		str_append(str, "Params");
	}
	else if(type->info == erw_TYPEINFO_STRUCT)
	{
		if(type->struct_.extensiontype)
		{
			erw_generatetypename(str, type->struct_.extensiontype);
		}

		str_append(str, "Struct");
		for(size_t i = 0; i < vec_getsize(type->struct_.members); i++)
		{
			if(!type->struct_.members[i].isextension)
			{
				erw_generatetypename(str, type->struct_.members[i].type);
			}
		}

		str_append(str, "Members");
	}
	else if(type->info == erw_TYPEINFO_UNION)
	{
		for(size_t i = 0; i < vec_getsize(type->union_.members); i++)
		{
			erw_generatetypename(str, type->union_.members[i]);
		}

		str_append(str, "Members");
	}
	else
	{
		log_assert(0, "Unhandled type: %s", erw_type_tostring(type).data);
	}
}

void erw_type_dtor(struct erw_Type* self)
{
	log_assert(self, "is NULL");

	for(size_t i = 0; i < erw_TYPEBUILTIN_COUNT; i++)
	{
		if(self == erw_type_builtins[i])
		{
			return;
		}
	}

	if(self->info == erw_TYPEINFO_STRUCT)
	{
		vec_dtor(self->struct_.members);
	}
	else if(self->info == erw_TYPEINFO_UNION)
	{
		vec_dtor(self->union_.members);
	}
	else if(self->info == erw_TYPEINFO_ENUM)
	{
		vec_dtor(self->enum_.members);
	}
	else if(self->info == erw_TYPEINFO_FUNC)
	{
		vec_dtor(self->func.params);
	}
	/*
	else if(self->info == erw_TYPEINFO_NAMED)
	{
		erw_type_dtor(self->named.type);
	}
	else if(self->info == erw_TYPEINFO_ARRAY)
	{
		erw_type_dtor(self->array.type);
	}
	else if(self->info == erw_TYPEINFO_SLICE)
	{
		erw_type_dtor(self->slice.type);
	}
	else if(self->info == erw_TYPEINFO_REFERENCE)
	{
		erw_type_dtor(self->reference.type);
	}
	*/

	//free(self);
}

