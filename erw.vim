" Vim syntax file
" Language: Erwall
" Maintainer: Erik Wallström
" Latest Revision: 23 August 2018

if exists("b:current_syntax")
	finish
endif

syn keyword Todo TODO FIXME NOTE XXX
syn keyword Conditional if else_if else match 
syn keyword Repeat while for_each for
syn keyword Label case 
syn keyword Operator size_of len_of enum_name enum_value and or cast 
syn keyword Exception defer
syn keyword Structure struct union array enum
syn keyword Keyword func let mut ref return type foreign continue break extend 
	\import

syn match Type "\u[[:alnum:]_]*"
syn match Variable "\l[[:alnum:]_]*"
syn match Number "\d\+"
syn match Float "\d\+\.\d\+"
syn match Boolean "true\|false"
syn match Function "\l[[:alnum:]_]*\ze("
syn match Function "\l[[:alnum:]_]*\ze!("
syn match Function "\l[[:alnum:]_]*\ze\s*:\s*("

syn region String start=+"+ end=+"+
syn region Character start=+'+ end=+'+
syn region PreProc start="#" end="$"
syn region Comment start="@" end="$" end=";" contains=Todo
syn region Comment start="@\[" end="@\]" contains=Todo
