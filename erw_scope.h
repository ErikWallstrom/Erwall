/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ERW_SCOPE_H
#define ERW_SCOPE_H

#include "erw_compiler.h"
#include "erw_type.h"
#include "erw_ast.h"

struct erw_TypeDeclr
{
	struct erw_ASTNode* node;
	struct erw_Type* type;
	const char* name;
	const char* module;
	int isused;
	int isforeign;
};

struct erw_TypeTemplate
{
	Vec(Vec(struct erw_Type*)) generatedtypes;
	const char* name;
	struct erw_ASTNode* node;
	struct erw_Scope* parent;
	const char* module;
};

struct erw_VarDeclr
{
	struct erw_ASTNode* node;
	struct erw_Type* type;
	const char* name;
	int isused;
	int isforeign;
	int ismut;
	int isconst;
	int isref;
	int isrefmut;
	int isvariadic;
	int hasvalue;
};

struct erw_FuncTemplate
{
	Vec(Vec(struct erw_Type*)) generatedtypes;
	const char* name;
	struct erw_ASTNode* node;
	struct erw_Scope* parent;
	const char* module;
};

struct erw_FuncDeclr
{
	struct erw_ASTNode* node;
	struct erw_Type* type;
	const char* name;
	struct erw_Type* rettype;
	struct erw_Scope* scope;
	Vec(struct erw_VarDeclr*) params;
	const char* module;
	int isused;
	int isforeign;
	int isprot;
	int isvariadic;
};

struct erw_Module
{
	struct erw_CompilerResult result;
	struct erw_ASTNode* node; const char* name;
	const char* filename;
	int isforeign;
	int isused;
};

struct erw_Scope
{
	struct erw_ASTNode* node;
	const char* name;
	struct erw_Scope* parent;
	Vec(struct erw_FuncDeclr) functions;
	Vec(struct erw_VarDeclr) variables;
	Vec(struct erw_TypeDeclr) types;
	Vec(struct erw_Scope*) children;
	Vec(struct erw_Module) modules;
	Vec(struct erw_TypeTemplate) typetemplates;
	Vec(struct erw_FuncTemplate) functemplates;
	size_t index;
	int isfunc;
};

struct erw_Scope* erw_scope_new(
	struct erw_Scope* parent, 
	const char* name, 
	int isfunc,
	struct erw_ASTNode* node
);
struct erw_VarDeclr* erw_scope_findvardeclr(
	struct erw_Scope* self, 
	const char* name
);
struct erw_FuncDeclr* erw_scope_findfuncdeclr(
	struct erw_Scope* self, 
	const char* name,
	int acceptprot
);
struct erw_TypeDeclr* erw_scope_findtypedeclr(
	struct erw_Scope* self, 
	const char* name
);
struct erw_Module* erw_scope_findmodule(
	struct erw_Scope* self, 
	const char* name
);
struct erw_VarDeclr* erw_scope_getvardeclr(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines
);
struct erw_FuncDeclr* erw_scope_getfuncdeclr(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines
);
struct erw_TypeDeclr* erw_scope_gettypedeclr(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines
);
struct erw_TypeDeclr* erw_scope_gettypedeclr(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines
);
struct erw_Module* erw_scope_getmodule(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines
);
struct erw_TypeTemplate* erw_scope_gettypetemplate(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines
);
struct erw_FuncTemplate* erw_scope_getfunctemplate(
	struct erw_Scope* self, 
	struct erw_Token* token,
	struct Str* lines
);
struct erw_VarDeclr* erw_scope_addvardeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
struct erw_FuncDeclr* erw_scope_addfuncdeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
struct erw_TypeDeclr* erw_scope_addtypedeclr(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
struct erw_Module* erw_scope_addmodule(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
struct erw_TypeTemplate* erw_scope_addtypetemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
struct erw_FuncTemplate* erw_scope_addfunctemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
struct erw_Token* erw_calltypetemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
struct erw_Token* erw_callfunctemplate(
	struct erw_Scope* self, 
	struct erw_ASTNode* node,
	struct Str* lines
);
void erw_scope_print(struct erw_Scope* self, struct Str* lines);
void erw_scope_dtor(struct erw_Scope* self);

#endif
