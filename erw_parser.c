/* 
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "erw_parser.h"
#include "log.h"
#include "erw_ast.h"
#include "erw_error.h"
#include <stdlib.h>
#include <gmp.h>

struct erw_Parser
{
	Vec(struct erw_Token) tokens;
	Vec(struct Str) lines;
	size_t current;
};

static int erw_parser_check(
	struct erw_Parser* parser,
	const struct erw_TokenType* type)
{
	log_assert(parser, "is NULL");
	log_assert(type, "is NULL");

	if(parser->current < vec_getsize(parser->tokens))
	{
		if(parser->tokens[parser->current].type == type)
		{
			return 1;
		}
	}
	else
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Expected '%s', reached EOF",
			type->name
		);

		erw_error(
			msg.data,
			parser->lines[parser->tokens[parser->current].linenum - 1].data,
			parser->tokens[parser->current].linenum,
			parser->tokens[parser->current].column,
			parser->tokens[parser->current].column
		);
		str_dtor(&msg);
	}

	return 0;
}

static struct erw_Token* erw_parser_getnext(struct erw_Parser* parser)
{
	log_assert(parser, "is NULL");
	
	parser->current++;
	return &parser->tokens[parser->current - 1];
}

static struct erw_Token* erw_parser_expect(
	struct erw_Parser* parser,
	const struct erw_TokenType* type)
{
	log_assert(parser, "is NULL");
	log_assert(type, "is NULL");

	if(!erw_parser_check(parser, type))
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Expected '%s', got '%s'",
			type->name,
			parser->tokens[parser->current].type->name
		);

		erw_error(
			msg.data,
			parser->lines[parser->tokens[parser->current].linenum - 1].data,
			parser->tokens[parser->current].linenum,
			parser->tokens[parser->current].column,
			parser->tokens[parser->current].column 
				+ vec_getsize(parser->tokens[parser->current].text) - 2
		);
		str_dtor(&msg);
	}

	return erw_parser_getnext(parser);
}

static struct erw_ASTNode* erw_parse_block(
	struct erw_Parser* parser, 
	int canbreak
);
static int erw_parse_varlist(
	struct erw_Parser* parser, 
	Vec(struct erw_ASTNode*) varlist,
	int isforeign
);
static struct erw_ASTNode* erw_parse_expr(struct erw_Parser* parser);
static struct erw_ASTNode* erw_parse_type(struct erw_Parser* parser);
static struct erw_ASTNode* erw_parse_factor(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* node = NULL;
	if(erw_parser_check(parser, erw_TOKENTYPE_LITERAL_BOOL) 
		|| erw_parser_check(parser, erw_TOKENTYPE_LITERAL_CHAR) 
		|| erw_parser_check(parser, erw_TOKENTYPE_LITERAL_INT) 
		|| erw_parser_check(parser, erw_TOKENTYPE_LITERAL_FLOAT) 
		|| erw_parser_check(parser, erw_TOKENTYPE_LITERAL_STRING))
	{ 
		node = erw_ast_new(erw_ASTNODETYPE_LITERAL, erw_parser_getnext(parser));
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_IDENT))
	{ 
		node = erw_ast_new(
			erw_ASTNODETYPE_LITERAL, //Is this really correct?
			erw_parser_expect(parser, erw_TOKENTYPE_IDENT)
		);
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
	{ 
		erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
		node = erw_parse_expr(parser);
		erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_CAST))
	{ 
		node = erw_ast_new(
			erw_ASTNODETYPE_CAST,
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_CAST)
		);

		erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
		node->cast.type = erw_parse_type(parser);
		erw_parser_expect(parser, erw_TOKENTYPE_COMMA);

		node->cast.expr = erw_parse_expr(parser);
		erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_LENGTHOF)
		|| erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_SIZEOF)
		|| erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ENUMVALUE)
		|| erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ENUMNAME))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_BUILTINCALL,
			erw_parser_getnext(parser)
		);

		erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
		node->builtincall.param = erw_parse_expr(parser);
		erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_STRUCT))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_STRUCTLITERAL, 
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_STRUCT)
		);

		erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
		int first = 1;
		while(!erw_parser_check(parser, erw_TOKENTYPE_RBRACKET))
		{
			if(first)
			{
				first = 0;
			}
			else
			{
				erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
			}

			vec_pushback(
				node->structliteral.names, 
				erw_parser_expect(parser, erw_TOKENTYPE_IDENT)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
			vec_pushback(node->structliteral.values, erw_parse_expr(parser));
			if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
			{
				break;
			}
		}

		erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_UNION))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_UNIONLITERAL, 
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_UNION)
		);
		
		erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
		node->unionliteral.type = erw_parse_type(parser);
		if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_DECLR))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
			node->unionliteral.value = erw_parse_expr(parser);
		}

		erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ARRAY))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_ARRAYLITERAL, 
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_ARRAY)
		);
		
		erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
		int first = 1;
		while(!erw_parser_check(parser, erw_TOKENTYPE_RBRACKET))
		{
			if(first)
			{
				first = 0;
				struct erw_ASTNode* expr = erw_parse_expr(parser);
				if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_DECLR))
				{
					node->arrayliteral.length = expr;
					erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
					vec_pushback(
						node->arrayliteral.values, 
						erw_parse_expr(parser)
					);
				}
				else
				{
					vec_pushback(node->arrayliteral.values, expr);
				}
			}
			else
			{
				erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
				vec_pushback(node->arrayliteral.values, erw_parse_expr(parser));
			}

			if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
			{
				break;
			}
		}

		erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ENUM))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_ENUMLITERAL, 
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_ENUM)
		);

		erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
		node->enumliteral.name = erw_parser_expect(parser, erw_TOKENTYPE_TYPE);
		erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
	}
	/*else if(erw_parser_check(parser, erw_TOKENTYPE_LBRACKET))
	{
		//Slicing
		
	}*/
	else if(erw_parser_check(parser, erw_TOKENTYPE_TYPE))
	{
		struct erw_ASTNode* type = erw_ast_new(
			erw_ASTNODETYPE_TYPE,
			erw_parser_expect(parser, erw_TOKENTYPE_TYPE)
		);

		node = erw_ast_new(
			erw_ASTNODETYPE_BINEXPR, 
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS)
		);

		node->binexpr.expr1 = type;
		struct erw_ASTNode* ident = erw_ast_new(
			erw_ASTNODETYPE_LITERAL,
			erw_parser_expect(parser, erw_TOKENTYPE_IDENT)
		);

		node->binexpr.expr2 = ident;
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_FUNC))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_FUNCLITERAL, 
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_FUNC)
		);

		node->funcliteral.isvariadic = erw_parse_varlist(
			parser, 
			node->funcliteral.params,
			0
		);

		if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_RETURN))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_RETURN);
			node->funcliteral.type = erw_parse_type(parser);
		}

		if(erw_parser_check(parser, erw_TOKENTYPE_LCURLY))
		{
			node->funcliteral.block = erw_parse_block(parser, 0);
		}
	}
	else
	{ 
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Unexpected %s",
			parser->tokens[parser->current].type->name
		);

		erw_error(
			msg.data, 
			parser->lines[parser->tokens[parser->current].linenum - 1].data, 
			parser->tokens[parser->current].linenum, 
			parser->tokens[parser->current].column,
			parser->tokens[parser->current].column 
				+ vec_getsize(parser->tokens[parser->current].text) - 2
		);
		str_dtor(&msg);
	}

	while(1)
	{
		if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ACCESS))
		{
			struct erw_ASTNode* newnode = erw_ast_new(
				erw_ASTNODETYPE_BINEXPR, 
				erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS)
			);

			newnode->binexpr.expr1 = node;
			newnode->binexpr.expr2 = erw_ast_new(
				erw_ASTNODETYPE_LITERAL,
				erw_parser_expect(
					parser,
					erw_TOKENTYPE_IDENT
				)
			);

			node = newnode;
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_BITAND))
		{
			struct erw_ASTNode* newnode = erw_ast_new(
				erw_ASTNODETYPE_UNEXPR, 
				erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_BITAND)
			);
			
			newnode->unexpr.left = 0;
			newnode->unexpr.expr = node;
			node = newnode;
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_LBRACKET))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
			struct erw_ASTNode* newnode = erw_ast_new(
				erw_ASTNODETYPE_ACCESS, 
				NULL
			);

			newnode->access.expr = node;
			newnode->access.index = erw_parse_expr(parser);
			erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
			node = newnode;
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
		{
			struct erw_ASTNode* newnode = erw_ast_new(
				erw_ASTNODETYPE_FUNCCALL, 
				NULL
			);

			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			int first = 1;
			while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
			{
				if(first)
				{
					first = 0;
				}
				else
				{
					erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
				}

				vec_pushback(newnode->funccall.args, erw_parse_expr(parser));
				if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
				{
					break;
				}
			}

			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			newnode->funccall.callee = node;
			node = newnode;
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_NOT))
		{
			struct erw_ASTNode* newnode = erw_ast_new(
				erw_ASTNODETYPE_TEMPLATECALL, 
				NULL
			);

			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_NOT);
			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			int first = 1;
			while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
			{
				if(first)
				{
					first = 0;
				}
				else
				{
					erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
				}

				vec_pushback(
					newnode->templatecall.types, 
					erw_parse_type(parser)
				);

				if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
				{
					break;
				}
			}

			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			struct erw_ASTNode* callnode = erw_ast_new(
				erw_ASTNODETYPE_FUNCCALL, 
				NULL
			);

			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			first = 1;
			while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
			{
				if(first)
				{
					first = 0;
				}
				else
				{
					erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
				}

				vec_pushback(callnode->funccall.args, erw_parse_expr(parser));
				if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
				{
					break;
				}
			}

			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			callnode->funccall.callee = node;
			newnode->templatecall.body = callnode;
			node = newnode;
		}
		else
		{
			break;
		}
	}

	return node;
}

static struct erw_ASTNode* erw_parse_ref(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* signnode;
	if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_BITAND))
	{
		signnode = erw_ast_new(
			erw_ASTNODETYPE_UNEXPR,
			erw_parser_getnext(parser)
		);

		if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_MUT);
			signnode->unexpr.mutable = 1;
		}

		signnode->unexpr.expr = erw_parse_factor(parser);
		signnode->unexpr.left = 1;
	}
	else
	{
		signnode = erw_parse_factor(parser);
	}

	return signnode;
}

static char* erw_getconstint(struct erw_ASTNode* node)
{
	if(node->type == erw_ASTNODETYPE_LITERAL 
		&& node->token->type == erw_TOKENTYPE_LITERAL_INT)
	{
		return node->token->text;
	}
	else if(node->type == erw_ASTNODETYPE_BINEXPR
		&& node->binexpr.result)
	{
		return node->binexpr.result;
	}
	else
	{
		return NULL;
	}
}

static struct erw_ASTNode* erw_parse_exponent(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* exponode = erw_parse_ref(parser);
	while(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_POW))
	{
		struct erw_ASTNode* oldnode = exponode;
		exponode = erw_ast_new(
			erw_ASTNODETYPE_BINEXPR,
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_POW)
		);

		exponode->binexpr.expr1 = oldnode;
		struct erw_ASTNode* newnode = erw_parse_ref(parser);
		exponode->binexpr.expr2 = newnode;

		char* const1 = erw_getconstint(exponode->binexpr.expr1);
		char* const2 = erw_getconstint(exponode->binexpr.expr2);
		if(const1 && const2)
		{
			mpz_t x;
			mpz_init_set_str(x, const1, 10);
			
			char* endptr = const2;
			unsigned long y = strtoul(const2, &endptr, 10);
			if(*endptr != '\0')
			{
				struct Str msg;
				str_ctorfmt(
					&msg,
					"Exponent cannot exceed %lu",
					LONG_MAX
				);

				erw_error(
					msg.data, 
					parser->lines[newnode->token->linenum - 1].data, 
					newnode->token->linenum, 
					newnode->token->column,
					newnode->token->column + 
						vec_getsize(newnode->token->text) - 2
				);
				str_dtor(&msg);
			}

			mpz_pow_ui(x, x, y);
			size_t len = mpz_sizeinbase(x, 10);
			exponode->binexpr.result = vec_ctor(char, len);
			vec_expand(exponode->binexpr.result, 0, len);
			mpz_get_str(exponode->binexpr.result, 10, x);
		}
	}

	return exponode;
}

static struct erw_ASTNode* erw_parse_sign(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* signnode;
	if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_SUB) 
		|| erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_NOT))
	{
		signnode = erw_ast_new(
			erw_ASTNODETYPE_UNEXPR,
			erw_parser_getnext(parser)
		);

		signnode->unexpr.expr = erw_parse_exponent(parser);
		signnode->unexpr.left = 1;
	}
	else
	{
		signnode = erw_parse_exponent(parser);
	}

	return signnode;
}

static struct erw_ASTNode* erw_parse_term(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* termnode = erw_parse_sign(parser);
	while(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_MUL) 
		|| erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_DIV) 
		|| erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_MOD))
	{
		struct erw_ASTNode* oldnode = termnode;
		termnode = erw_ast_new(
			erw_ASTNODETYPE_BINEXPR,
			erw_parser_getnext(parser)
		);

		termnode->binexpr.expr1 = oldnode;
		struct erw_ASTNode* newnode = erw_parse_sign(parser);
		termnode->binexpr.expr2 = newnode;

		char* const1 = erw_getconstint(termnode->binexpr.expr1);
		char* const2 = erw_getconstint(termnode->binexpr.expr2);
		if(const1 && const2)
		{
			mpz_t x;
			mpz_init_set_str(x, const1, 10);

			mpz_t y;
			mpz_init_set_str(y, const2, 10);

			if(termnode->token->type == erw_TOKENTYPE_OPERATOR_MUL)
			{
				mpz_mul(x, x, y);
			}
			else if(termnode->token->type == erw_TOKENTYPE_OPERATOR_DIV)
			{
				mpz_tdiv_q(x, x, y);
			}
			else if(termnode->token->type == erw_TOKENTYPE_OPERATOR_MOD)
			{
				mpz_mod(x, x, y);
			}

			size_t len = mpz_sizeinbase(x, 10);
			termnode->binexpr.result = vec_ctor(char, len);
			vec_expand(termnode->binexpr.result, 0, len);
			mpz_get_str(termnode->binexpr.result, 10, x);
		}
	}

	return termnode;
}

static struct erw_ASTNode* erw_parse_plusminus(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* pmnode = erw_parse_term(parser);
	while(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ADD) ||
		erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_SUB))
	{
		struct erw_ASTNode* oldnode = pmnode;
		pmnode = erw_ast_new(
			erw_ASTNODETYPE_BINEXPR,
			erw_parser_getnext(parser)
		);

		pmnode->binexpr.expr1 = oldnode;
		struct erw_ASTNode* newnode = erw_parse_term(parser);
		pmnode->binexpr.expr2 = newnode;

		char* const1 = erw_getconstint(pmnode->binexpr.expr1);
		char* const2 = erw_getconstint(pmnode->binexpr.expr2);
		if(const1 && const2)
		{
			mpz_t x;
			mpz_init_set_str(x, const1, 10);

			mpz_t y;
			mpz_init_set_str(y, const2, 10);

			if(pmnode->token->type == erw_TOKENTYPE_OPERATOR_ADD)
			{
				mpz_add(x, x, y);
			}
			else if(pmnode->token->type == erw_TOKENTYPE_OPERATOR_SUB)
			{
				mpz_sub(x, x, y);
			}

			size_t len = mpz_sizeinbase(x, 10);
			pmnode->binexpr.result = vec_ctor(char, len);
			vec_expand(pmnode->binexpr.result, 0, len);
			mpz_get_str(pmnode->binexpr.result, 10, x);
		}
	}

	return pmnode;
}

static struct erw_ASTNode* erw_parse_comparison(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* cmpnode = erw_parse_plusminus(parser);
	while(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_LESS) 
		|| erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_LESSOREQUAL) 
		|| erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_GREATER) 
		|| erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_GREATEROREQUAL))
	{ 
		struct erw_ASTNode* oldnode = cmpnode;
		cmpnode = erw_ast_new(
			erw_ASTNODETYPE_BINEXPR,
			erw_parser_getnext(parser)
		);

		cmpnode->binexpr.expr1 = oldnode;
		struct erw_ASTNode* newnode = erw_parse_plusminus(parser);
		cmpnode->binexpr.expr2 = newnode;
	}

	return cmpnode;
}

static struct erw_ASTNode* erw_parse_equality(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* eqnode = erw_parse_comparison(parser);
	while(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_EQUAL) ||
		erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_NOTEQUAL))
	{ 
		struct erw_ASTNode* oldnode = eqnode;
		eqnode = erw_ast_new(
			erw_ASTNODETYPE_BINEXPR,
			erw_parser_getnext(parser)
		);

		eqnode->binexpr.expr1 = oldnode;
		struct erw_ASTNode* newnode = erw_parse_comparison(parser);
		eqnode->binexpr.expr2 = newnode;
	}

	return eqnode;
}

static struct erw_ASTNode* erw_parse_andor(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* andornode = erw_parse_equality(parser);
	while(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_AND) ||
		erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_OR))
	{ 
		struct erw_ASTNode* oldnode = andornode;
		andornode = erw_ast_new(
			erw_ASTNODETYPE_BINEXPR,
			erw_parser_getnext(parser)
		);

		andornode->binexpr.expr1 = oldnode;
		struct erw_ASTNode* newnode = erw_parse_equality(parser);
		andornode->binexpr.expr2 = newnode;
	}

	return andornode;
}

static struct erw_ASTNode* erw_parse_expr(struct erw_Parser* parser)
{
	return erw_parse_andor(parser);
}

static struct erw_ASTNode* erw_parse_struct(struct erw_Parser* parser);
static struct erw_ASTNode* erw_parse_union(struct erw_Parser* parser);
static struct erw_ASTNode* erw_parse_type(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* root = NULL;
	struct erw_ASTNode* node = NULL;
	int done = 0;
	while(!done)
	{ 
		struct erw_ASTNode* tmpnode = NULL;
		if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_BITAND))
		{ 
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_BITAND);
			tmpnode = erw_ast_new(erw_ASTNODETYPE_REFERENCE, NULL);
			if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
			{
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_MUT);
				tmpnode->reference.mutable = 1;
			}
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_LBRACKET))
		{ 
			erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
			if(erw_parser_check(parser, erw_TOKENTYPE_LITERAL_INT))
			{
				tmpnode = erw_ast_new(erw_ASTNODETYPE_ARRAY, NULL);
				//Parse expression?
				tmpnode->array.size = erw_ast_new(
					erw_ASTNODETYPE_LITERAL,
					erw_parser_expect(parser, erw_TOKENTYPE_LITERAL_INT)
				);
				erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
			}
			else
			{
				tmpnode = erw_ast_new(erw_ASTNODETYPE_SLICE, NULL);
				erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);

				if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
				{
					erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_MUT);
					tmpnode->slice.mutable = 1;
				}
			}
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_FUNC))
		{
			tmpnode = erw_ast_new(
				erw_ASTNODETYPE_FUNCTYPE,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_FUNC)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			int first = 1;
			while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
			{
				if(first)
				{
					first = 0;
				}
				else
				{
					erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
				}

				vec_pushback(tmpnode->functype.params, erw_parse_type(parser));
				if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
				{
					break;
				}
			}

			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_RETURN))
			{
				erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_RETURN);
				tmpnode->functype.type = erw_parse_type(parser);
			}
			
			done = 1;
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_TYPE))
		{ 
			struct erw_ASTNode* type = erw_ast_new(
				erw_ASTNODETYPE_TYPE,
				erw_parser_expect(parser, erw_TOKENTYPE_TYPE)
			);

			if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ACCESS))
			{
				tmpnode = erw_ast_new(
					erw_ASTNODETYPE_BINEXPR, 
					erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS)
				);

				tmpnode->binexpr.expr1 = type;
				struct erw_ASTNode* ident = erw_ast_new(
					erw_ASTNODETYPE_TYPE,
					erw_parser_expect(parser, erw_TOKENTYPE_TYPE)
				);

				tmpnode->binexpr.expr2 = ident;
			}
			else
			{
				tmpnode = type;
			}

			if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
			{
				erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
				struct erw_ASTNode* template = erw_ast_new(
					erw_ASTNODETYPE_TEMPLATECALL, 
					NULL
				);

				int first = 1;
				while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
				{
					if(first)
					{
						first = 0;
					}
					else
					{
						erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
					}

					vec_pushback(
						template->templatecall.types, 
						erw_parse_type(parser)
					);

					if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
					{
						break;
					}
				}

				erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
				template->templatecall.body = tmpnode;
				tmpnode = template;
			}

			done = 1; //Break loop
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_STRUCT))
		{
			tmpnode = erw_parse_struct(parser);
			done = 1;
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_UNION))
		{
			tmpnode = erw_parse_union(parser);
			done = 1;
		}
		else
		{
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Expected '&', '[', or %s. Got %s",
				erw_TOKENTYPE_TYPE->name,
				parser->tokens[parser->current].type->name
			);

			erw_error(
				msg.data, 
				parser->lines[parser->tokens[parser->current]
					.linenum - 1].data, 
				parser->tokens[parser->current].linenum, 
				parser->tokens[parser->current].column,
				parser->tokens[parser->current].column + 
					vec_getsize(parser->tokens[parser->current].text) - 2
			);
			str_dtor(&msg);
		}

		if(!root)
		{
			node = tmpnode;
			root = node;
		}
		else
		{
			node->reference.type = tmpnode;
			node = tmpnode;
		}
	}

	return root;
}

static struct erw_ASTNode* erw_parse_vardeclr(struct erw_Parser* parser)
{
	struct erw_ASTNode* node = NULL;
	if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_LET))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_VARDECLR,
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_LET)
		);
		node->vardeclr.ismut = 0;
	}
	else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
	{
		node = erw_ast_new(
			erw_ASTNODETYPE_VARDECLR,
			erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_MUT)
		);
		node->vardeclr.ismut = 1;
	}
	else
	{
		struct Str msg;
		str_ctorfmt(
			&msg,
			"Expected %s or %s, got %s",
			erw_TOKENTYPE_KEYWORD_LET->name,
			erw_TOKENTYPE_KEYWORD_MUT->name,
			parser->tokens[parser->current].type->name
		);

		erw_error(
			msg.data, 
			parser->lines[parser->tokens[parser->current].linenum - 1].data, 
			parser->tokens[parser->current].linenum, 
			parser->tokens[parser->current].column,
			parser->tokens[parser->current].column 
				+ vec_getsize(parser->tokens[parser->current].text) - 2
		);
		str_dtor(&msg);
	}

	if(node) //Remove warning
	{
		node->vardeclr.name = erw_parser_expect(parser, erw_TOKENTYPE_IDENT);
		erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
		node->vardeclr.type = erw_parse_type(parser);

		if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ASSIGN))
		{ 
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ASSIGN);
			node->vardeclr.value = erw_parse_expr(parser);
		}
	}

	return node;
}

static int erw_parse_varlist(
	struct erw_Parser* parser, 
	Vec(struct erw_ASTNode*) varlist,
	int isforeign)
{
	erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
	int isvariadic = 0;
	int first = 1;
	while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
	{
		if(first)
		{
			first = 0;
		}
		else
		{
			erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
		}

		if(isforeign && erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ACCESS))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS);
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS);
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS);

			isvariadic = 1;
			break;
		}

		struct erw_ASTNode* varnode;
		if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_LET))
		{
			varnode = erw_ast_new(
				erw_ASTNODETYPE_VARDECLR,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_LET)
			);
		}
		else
		{
			if(isforeign)
			{
				varnode = erw_ast_new(erw_ASTNODETYPE_VARDECLR, NULL);
			}
			else
			{
				varnode = erw_ast_new(
					erw_ASTNODETYPE_VARDECLR,
					erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_MUT)
				);
				varnode->vardeclr.ismut = 1;
			}
		}

		varnode->vardeclr.name = erw_parser_expect(
			parser,
			erw_TOKENTYPE_IDENT
		);

		erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
		if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ACCESS))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS);
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS);
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ACCESS);

			varnode->vardeclr.isvariadic = 1;
			if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
			{
				erw_parser_expect(
					parser, 
					erw_TOKENTYPE_KEYWORD_MUT
				);

				varnode->vardeclr.isrefmut = 1;
			}

			isvariadic = 1;
		}

		varnode->vardeclr.type = erw_parse_type(parser);
		vec_pushback(varlist, varnode);
		if(isvariadic || !erw_parser_check(parser, erw_TOKENTYPE_COMMA))
		{
			break;
		}
	}

	erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
	return isvariadic;
}

static struct erw_ASTNode* erw_parse_struct(struct erw_Parser* parser)
{
	struct erw_ASTNode* node = erw_ast_new(
		erw_ASTNODETYPE_STRUCT, 
		erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_STRUCT)
	);

	erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
	int first = 1;
	while(!erw_parser_check(parser, erw_TOKENTYPE_RBRACKET))
	{
		if(first)
		{
			first = 0;
			if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_EXTEND))
			{
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_EXTEND);
				node->struct_.extension = erw_ast_new(
					erw_ASTNODETYPE_TYPE, 
					erw_parser_expect(parser, erw_TOKENTYPE_TYPE)
				);

				continue;
			}
		}
		else
		{
			erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
		}

		struct erw_ASTNode* member = erw_ast_new(
			erw_ASTNODETYPE_STRUCTMEMBER, 
			NULL
		);

		member->structmember.name = erw_parser_expect(
			parser, 
			erw_TOKENTYPE_IDENT
		);
		erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
		member->structmember.type = erw_parse_type(parser);

		if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ASSIGN))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ASSIGN);
			member->structmember.value = erw_parse_expr(parser);
		}

		vec_pushback(node->struct_.members, member);
		if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
		{
			break;
		}
	}

	erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
	return node;
}

static struct erw_ASTNode* erw_parse_union(struct erw_Parser* parser)
{
	struct erw_ASTNode* node = erw_ast_new(
		erw_ASTNODETYPE_UNION, 
		erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_UNION)
	);

	erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
	int first = 1;
	while(!erw_parser_check(parser, erw_TOKENTYPE_RBRACKET))
	{
		if(first)
		{
			first = 0;
		}
		else
		{
			erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
		}

		vec_pushback(node->union_.members, erw_parse_type(parser));
		if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
		{
			break;
		}
	}

	erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
	return node;
}

static struct erw_ASTNode* erw_parse_enum(struct erw_Parser* parser)
{
	struct erw_ASTNode* node = erw_ast_new(
		erw_ASTNODETYPE_ENUM, 
		erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_ENUM)
	);

	if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
	{
		erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
		node->enum_.type = erw_parse_type(parser);
		erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
	}

	erw_parser_expect(parser, erw_TOKENTYPE_LBRACKET);
	int first = 1;
	while(!erw_parser_check(parser, erw_TOKENTYPE_RBRACKET))
	{
		if(first)
		{
			first = 0;
		}
		else
		{
			erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
		}

		struct erw_ASTNode* membernode = erw_ast_new(
			erw_ASTNODETYPE_ENUMMEMBER, 
			NULL
		);

		membernode->enummember.name = erw_parser_expect(
			parser, 
			erw_TOKENTYPE_TYPE
		);

		if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
		{
			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			membernode->enummember.str = erw_parser_expect(
				parser, 
				erw_TOKENTYPE_LITERAL_STRING
			);

			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
		}

		
		if(node->enum_.type)
		{
			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ASSIGN);
			membernode->enummember.value = erw_parse_expr(parser);
		}
		else
		{
			if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_ASSIGN))
			{
				erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_ASSIGN);
				membernode->enummember.value = erw_ast_new(
					erw_ASTNODETYPE_LITERAL,
					erw_parser_expect(parser, erw_TOKENTYPE_LITERAL_INT)
				);
			}
		}

		vec_pushback(node->enum_.members, membernode);
		if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
		{
			break;
		}
	}

	erw_parser_expect(parser, erw_TOKENTYPE_RBRACKET);
	return node;
}

static struct erw_ASTNode* erw_parse_typedeclr(struct erw_Parser* parser)
{ 
	struct erw_ASTNode* typedeclr = erw_ast_new(
		erw_ASTNODETYPE_TYPEDECLR,
		erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_TYPE)
	);

	struct erw_ASTNode* node = typedeclr;
	typedeclr->typedeclr.name = erw_parser_expect(parser, erw_TOKENTYPE_TYPE);
	if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
	{
		erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
		node = erw_ast_new(erw_ASTNODETYPE_TEMPLATE, NULL);
		node->template.body = typedeclr;

		int first = 1;
		while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
		{
			if(first)
			{
				first = 0;
			}
			else
			{
				erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
			}

			vec_pushback(
				node->template.types, 
				erw_parser_expect(parser, erw_TOKENTYPE_TYPE)
			);
			if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
			{
				break;
			}
		}

		erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
	}

	if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_DECLR))
	{ 
		erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
		if(erw_parser_check(parser, erw_TOKENTYPE_TYPE))
		{
			typedeclr->typedeclr.type = erw_ast_new(
				erw_ASTNODETYPE_TYPE,
				erw_parser_expect(parser, erw_TOKENTYPE_TYPE)
			);
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_STRUCT))
		{
			typedeclr->typedeclr.type = erw_parse_struct(parser);
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_UNION))
		{
			typedeclr->typedeclr.type = erw_parse_union(parser);
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ENUM))
		{
			typedeclr->typedeclr.type = erw_parse_enum(parser);
		}
		else
		{
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Expected %s, %s, %s, or %s. Got %s",
				erw_TOKENTYPE_TYPE->name,
				erw_TOKENTYPE_KEYWORD_STRUCT->name,
				erw_TOKENTYPE_KEYWORD_UNION->name,
				erw_TOKENTYPE_KEYWORD_ENUM->name,
				parser->tokens[parser->current].type->name
			);

			erw_error(
				msg.data, 
				parser->lines[parser->tokens[parser->current]
					.linenum - 1].data, 
				parser->tokens[parser->current].linenum, 
				parser->tokens[parser->current].column,
				parser->tokens[parser->current].column 
					+ vec_getsize(parser->tokens[parser->current].text) - 2
			);
			str_dtor(&msg);
		}
	}

	erw_parser_expect(parser, erw_TOKENTYPE_END);
	return node;
}

static struct erw_ASTNode* erw_parse_funcdeclr(
	struct erw_Parser* parser,
	int isforeign
);

static struct erw_ASTNode* erw_parse_block(
	struct erw_Parser* parser, 
	int canbreak)
{
	struct erw_ASTNode* node = erw_ast_new(erw_ASTNODETYPE_BLOCK, NULL);
	erw_parser_expect(parser, erw_TOKENTYPE_LCURLY);
	while(!erw_parser_check(parser, erw_TOKENTYPE_RCURLY))
	{
		if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_FUNC))
		{ 
			vec_pushback(node->block.stmts, erw_parse_funcdeclr(parser, 0));
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_TYPE))
		{ 
			vec_pushback(node->block.stmts, erw_parse_typedeclr(parser));
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_FOREIGN))
		{
			struct erw_ASTNode* foreignnode = erw_ast_new(
				erw_ASTNODETYPE_FOREIGN,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_FOREIGN)
			);

			if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
			{
				erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
				foreignnode->foreign.name = erw_parser_expect(
					parser, 
					erw_TOKENTYPE_LITERAL_STRING
				);

				erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			}

			if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_FUNC))
			{
				foreignnode->foreign.node = erw_parse_funcdeclr(parser, 1);
			}
			else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_TYPE))
			{
				foreignnode->foreign.node = erw_parse_typedeclr(parser);
			}
			else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_LET) 
				|| erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
			{
				foreignnode->foreign.node = erw_parse_vardeclr(parser);
			}
			else
			{
				struct Str msg;
				str_ctorfmt(
					&msg,
					"Expected %s, %s, %s or %s, got %s",
					erw_TOKENTYPE_KEYWORD_FUNC->name,
					erw_TOKENTYPE_KEYWORD_TYPE->name,
					erw_TOKENTYPE_KEYWORD_LET->name,
					erw_TOKENTYPE_KEYWORD_MUT->name,
					parser->tokens[parser->current].type->name
				);

				erw_error(
					msg.data,
					parser->lines[
						parser->tokens[parser->current].linenum - 1
					].data,
					parser->tokens[parser->current].linenum,
					parser->tokens[parser->current].column,
					parser->tokens[parser->current].column 
						+ vec_getsize(parser->tokens[parser->current].text) - 2
				);
				str_dtor(&msg);
			}

			vec_pushback(node->block.stmts, foreignnode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_LET)
			|| erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
		{
			vec_pushback(node->block.stmts, erw_parse_vardeclr(parser));
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_IDENT))
		{ 
			struct erw_ASTNode* ident = erw_parse_expr(parser);
			if(parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_BITAND
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_ACCESS
			|| parser->tokens[parser->current].type 
				==  erw_TOKENTYPE_OPERATOR_ASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_ADDASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_SUBASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_MULASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_DIVASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_POWASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_MODASSIGN)
			{
				struct erw_ASTNode* assignnode = erw_ast_new(
					erw_ASTNODETYPE_ASSIGNMENT,
					erw_parser_getnext(parser)
				);

				struct erw_ASTNode* expr = erw_parse_expr(parser);
				assignnode->assignment.assignee = ident;
				assignnode->assignment.expr = expr;
				vec_pushback(node->block.stmts, assignnode);
			}
			else
			{
				//Assume struct access/function call
				vec_pushback(node->block.stmts, ident); 
			}
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_TYPE))
			//Module access
		{
			struct erw_ASTNode* access = erw_parse_expr(parser);
			vec_pushback(node->block.stmts, access); 
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_IF))
		{ 
			struct erw_ASTNode* ifnode = erw_ast_new(
				erw_ASTNODETYPE_IF,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_IF)
			);
			
			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			ifnode->if_.expr = erw_parse_expr(parser);
			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			ifnode->if_.block = erw_parse_block(parser, canbreak);
			while(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ELSEIF))
			{ 
				struct erw_ASTNode* elseifnode = erw_ast_new(
					erw_ASTNODETYPE_ELSEIF,
					erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_ELSEIF)
				);
				
				erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
				elseifnode->elseif.expr = erw_parse_expr(parser);
				erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
				elseifnode->elseif.block = erw_parse_block(parser, canbreak);
				vec_pushback(ifnode->if_.elseifs, elseifnode);
			}

			if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ELSE))
			{ 
				struct erw_ASTNode* elsenode = erw_ast_new(
					erw_ASTNODETYPE_ELSE,
					erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_ELSE)
				);

				elsenode->else_.block = erw_parse_block(parser, canbreak);
				ifnode->if_.else_ = elsenode;
			}

			vec_pushback(node->block.stmts, ifnode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_RETURN))
		{ 
			struct erw_ASTNode* retnode = erw_ast_new(
				erw_ASTNODETYPE_RETURN,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_RETURN)
			);

			if(!erw_parser_check(parser, erw_TOKENTYPE_END))
			{
				retnode->return_.expr = erw_parse_expr(parser);
			}

			erw_parser_expect(parser, erw_TOKENTYPE_END);
			vec_pushback(node->block.stmts, retnode);
			break; //Don't parse any statements after return
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_DEFER))
		{
			struct erw_ASTNode* defernode = erw_ast_new(
				erw_ASTNODETYPE_DEFER,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_DEFER)
			);

			defernode->defer.block = erw_parse_block(parser, 0);
			vec_pushback(node->block.stmts, defernode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_UNSAFE))
		{
			struct erw_ASTNode* unsafenode = erw_ast_new(
				erw_ASTNODETYPE_UNSAFE,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_UNSAFE)
			);

			unsafenode->unsafe.block = erw_parse_block(parser, canbreak);
			vec_pushback(node->block.stmts, unsafenode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_WHILE))
		{
			struct erw_ASTNode* whilenode = erw_ast_new(
				erw_ASTNODETYPE_WHILE,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_WHILE)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			whilenode->while_.expr = erw_parse_expr(parser);
			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			whilenode->while_.block = erw_parse_block(parser, 1);
			vec_pushback(node->block.stmts, whilenode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MATCH))
		{
			struct erw_ASTNode* matchnode = erw_ast_new(
				erw_ASTNODETYPE_MATCH,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_MATCH)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			matchnode->match.expr = erw_parse_expr(parser);

			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			erw_parser_expect(parser, erw_TOKENTYPE_LCURLY);
			while(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_CASE))
			{
				struct erw_ASTNode* casenode = erw_ast_new(
					erw_ASTNODETYPE_CASE,
					erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_CASE)
				);

				erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);

				int islet = erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_LET);
				int ismut = erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT);
				if(islet || ismut)
				{
					struct erw_ASTNode* unioncasenode = erw_ast_new(
						erw_ASTNODETYPE_VARDECLR,
						erw_parser_getnext(parser)
					);

					unioncasenode->vardeclr.ismut = ismut;
					unioncasenode->vardeclr.name = erw_parser_expect(
						parser,
						erw_TOKENTYPE_IDENT
					);

					erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
					if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_REF))
					{
						erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_REF);
						unioncasenode->vardeclr.isref = 1;

						if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
						{
							erw_parser_expect(
								parser, 
								erw_TOKENTYPE_KEYWORD_MUT
							);

							unioncasenode->vardeclr.isrefmut = 1;
						}
					}

					unioncasenode->vardeclr.type = erw_parse_type(parser);
					casenode->case_.expr = unioncasenode;
				}
				else
				{
					casenode->case_.expr = erw_parse_expr(parser);
				}

				erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
				casenode->case_.block = erw_parse_block(parser, canbreak);

				vec_pushback(matchnode->match.cases, casenode);
				if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_ELSE))
				{
					struct erw_ASTNode* elsenode = erw_ast_new(
						erw_ASTNODETYPE_ELSE,
						erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_ELSE)
					);

					elsenode->else_.block = erw_parse_block(parser, canbreak);
					matchnode->match.else_ = elsenode;
					break;
				}
			}

			erw_parser_expect(parser, erw_TOKENTYPE_RCURLY);
			vec_pushback(node->block.stmts, matchnode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_BREAK) 
			&& canbreak)
		{
			struct erw_ASTNode* breaknode = erw_ast_new(
				erw_ASTNODETYPE_BREAK,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_BREAK)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_END);
			vec_pushback(node->block.stmts, breaknode);
			break; //Don't parse any statements after break
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_CONTINUE) 
			&& canbreak)
		{
			struct erw_ASTNode* continuenode = erw_ast_new(
				erw_ASTNODETYPE_CONTINUE,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_CONTINUE)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_END);
			vec_pushback(node->block.stmts, continuenode);
			break; //Don't parse any statements after continue
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_FOR))
		{
			struct erw_ASTNode* fornode = erw_ast_new(
				erw_ASTNODETYPE_FOR,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_FOR)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			fornode->for_.var = erw_parse_vardeclr(parser);
			erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
			fornode->for_.expr = erw_parse_expr(parser);
			erw_parser_expect(parser, erw_TOKENTYPE_COMMA);

			struct erw_ASTNode* ident = erw_parse_expr(parser);
			if(parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_BITAND
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_ACCESS
			|| parser->tokens[parser->current].type 
				==  erw_TOKENTYPE_OPERATOR_ASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_ADDASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_SUBASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_MULASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_DIVASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_POWASSIGN 
			|| parser->tokens[parser->current].type 
				== erw_TOKENTYPE_OPERATOR_MODASSIGN)
			{
				struct erw_ASTNode* assignnode = erw_ast_new(
					erw_ASTNODETYPE_ASSIGNMENT,
					erw_parser_getnext(parser)
				);

				struct erw_ASTNode* expr = erw_parse_expr(parser);
				assignnode->assignment.assignee = ident;
				assignnode->assignment.expr = expr;
				fornode->for_.increment = assignnode;
			}

			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			fornode->for_.block = erw_parse_block(parser, 1);

			vec_pushback(node->block.stmts, fornode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_FOREACH))
		{
			struct erw_ASTNode* foreachnode = erw_ast_new(
				erw_ASTNODETYPE_FOREACH,
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_FOREACH)
			);

			erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
			struct erw_ASTNode* varnode;
			if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_LET))
			{
				varnode = erw_ast_new(
					erw_ASTNODETYPE_VARDECLR,
					erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_LET)
				);
			}
			else
			{
				varnode = erw_ast_new(
					erw_ASTNODETYPE_VARDECLR,
					erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_MUT)
				);

				varnode->vardeclr.ismut = 1;
			}

			varnode->vardeclr.name = erw_parser_expect(
				parser,
				erw_TOKENTYPE_IDENT
			);

			erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
			if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_REF))
			{
				erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_REF);
				varnode->vardeclr.isref = 1;

				if(erw_parser_check(parser, erw_TOKENTYPE_KEYWORD_MUT))
				{
					erw_parser_expect(
						parser, 
						erw_TOKENTYPE_KEYWORD_MUT
					);

					varnode->vardeclr.isrefmut = 1;
				}
			}

			varnode->vardeclr.type = erw_parse_type(parser);
			erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
			foreachnode->foreach.var = varnode;
			foreachnode->foreach.expr = erw_parse_expr(parser);
			erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
			foreachnode->foreach.block = erw_parse_block(parser, 1);

			vec_pushback(node->block.stmts, foreachnode);
			continue; //Don't require semicolon
		}
		else if(erw_parser_check(parser, erw_TOKENTYPE_LCURLY))
		{
			struct erw_ASTNode* blocknode = erw_parse_block(parser, canbreak);
			vec_pushback(node->block.stmts, blocknode);
			continue; //Don't require semicolon
		}
		else
		{ 
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Unexpected %s",
				parser->tokens[parser->current].type->name
			);

			erw_error(
				msg.data, 
				parser->lines[parser->tokens[parser->current].linenum - 1].data, 
				parser->tokens[parser->current].linenum, 
				parser->tokens[parser->current].column,
				parser->tokens[parser->current].column 
					+ vec_getsize(parser->tokens[parser->current].text) - 2
			);
			str_dtor(&msg);
		}

		erw_parser_expect(parser, erw_TOKENTYPE_END);
	}

	erw_parser_expect(parser, erw_TOKENTYPE_RCURLY);
	return node;
}

static struct erw_ASTNode* erw_parse_funcdeclr(
	struct erw_Parser* parser, 
	int isforeign)
{
	struct erw_ASTNode* funcdeclr = erw_ast_new(
		erw_ASTNODETYPE_FUNCDEF, 
		erw_parser_expect(parser, erw_TOKENTYPE_KEYWORD_FUNC)
	);

	struct erw_ASTNode* node = funcdeclr;
	funcdeclr->funcdef.name = erw_parser_expect(parser, erw_TOKENTYPE_IDENT);
	if(erw_parser_check(parser, erw_TOKENTYPE_LPAREN))
	{
		erw_parser_expect(parser, erw_TOKENTYPE_LPAREN);
		node = erw_ast_new(erw_ASTNODETYPE_TEMPLATE, NULL);
		node->template.body = funcdeclr;

		int first = 1;
		while(!erw_parser_check(parser, erw_TOKENTYPE_RPAREN))
		{
			if(first)
			{
				first = 0;
			}
			else
			{
				erw_parser_expect(parser, erw_TOKENTYPE_COMMA);
			}

			vec_pushback(
				node->template.types, 
				erw_parser_expect(parser, erw_TOKENTYPE_TYPE)
			);
			if(!erw_parser_check(parser, erw_TOKENTYPE_COMMA))
			{
				break;
			}
		}

		erw_parser_expect(parser, erw_TOKENTYPE_RPAREN);
	}

	erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_DECLR);
	funcdeclr->funcdef.isvariadic = erw_parse_varlist(
		parser, 
		funcdeclr->funcdef.params, 
		isforeign
	);

	if(erw_parser_check(parser, erw_TOKENTYPE_OPERATOR_RETURN))
	{
		erw_parser_expect(parser, erw_TOKENTYPE_OPERATOR_RETURN);
		funcdeclr->funcdef.type = erw_parse_type(parser);
	}

	if(erw_parser_check(parser, erw_TOKENTYPE_LCURLY))
	{
		funcdeclr->funcdef.block = erw_parse_block(parser, 0);
	}
	else
	{
		erw_parser_expect(parser, erw_TOKENTYPE_END);
	}

	return node;
}

struct erw_ASTNode* erw_parse(
	Vec(struct erw_Token) tokens,
	Vec(struct Str) lines,
	const char* filename)
{
	log_assert(tokens, "is NULL");
	log_assert(lines, "is NULL");
	log_assert(filename, "is NULL");
	
	erw_errorfilename = filename;
	struct erw_Parser parser = {
		.tokens = tokens,
		.lines = lines,
		.current = 0
	};

	struct erw_ASTNode* root = erw_ast_new(erw_ASTNODETYPE_START, NULL);
	root->start.filename = filename;
	while(parser.current < vec_getsize(tokens))
	{
		if(erw_parser_check(&parser, erw_TOKENTYPE_KEYWORD_FUNC))
		{
			vec_pushback(root->start.children, erw_parse_funcdeclr(&parser, 0));
		}
		else if(erw_parser_check(&parser, erw_TOKENTYPE_KEYWORD_TYPE))
		{
			vec_pushback(root->start.children, erw_parse_typedeclr(&parser));
		}
		else if(erw_parser_check(&parser, erw_TOKENTYPE_KEYWORD_IMPORT))
		{
			struct erw_ASTNode* node = erw_ast_new(
				erw_ASTNODETYPE_IMPORT,
				erw_parser_expect(&parser, erw_TOKENTYPE_KEYWORD_IMPORT)
			);

			erw_parser_expect(&parser, erw_TOKENTYPE_LPAREN);
			node->import.file = erw_parser_expect(
				&parser, 
				erw_TOKENTYPE_LITERAL_STRING
			);

			if(erw_parser_check(&parser, erw_TOKENTYPE_COMMA))
			{
				erw_parser_expect(&parser, erw_TOKENTYPE_COMMA);
				node->import.name = erw_parser_expect(
					&parser, 
					erw_TOKENTYPE_TYPE
				);
			}

			erw_parser_expect(&parser, erw_TOKENTYPE_RPAREN);
			erw_parser_expect(&parser, erw_TOKENTYPE_END);
			vec_pushback(root->start.children, node);
		}
		else if(erw_parser_check(&parser, erw_TOKENTYPE_KEYWORD_FOREIGN))
		{
			struct erw_ASTNode* node = erw_ast_new(
				erw_ASTNODETYPE_FOREIGN,
				erw_parser_expect(&parser, erw_TOKENTYPE_KEYWORD_FOREIGN)
			);

			if(erw_parser_check(&parser, erw_TOKENTYPE_LPAREN))
			{
				erw_parser_expect(&parser, erw_TOKENTYPE_LPAREN);
				node->foreign.name = erw_parser_expect(
					&parser, 
					erw_TOKENTYPE_LITERAL_STRING
				);

				erw_parser_expect(&parser, erw_TOKENTYPE_RPAREN);
			}

			if(erw_parser_check(&parser, erw_TOKENTYPE_KEYWORD_FUNC))
			{
				node->foreign.node = erw_parse_funcdeclr(&parser, 1);
			}
			else if(erw_parser_check(&parser, erw_TOKENTYPE_KEYWORD_TYPE))
			{
				node->foreign.node = erw_parse_typedeclr(&parser);
			}
			else if(erw_parser_check(&parser, erw_TOKENTYPE_KEYWORD_IMPORT))
			{
				struct erw_ASTNode* importnode = erw_ast_new(
					erw_ASTNODETYPE_IMPORT,
					erw_parser_expect(&parser, erw_TOKENTYPE_KEYWORD_IMPORT)
				);

				erw_parser_expect(&parser, erw_TOKENTYPE_LPAREN);
				importnode->import.file = erw_parser_expect(
					&parser, 
					erw_TOKENTYPE_LITERAL_STRING
				);

				/*
				erw_parser_expect(&parser, erw_TOKENTYPE_COMMA);
				importnode->import.name = erw_parser_expect(
					&parser, 
					erw_TOKENTYPE_TYPE
				);
				*/

				erw_parser_expect(&parser, erw_TOKENTYPE_RPAREN);
				erw_parser_expect(&parser, erw_TOKENTYPE_END);

				node->foreign.node = importnode;
			}
			else
			{
				struct Str msg;
				str_ctorfmt(
					&msg,
					"Expected %s or %s, got %s",
					erw_TOKENTYPE_KEYWORD_FUNC->name,
					erw_TOKENTYPE_KEYWORD_TYPE->name,
					tokens[parser.current].type->name
				);

				erw_error(
					msg.data,
					lines[tokens[parser.current].linenum - 1].data,
					tokens[parser.current].linenum,
					tokens[parser.current].column,
					tokens[parser.current].column 
						+ vec_getsize(tokens[parser.current].text) - 2
				);
				str_dtor(&msg);
			}

			vec_pushback(root->start.children, node);
		}
		else
		{
			struct Str msg;
			str_ctorfmt(
				&msg,
				"Unexpected %s",
				tokens[parser.current].type->name
			);

			erw_error(
				msg.data,
				lines[tokens[parser.current].linenum - 1].data,
				tokens[parser.current].linenum,
				tokens[parser.current].column,
				tokens[parser.current].column 
					+ vec_getsize(tokens[parser.current].text) - 2
			);
			str_dtor(&msg);
		}
	}

	return root;
}

