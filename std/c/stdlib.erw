import("stddef.erw");
foreign import("stdlib.h");

foreign("div_t")  	 type Div_t:     Int64;
foreign("ldiv_t")  	 type Ldiv_t:    Int64;
foreign("lldiv_t") 	 type Lldiv_t:   Int64;

foreign func abort: ();
foreign func exit:       (exit_code: Int);
foreign func quick_exit: (exit_code: Int);

foreign func atexit:        (f: func()) -> Int;
foreign func at_quick_exit: (f: func()) -> Int;

foreign func system:  (command: &Char) -> Int;
foreign func getenv:  (name: &Char) -> &Char;
foreign func malloc:  (size: Size_t) -> &mut Void;
foreign func calloc:  (num: Size_t, size: Size_t) -> &mut Void;
foreign func realloc: (ptr: &mut Void, new_size: Size_t) -> &mut Void;
foreign func free:    (ptr: &mut Void);
foreign func aligned_alloc: (alignment: Size_t, size: Size_t) -> &mut Char;

foreign func rand: () -> Int;
foreign func srand: (seed: Unsigned);

foreign func abs:     (n: Int) -> Int;
foreign func labs:    (n: Long) -> Long;
foreign func llabs:   (n: Longlong) -> Longlong;
foreign func div:     (x: Int, y: Int) -> Div_t;
foreign func ldiv:    (x: Long, y: Long) -> Ldiv_t;
foreign func lldiv:   (x: Longlong, y: Longlong) -> Lldiv_t;

foreign func atof:     (s: &Char) -> Float64;
foreign func atoi:     (s: &Char) -> Int;
foreign func atol:     (s: &Char) -> Long;
foreign func atoll:    (s: &Char) -> Longlong;
foreign func strtol:   (s: &Char, s_end: &mut &Char, base: Int) -> Long;
foreign func strtoll:  (s: &Char, s_end: &mut &Char, base: Int) -> Longlong;
foreign func strtoul:  (s: &Char, s_end: &mut &Char, base: Int) -> Unsignedlong;
foreign func strtoull: (s: &Char, 
						s_end: &mut &Char, 
						base: Int) -> Unsignedlonglong;
foreign func strtof:  (s: &Char, s_end: &mut &Char) -> Float32;
foreign func strtod:  (s: &Char, s_end: &mut &Char) -> Float64;
foreign func strtold: (s: &Char, s_end: &mut &Char) -> Longdouble;

foreign func bsearch: (key: &Void, 
					   base: &Void, 
					   num_items: Size_t, 
					   compare: func(&Void, &Void) -> Int32) -> &Void;
foreign func qsort:   (base: &Void, 
					   num_items: Size_t, 
					   size: Size_t, 
					   compare: func(&Void, &Void) -> Int) -> &Void;

foreign func mblen: (s: &Char, n: Size_t) -> Int;

@foreign func mbstowcs
@foreign func mbtowc
@foreign func wcstombs
@foreign func wctomb
