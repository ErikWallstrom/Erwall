# Arrays

```erw
let x: [5]Int32 = array(4, 5, 10, 0, 2);
let y: [2]&Int32 = array(&a, &b);
```

```c
struct erw_Int32Array5
{
	erw_Int32 elements[5];
};

struct erw_Int32Array5 x = (struct erw_Int32Array5){{4, 5, 10, 0, 2}};

struct erw_Int32RefArray2
{
	const erw_Int32* elements[5];
};

struct erw_Int32_Array_5 x = (struct erw_Int32Array5){{4, 5, 10, 0, 2}};
```

# Slices

```erw
let x_slice: []Int32 = &x;
```

```c
struct erw_Int32Slice
{
	erw_Int32* arr;
	size_t len;
};

struct erw_Int32Slice x_slice = {x.elements, sizeof x.elements / sizeof *x.elements};
```

# References

```erw
let x: Int32 = 19;
let x_ref: &Int32 = &x;
```

```c
erw_Int32 x = 19;
const erw_Int32* x_ref = &x;
```

# Unions

```erw
type TestUnion: union[Int32, None];
let test: TestUnion = union[Int32: 42];
```

```c
struct erw_TestUnion
{
	union
	{
		erw_Int32 int32_;
		erw_None none_;
	};

	erw_UInt8 type;
};
erw_UInt8 const erw_TestUnion_INT32 = 0;
erw_UInt8 const erw_TestUnion_NONE = 1;

struct TestUnion test = {.int32_ = 42, .type = erw_TestUnion_INT32};
```

# Enums

```erw
type TestEnum: enum[
	hello,
	world = 3,
	lol("Haha")
];

let test: TestEnum = TestEnum.world;
```

```c
struct erw_TestEnum
{
	const char* const str;
	const int value;
};

const struct erw_TestEnum* const erw_TESTENUM_HELLO = &(struct erw_TestEnum){.str = NULL, .value = 0};
const struct erw_TestEnum* const erw_TESTENUM_WORLD = &(struct erw_TestEnum){.str = NULL, .value = 3};
const struct erw_TestEnum* const erw_TESTENUM_LOL = &(struct erw_TestEnum){.str = "Haha", .value = 4};
```

# Match

```erw
match(x)
{
	case(1)
	{
	}
	case(2)
	{
	}
	case(3)
	{
	}
	else
	{
	}
}
```

```c
if(x == 1)
{
}
else if(x == 2)
{
}
else if(x == 3)
{
}
else
{
}
```

```erw
match(result)
{
	case(let x: Int32)
	{
	}
	case(let y: Float32)
	{
	}
	else
	{
	}
}
```

```c
if(result.type == erw_Union_INT32)
{
}
else if(result.type == erw_Union_FLOAT32)
{
}
else
{
}
```
