/*
	Copyright (C) 2017 Erik Wallström

	This file is part of Erwall.

	Erwall is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Erwall is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Erwall.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "erw_generator.h"
#include "log.h"

#define ERW_PREFIX "erw_"

struct erw_Generator
{
	Vec(struct erw_Type*) generatedtypes;
};

struct erw_GenerationResult
{
	struct Str header;
	struct Str code;
};

int erw_checktypegenerated(
	struct erw_Generator* generator, 
	struct erw_Type* type)
{
	for(size_t i = 0; i < vec_getsize(generator->generatedtypes); i++)
	{
		if(erw_type_compare(generator->generatedtypes[i], type))
		{
			return 1;
		}
	}

	return 0;
}

static struct erw_GenerationResult erw_generatetype(
	struct erw_Generator* generator,
	struct erw_Type* type, 
	struct erw_Scope* scope,
	const char* module)
{
	log_assert(generator, "is NULL");
	log_assert(type, "is NULL");
	log_assert(scope, "is NULL");
	log_assert(module, "is NULL");

	struct erw_GenerationResult result;
	str_ctor(&result.header, "");
	str_ctor(&result.code, "");

	struct erw_Type* name = erw_type_getnamed(type);
	if(name)
	{
		struct erw_TypeDeclr* declr;
		if(name->named.module)
		{
			struct erw_Module* importmodule = erw_scope_findmodule(
				scope, 
				name->named.module
			);

			declr = erw_scope_findtypedeclr(
				importmodule->result.scope, 
				name->named.name
			);
		}
		else
		{
			declr = erw_scope_findtypedeclr(
				scope, 
				name->named.name
			);
		}

		if(type->info != erw_TYPEINFO_REFERENCE && !declr->isforeign) 
			//Reference requires no prefix
		{
			struct erw_Scope* tempscope = scope;
			const char* lastname = "";
			int isglobal = 1;
			int found = 0;

			while(tempscope->parent)
			{
				if(!found)
				{
					for(size_t i = 0; i < vec_getsize(tempscope->types); i++)
					{
						if(erw_type_compare(tempscope->types[i].type, name))
						{
							found = 1;
							isglobal = 0;
							break;
						}
					}
				}

				if(found)
				{
					if(tempscope->isfunc)
					{
						if(strcmp(lastname, tempscope->name))
						{
							str_prependfmt(
								&result.code, 
								"%s_", 
								tempscope->name
							);

							lastname = tempscope->name;
						}
					}
					else
					{
						str_prependfmt(&result.code, "%zu_", tempscope->index);
					}
				}

				tempscope = tempscope->parent;
			}

			if(isglobal)
			{
				str_prependfmt(
					&result.code, 
					ERW_PREFIX "%s%s", 
					module,
					name->named.module ? name->named.module : ""
				);
			}
			else
			{
				str_prependfmt(
					&result.code, 
					ERW_PREFIX "%s%s_", 
					module,
					name->named.module ? name->named.module : ""
				);
			}
		}

		if(type->info == erw_TYPEINFO_NAMED)
		{
			struct erw_Type* base = erw_type_getbase(type);
			if(declr->isforeign && declr->node->foreign.name)
			{
				str_appendfmt(
					&result.code, 
					"%.*s", 
					(int)vec_getsize(declr->node->foreign.name->text) - 3,
					declr->node->foreign.name->text + 1
				);
			}
			else
			{
				str_append(&result.code, type->named.name);
				if(base->info == erw_TYPEINFO_ENUM)
				{
					str_append(&result.code, " const*");
				}
			}
		}
		else if(type->info == erw_TYPEINFO_ARRAY)
		{
			erw_generatetypename(&result.code, type);
			if(!erw_checktypegenerated(generator, type))
			{
				vec_pushback(generator->generatedtypes, type);
				str_append(&result.header, "typedef struct");
				struct erw_GenerationResult gresult = erw_generatetype(
					generator, 
					type->array.type, 
					scope,
					""
				);

				str_prepend(&result.header, gresult.header.data);
				str_appendfmt(
					&result.header, 
					"\n{\n\t%s arr[%zu];\n} ", 
					gresult.code.data,
					type->array.numelements
				);

				str_append(&result.header, result.code.data);
				str_append(&result.header, ";\n\n");
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
			}
		}
		else if(type->info == erw_TYPEINFO_SLICE)
		{
			erw_generatetypename(&result.code, type);
			if(!erw_checktypegenerated(generator, type))
			{
				vec_pushback(generator->generatedtypes, type);
				str_append(&result.header, "typedef struct");
				struct erw_GenerationResult gresult = erw_generatetype(
					generator, 
					type->slice.type, 
					scope,
					""
				);

				if(!type->slice.ismut)
				{
					str_append(&gresult.code, " const");
				}

				str_append(&gresult.code, "*");
				str_prepend(&result.header, gresult.header.data);
				str_appendfmt(
					&result.header, 
					"\n{\n\t%s arr;\n\tsize_t len;\n} ", 
					gresult.code.data
				);

				str_append(&result.header, result.code.data);
				str_append(&result.header, ";\n\n");
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
			}
		}
		else if(type->info == erw_TYPEINFO_REFERENCE)
		{
			struct erw_GenerationResult gresult = erw_generatetype(
				generator, 
				type->reference.type, 
				scope,
				""
			);

			str_prepend(&result.header, gresult.header.data);
			str_append(&result.code, gresult.code.data);
			if(!type->reference.ismut)
			{
				str_append(&result.code, " const");
			}

			str_append(&result.code, "*");
			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
		}
		else
		{
			log_assert(0, "Unhandled type: %s", erw_type_tostring(type).data);
		}
	}
	else //anonymous function/struct/union type
	{
		str_prepend(&result.code, ERW_PREFIX);
		if(type->info == erw_TYPEINFO_FUNC)
		{
			erw_generatetypename(&result.code, type);
			if(!erw_checktypegenerated(generator, type))
			{
				vec_pushback(generator->generatedtypes, type);
				str_append(&result.header, "typedef ");
				if(type->func.rettype)
				{
					struct erw_GenerationResult gresult = erw_generatetype(
						generator, 
						type->func.rettype, 
						scope,
						""
					);

					str_prepend(&result.header, gresult.header.data);
					str_append(&result.header, gresult.code.data);
					str_dtor(&gresult.code);
					str_dtor(&gresult.header);
				}
				else
				{
					str_append(&result.header, "void");
				}

				str_append(&result.header, "(*" ERW_PREFIX);
				erw_generatetypename(&result.header, type);
				str_append(&result.header, ")(");

				if(!vec_getsize(type->func.params))
				{
					str_append(&result.header, "void");
				}
				else
				{
					for(size_t i = 0; i < vec_getsize(type->func.params); i++)
					{
						if(i != 0)
						{
							str_append(&result.header, ", ");
						}

						struct erw_GenerationResult gresult = erw_generatetype(
							generator, 
							type->func.params[i],
							scope,
							""
						);

						str_prepend(&result.header, gresult.header.data);
						str_append(&result.header, gresult.code.data);
						str_dtor(&gresult.code);
						str_dtor(&gresult.header);
					}
				}

				str_append(&result.header, ");\n");
			}
		}
		else if(type->info == erw_TYPEINFO_STRUCT)
		{
			erw_generatetypename(&result.code, type);
			if(!erw_checktypegenerated(generator, type))
			{
				vec_pushback(generator->generatedtypes, type);
				str_append(&result.header, "typedef struct\n{\n");
				if(type->struct_.extensiontype)
				{
					struct erw_GenerationResult gresult = erw_generatetype(
						generator,
						type->struct_.extensiontype,
						scope,
						""
					);

					str_prepend(&result.header, gresult.header.data);
					str_appendfmt(
						&result.header, 
						"\t%s ext;\n",
						gresult.code.data
					);

					str_dtor(&gresult.code);
					str_dtor(&gresult.header);
				}

				for(size_t i = 0; i < vec_getsize(type->struct_.members); i++)
				{
					if(!type->struct_.members[i].isextension)
					{
						struct erw_GenerationResult gresult = erw_generatetype(
							generator,
							type->struct_.members[i].type,
							scope,
							""
						);
						
						str_prepend(&result.header, gresult.header.data);
						str_appendfmt(
							&result.header, 
							"\t%s " ERW_PREFIX "%s;\n",
							gresult.code.data,
							type->struct_.members[i].name
						);

						str_dtor(&gresult.code);
						str_dtor(&gresult.header);
					}
				}

				str_append(&result.header, "} " ERW_PREFIX);
				erw_generatetypename(&result.header, type);
				str_append(&result.header, ";\n\n");
			}
		}
		else if(type->info == erw_TYPEINFO_UNION)
		{
			erw_generatetypename(&result.code, type);
			if(!erw_checktypegenerated(generator, type))
			{
				vec_pushback(generator->generatedtypes, type);
				str_append(&result.header, "typedef struct\n{\n\tunion\n\t{\n");
				for(size_t i = 0; i < vec_getsize(type->union_.members); i++)
				{
					struct erw_GenerationResult gresult = erw_generatetype(
						generator,
						type->union_.members[i],
						scope,
						""
					);

					str_append(&result.header, gresult.header.data);

					struct Str membername;
					str_ctor(&membername, "");
					erw_generatetypename(&membername, type->union_.members[i]);
					str_lower(&membername);

					str_appendfmt(
						&result.header, 
						"\t\t%s " ERW_PREFIX "%s;\n", 
						gresult.code.data,
						membername.data
					);
					
					str_dtor(&membername);
					str_dtor(&gresult.code);
					str_dtor(&gresult.header);
				}

				str_append(
					&result.header, 
					"\t};\n\t" ERW_PREFIX "UInt8 type;\n} "
				);

				struct Str typename;
				str_ctor(&typename, "");
				erw_generatetypename(&typename, type);
				str_appendfmt(
					&result.header, 
					ERW_PREFIX "%s;\n\n", 
					typename.data
				);

				for(size_t i = 0; i < vec_getsize(type->union_.members); i++)
				{
					struct Str membername;
					str_ctor(&membername, "");
					erw_generatetypename(&membername, type->union_.members[i]);
					str_upper(&membername);

					str_appendfmt(
						&result.header, 
						"erw_UInt8 const " ERW_PREFIX "%s_%s = %zu", 
						typename.data,
						membername.data,
						i
					);
					
					if(i != vec_getsize(type->union_.members))
					{
						str_append(&result.header, ";\n");
					}

					str_dtor(&membername);
				}

				str_dtor(&typename);
			}
		}
		else
		{
			log_assert(0, "Unhandled type: %s", erw_type_tostring(type).data);
		}
	}

	return result;
}

static struct erw_GenerationResult erw_generatetypedeclr(
	struct erw_Generator* generator,
	struct erw_TypeDeclr* type, 
	struct erw_Scope* scope,
	const char* module)
{
	log_assert(generator, "is NULL");
	log_assert(type, "is NULL");
	log_assert(scope, "is NULL");
	log_assert(module, "is NULL");

	struct erw_GenerationResult result;
	str_ctor(&result.header, "");

	if(type->type->named.type->info == erw_TYPEINFO_EMPTY)
	{
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			type->type,
			scope,
			module
		);

		str_append(&result.header, gresult.header.data);
		str_ctorfmt(
			&result.code, 
			"typedef struct %s %s;\nstruct %s\n{\n\tchar _;\n}", 
			gresult.code.data,
			gresult.code.data,
			gresult.code.data
		);

		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
	}
	else if(type->type->named.type->info == erw_TYPEINFO_STRUCT)
	{
		struct erw_GenerationResult gresult1 = erw_generatetype(
			generator,
			type->type,
			scope,
			module
		);

		str_ctorfmt(
			&result.code, 
			"typedef struct %s %s;\n", 
			gresult1.code.data, 
			gresult1.code.data
		);

		str_append(&result.header, gresult1.header.data);
		str_appendfmt(&result.code, "struct %s\n{\n", gresult1.code.data);
		str_dtor(&gresult1.code);
		str_dtor(&gresult1.header);

		if(type->type->named.type->struct_.extensiontype)
		{
			struct erw_GenerationResult gresult = erw_generatetype(
				generator,
				type->type->named.type->struct_.extensiontype,
				scope,
				""
			);

			str_prepend(&result.header, gresult.header.data);
			str_appendfmt(
				&result.code, 
				"\t%s ext;\n",
				gresult.code.data
			);

			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
		}

		for(size_t i = 0; 
			i < vec_getsize(type->type->named.type->struct_.members);
			i++)
		{
			if(!type->type->named.type->struct_.members[i].isextension)
			{
				struct erw_GenerationResult gresult = erw_generatetype(
					generator,
					type->type->named.type->struct_.members[i].type,
					scope,
					"" //?
				);

				str_append(&result.header, gresult.header.data);
				str_appendfmt(
					&result.code, 
					"\t%s " ERW_PREFIX "%s;\n", 
					gresult.code.data,
					type->type->named.type->struct_.members[i].name
				);

				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
			}
		}

		str_append(&result.code, "}");
	}
	else if(type->type->named.type->info == erw_TYPEINFO_UNION)
	{
		struct erw_GenerationResult gresult1 = erw_generatetype(
			generator,
			type->type,
			scope,
			module
		);

		str_ctorfmt(
			&result.code, 
			"typedef struct %s %s;\n", 
			gresult1.code.data, 
			gresult1.code.data
		);

		str_append(&result.header, gresult1.header.data);
		str_appendfmt(
			&result.code, 
			"struct %s\n{\n\tunion\n\t{\n", 
			gresult1.code.data
		);

		for(size_t i = 0; 
			i < vec_getsize(type->type->named.type->union_.members);
			i++)
		{
			struct erw_GenerationResult gresult = erw_generatetype(
				generator,
				type->type->named.type->union_.members[i],
				scope,
				""
			);

			str_append(&result.header, gresult.header.data);
			struct Str membername;
			str_ctor(&membername, "");
			erw_generatetypename(
				&membername, 
				type->type->named.type->union_.members[i]
			);
			str_lower(&membername);

			str_appendfmt(
				&result.code, 
				"\t\t%s " ERW_PREFIX "%s;\n", 
				gresult.code.data,
				membername.data
			);
			
			str_dtor(&membername);
			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
		}

		str_append(&result.code, "\t};\n\t" ERW_PREFIX "UInt8 type;\n};\n\n");
		for(size_t i = 0; 
			i < vec_getsize(type->type->named.type->union_.members);
			i++)
		{
			struct Str membername;
			str_ctor(&membername, "");
			erw_generatetypename(
				&membername, 
				type->type->named.type->union_.members[i]
			);
			str_upper(&membername);

			str_appendfmt(
				&result.code, 
				"erw_UInt8 const %s_%s = %zu", 
				gresult1.code.data,
				membername.data,
				i
			);
			
			if(i != vec_getsize(type->type->named.type->union_.members) - 1)
			{
				str_append(&result.code, ";\n");
			}

			str_dtor(&membername);
		}

		str_dtor(&gresult1.code);
		str_dtor(&gresult1.header);
	}
	else if(type->type->named.type->info == erw_TYPEINFO_ENUM)
	{
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			type->type,
			scope,
			module
		);

		str_ctorfmt(
			&result.code, 
			"typedef struct %.*s %.*s;\n", 
			(int)(gresult.code.len - sizeof "const*"),
			gresult.code.data, 
			(int)(gresult.code.len - sizeof "const*"),
			gresult.code.data
		);

		str_append(&result.header, gresult.header.data);
		str_appendfmt(
			&result.code, 
			"struct %.*s\n{\n\tchar const* const str;\n\tint const value;\n};"
				"\n\n",
			(int)(gresult.code.len - sizeof "const*"),
			gresult.code.data
		);

		for(size_t i = 0; 
			i < vec_getsize(type->type->named.type->enum_.members);
			i++)
		{
			struct Str uppername;
			str_ctor(&uppername, type->type->named.type->enum_.members[i].name);
			str_upper(&uppername);
			str_appendfmt(
				&result.code, 
				"%s const %.*s_%s = &(%.*s){.value = %zu,"
					" .str = ", 
				gresult.code.data,
				(int)(gresult.code.len - sizeof "const*"), 
				gresult.code.data,
				uppername.data,
				(int)(gresult.code.len - sizeof "const*"), 
				gresult.code.data,
				type->type->named.type->enum_.members[i].value
			);

			if(type->node->typedeclr.type->enum_.members[i]->enummember.str)
			{
				str_appendfmt(
					&result.code, 
					"%s}", 
					type->node->typedeclr.type->enum_.members[i]->enummember 
						.str->text
				);
			}
			else
			{
				struct Str upper;
				str_ctor(&upper, type->type->named.name);
				str_upper(&upper);
				str_appendfmt(
					&result.code,
					"\"%s_%s\"}", 
					upper.data,
					uppername.data
				);

				str_dtor(&upper);
			}
			
			if(i != vec_getsize(type->type->named.type->enum_.members) - 1)
			{
				str_append(&result.code, ";\n");
			}

			str_dtor(&uppername);
		}

		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
	}
	else
	{
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			type->type->named.type,
			scope,
			""
		);

		str_append(&result.header, gresult.header.data);
		str_ctorfmt(&result.code, "typedef %s ", gresult.code.data);
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);

		gresult = erw_generatetype(
			generator,
			type->type,
			scope,
			module
		);

		str_append(&result.header, gresult.header.data);
		str_append(&result.code, gresult.code.data);
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
		str_append(&result.code, ";\n"); //Only one newline, ugly
		return result;
	}

	str_append(&result.code, ";\n\n");
	return result;
}

static struct Str erw_generatefuncname(
	struct erw_FuncDeclr* func,
	struct erw_Scope* scope,
	const char* module)
{
	log_assert(func, "is NULL");
	log_assert(scope, "is NULL");
	log_assert(module, "is NULL");

	struct Str result;
	str_ctor(&result, "");

	int isglobal = 1;
	struct erw_Scope* tempscope = scope;
	const char* lastname = "";
	int found = 0;
	while(tempscope->parent)
	{
		if(!found)
		{
			for(size_t i = 0; i < vec_getsize(tempscope->functions); i++)
			{
				if(&tempscope->functions[i] == func)
				{
					found = 1;
					isglobal = 0;
					break;
				}
			}
		}

		if(found)
		{
			if(tempscope->isfunc)
			{
				if(strcmp(lastname, tempscope->name))
				{
					str_prependfmt(
						&result, 
						"%s_", 
						tempscope->name
					);

					lastname = tempscope->name;
				}
			}
			else
			{
				str_prependfmt(&result, "%zu_", tempscope->index);
			}
		}

		tempscope = tempscope->parent;
	}

	if(isglobal)
	{
		str_prependfmt(&result, ERW_PREFIX "%s", module);
	}
	else
	{
		str_prependfmt(&result, ERW_PREFIX "%s_", module);
	}

	str_append(&result, func->name);
	return result;
}

static struct erw_GenerationResult erw_generatefuncprot(
	struct erw_Generator* generator,
	struct erw_FuncDeclr* func, 
	struct erw_Scope* scope,
	const char* module)
{
	log_assert(generator, "is NULL");
	log_assert(func, "is NULL");
	log_assert(scope, "is NULL");
	log_assert(module, "is NULL");

	struct erw_GenerationResult result;
	str_ctor(&result.header, "");

	if(func->rettype)
	{
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			func->rettype,
			scope,
			""
		);

		str_ctorfmt(&result.code, "%s ", gresult.code.data);
		str_append(&result.header, gresult.header.data);

		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
	}
	else
	{
		str_ctor(&result.code, "void ");
	}

	struct Str name = erw_generatefuncname(func, scope, module);
	str_append(&result.code, name.data);
	if(!vec_getsize(func->params))
	{
		str_append(&result.code, "(void)");
	}
	else
	{
		str_append(&result.code, "(");
		for(size_t i = 0; i < vec_getsize(func->params); i++)
		{
			struct erw_GenerationResult gresult = erw_generatetype(
				generator,
				func->params[i]->type,
				scope,
				""
			);

			str_append(&result.header, gresult.header.data);
			str_appendfmt(
				&result.code, 
				"%s " ERW_PREFIX "%s", 
				gresult.code.data,
				func->params[i]->name
			);

			if(i != vec_getsize(func->params) - 1)
			{
				str_append(&result.code, ", ");
			}

			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
		}

		str_append(&result.code, ")");
	}

	str_dtor(&name);
	return result;
}

static struct erw_GenerationResult erw_generateblock(
	struct erw_Generator* generator,
	struct erw_ASTNode* node,
	struct erw_Scope* scope,
	size_t indentlvl,
	const char* module
);

static struct erw_GenerationResult erw_generateexpr(
	struct erw_Generator* generator,
	struct erw_ASTNode* node,
	struct erw_Scope* scope,
	size_t* scopecounter,
	const char* module)
{
	log_assert(generator, "is NULL");
	log_assert(node, "is NULL");
	log_assert(scope, "is NULL");
	log_assert(scopecounter, "is NULL");
	log_assert(module, "is NULL");

	struct erw_GenerationResult result;
	str_ctor(&result.header, "");
	str_ctor(&result.code, "");

	if(node->type == erw_ASTNODETYPE_BINEXPR)
	{
		if(node->binexpr.expr1->type == erw_ASTNODETYPE_TYPE)
		{
			struct erw_Module* importmodule = erw_scope_findmodule(
				scope,
				node->binexpr.expr1->token->text
			);

			struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
				importmodule->result.scope,
				node->binexpr.expr2->token->text,
				1
			);

			struct Str name = erw_generatefuncname(
				func, 
				scope, 
				importmodule->name
			);

			str_append(&result.code, name.data);
			str_dtor(&name);
		}
		else
		{
			struct erw_GenerationResult expr1 = erw_generateexpr(
				generator, 
				node->binexpr.expr1, 
				scope,
				scopecounter,
				""
			);

			struct erw_GenerationResult expr2 = erw_generateexpr(
				generator, 
				node->binexpr.expr2, 
				scope,
				scopecounter,
				""
			);

			if(node->token->type == erw_TOKENTYPE_OPERATOR_POW)
			{
				//TODO: Fix generation for integers etc
				str_appendfmt(
					&result.code, 
					"pow(%s, %s)", 
					expr1.code.data, 
					expr2.code.data
				);
			}
			else if(node->token->type == erw_TOKENTYPE_OPERATOR_AND)
			{
				str_appendfmt(
					&result.code, 
					"(%s && %s)", 
					expr1.code.data, 
					expr2.code.data
				);
			}
			else if(node->token->type == erw_TOKENTYPE_OPERATOR_OR)
			{
				str_appendfmt(
					&result.code, 
					"(%s || %s)", 
					expr1.code.data, 
					expr2.code.data
				);
			}
			else if(node->token->type == erw_TOKENTYPE_OPERATOR_ACCESS)
			{
				str_appendfmt(&result.code, "%s.", expr1.code.data);
				struct erw_Type* base = erw_type_getbase(node->binexpr.accesstype);
				if(base->struct_.extensiontype)
				{
					while(base->struct_.extensiontype)
					{
						for(size_t j = 0; 
							j < vec_getsize(base->struct_.members); 
							j++)
						{
							if(!strcmp(
								expr2.code.data, 
								base->struct_.members[j].name))
							{
								if(!base->struct_.members[j].isextension)
								{
									goto done1;
								}
							}
						}

						str_append(&result.code, "ext.");
						base = erw_type_getbase(base->struct_.extensiontype);
					}
				}

			done1:
				str_append(&result.code, expr2.code.data);
			}
			else
			{
				str_appendfmt(
					&result.code, 
					"(%s %s %s)", 
					expr1.code.data, 
					node->token->text, 
					expr2.code.data
				);
			}

			str_dtor(&expr2.code);
			str_dtor(&expr2.header);
			str_dtor(&expr1.code);
			str_dtor(&expr1.header);
		}
	}
	else if(node->type == erw_ASTNODETYPE_UNEXPR)
	{
		struct erw_GenerationResult expr = erw_generateexpr(
			generator, 
			node->unexpr.expr, 
			scope,
			scopecounter,
			""
		);

		str_append(&result.header, expr.header.data);
		if(node->token->type == erw_TOKENTYPE_OPERATOR_NOT
			|| node->token->type == erw_TOKENTYPE_OPERATOR_SUB)
		{
			str_appendfmt(
				&result.code, 
				"%s(%s)", 
				node->token->text, 
				expr.code.data
			);
		}
		else if(node->token->type == erw_TOKENTYPE_OPERATOR_BITAND)
		{
			if(node->unexpr.left)
			{
				if(node->unexpr.literaltype)
				{
					struct erw_GenerationResult gresult = erw_generatetype(
						generator,
						node->unexpr.literaltype,
						scope,
						""
					);

					str_appendfmt(
						&result.code, 
						"(%s){%s.arr, sizeof %s.arr / sizeof"
							" %s.arr[0]}", 
						gresult.code.data, 
						expr.code.data,
						expr.code.data,
						expr.code.data
					);

					str_dtor(&gresult.code);
					str_dtor(&gresult.header);
				}
				else
				{
					str_appendfmt(&result.code, "&%s", expr.code.data);
				}
			}
			else
			{
				str_appendfmt(&result.code, "(*%s)", expr.code.data);
			}
		}
		else
		{
			log_assert(0, "Unhandled type: %s", node->token->type->name);
		}

		str_dtor(&expr.code);
		str_dtor(&expr.header);
	}
	else if(node->type == erw_ASTNODETYPE_CAST)
	{
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			node->cast.casttype,
			scope,
			""
		);

		struct erw_GenerationResult expr = erw_generateexpr(
			generator, 
			node->cast.expr, 
			scope,
			scopecounter,
			""
		);

		str_appendfmt(
			&result.code, 
			"(%s)(%s)", 
			gresult.code.data, 
			expr.code.data
		);

		str_dtor(&expr.code);
		str_dtor(&expr.header);
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
	}
	else if(node->type == erw_ASTNODETYPE_BUILTINCALL)
	{
		if(node->token->type == erw_TOKENTYPE_KEYWORD_LENGTHOF)
		{
			struct erw_GenerationResult expr = erw_generateexpr(
				generator, 
				node->builtincall.param, 
				scope,
				scopecounter,
				""
			);

			struct erw_Type* type = node->builtincall.builtintype;
			if(type->info == erw_TYPEINFO_ARRAY)
			{
				str_appendfmt(
					&result.code, 
					"(sizeof(%s.arr) / sizeof(%s.arr[0]))", 
					expr.code.data,
					expr.code.data
				);
			}
			else //Assume slice
			{
				str_appendfmt(&result.code, "%s.len", expr.code.data);
			}

			str_dtor(&expr.code);
			str_dtor(&expr.header);
		}
		else if(node->token->type == erw_TOKENTYPE_KEYWORD_SIZEOF)
		{
			struct erw_GenerationResult expr = erw_generateexpr(
				generator, 
				node->builtincall.param, 
				scope,
				scopecounter,
				""
			);
			
			str_appendfmt(&result.code, "sizeof(%s)", expr.code.data);
			str_dtor(&expr.code);
			str_dtor(&expr.header);
		}
		else if(node->token->type == erw_TOKENTYPE_KEYWORD_ENUMVALUE)
		{
			struct erw_GenerationResult expr = erw_generateexpr(
				generator, 
				node->builtincall.param, 
				scope,
				scopecounter,
				""
			);

			str_appendfmt(&result.code, "%s->value", expr.code.data);
			str_dtor(&expr.code);
			str_dtor(&expr.header);
		}
		else if(node->token->type == erw_TOKENTYPE_KEYWORD_ENUMNAME)
		{
			struct erw_GenerationResult expr = erw_generateexpr(
				generator, 
				node->builtincall.param, 
				scope,
				scopecounter,
				""
			);

			str_appendfmt(&result.code, "%s->str", expr.code.data);
			str_dtor(&expr.code);
			str_dtor(&expr.header);
		}
	}
	else if(node->type == erw_ASTNODETYPE_STRUCTLITERAL)
	{
		struct erw_Type* type = node->structliteral.literaltype;
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			type,
			scope,
			""
		);

		str_appendfmt(&result.code, "(%s){", gresult.code.data);
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);

		for(size_t i = 0; i < vec_getsize(node->structliteral.names); i++)
		{
			struct erw_GenerationResult expr = erw_generateexpr(
				generator, 
				node->structliteral.values[i], 
				scope,
				scopecounter,
				""
			);

			str_append(&result.code, ".");
			struct erw_Type* base = erw_type_getbase(type);
			if(base->struct_.extensiontype)
			{
				while(base->struct_.extensiontype)
				{
					for(size_t j = 0; 
						j < vec_getsize(base->struct_.members); 
						j++)
					{
						if(!strcmp(
							node->structliteral.names[i]->text, 
							base->struct_.members[j].name))
						{
							if(!base->struct_.members[j].isextension)
							{
								goto done2;
							}
						}
					}

					str_append(&result.code, "ext.");
					base = erw_type_getbase(base->struct_.extensiontype);
				}
			}

		done2:
			str_appendfmt(
				&result.code, 
				ERW_PREFIX "%s = %s", 
				node->structliteral.names[i]->text,
				expr.code.data
			);

			if(i != vec_getsize(node->structliteral.names) - 1)
			{
				str_append(&result.code, ", ");
			}

			str_dtor(&expr.code);
			str_dtor(&expr.header);
		}

		str_append(&result.code, "}");
	}
	else if(node->type == erw_ASTNODETYPE_ARRAYLITERAL)
	{
		if(!erw_checktypegenerated(generator, node->arrayliteral.literaltype))
		{
			vec_pushback(
				generator->generatedtypes, 
				node->arrayliteral.literaltype
			);

			str_append(&result.header, "typedef struct");
			struct erw_GenerationResult gresult = erw_generatetype(
				generator, 
				((struct erw_Type*)(node->arrayliteral.literaltype))->array
					.type, 
				scope,
				""
			);

			str_prepend(&result.header, gresult.header.data);
			str_appendfmt(
				&result.header, 
				"\n{\n\t%s arr[%zu];\n} " ERW_PREFIX, 
				gresult.code.data,
				((struct erw_Type*)(node->arrayliteral.literaltype))->array
					.numelements
			);

			erw_generatetypename(
				&result.header, 
				node->arrayliteral.literaltype
			);

			str_append(&result.header, ";\n\n");
			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
		}

		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			node->arrayliteral.literaltype,
			scope,
			""
		);

		str_appendfmt(&result.code, "(%s){{", gresult.code.data);
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);

		if(node->arrayliteral.length)
		{
			size_t len = 0;
			sscanf( //XXX
				node->arrayliteral.length->token->text, 
				"%zu", 
				&len
			);

			struct erw_GenerationResult expr = erw_generateexpr(
				generator, 
				node->arrayliteral.values[0], 
				scope,
				scopecounter,
				""
			);

			for(size_t i = 0; i < len; i++)
			{
				str_append(&result.code, expr.code.data);
				if(i != len - 1)
				{
					str_append(&result.code, ", ");
				}
			}

			str_dtor(&expr.code);
			str_dtor(&expr.header);
		}
		else
		{
			for(size_t i = 0; i < vec_getsize(node->arrayliteral.values); i++)
			{
				struct erw_GenerationResult expr = erw_generateexpr(
					generator, 
					node->arrayliteral.values[i], 
					scope,
					scopecounter,
					""
				);

				str_append(&result.code, expr.code.data);
				if(i != vec_getsize(node->arrayliteral.values) - 1)
				{
					str_append(&result.code, ", ");
				}

				str_dtor(&expr.code);
				str_dtor(&expr.header);
			}
		}

		str_append(&result.code, "}}");
	}
	else if(node->type == erw_ASTNODETYPE_UNIONLITERAL)
	{
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			node->unionliteral.literaltype,
			scope,
			""
		);

		str_appendfmt(&result.code, "(%s){", gresult.code.data);
		if(node->unionliteral.value)
		{
			str_append(&result.code, "." ERW_PREFIX);
			struct Str typename;
			str_ctor(&typename, "");

			erw_generatetypename(&typename, node->unionliteral.uniontype);
			str_lower(&typename);
			str_append(&result.code, typename.data);
			str_dtor(&typename);

			struct erw_GenerationResult expr = erw_generateexpr(
				generator, 
				node->unionliteral.value, 
				scope,
				scopecounter,
				""
			);

			str_appendfmt(&result.code, " = %s, ", expr.code.data);
			str_dtor(&expr.code);
			str_dtor(&expr.header);
		}

		struct Str name;
		str_ctor(&name, "");
		erw_generatetypename(&name, node->unionliteral.uniontype);
		str_upper(&name);
		str_appendfmt(
			&result.code, 
			".type = %s_%s}", 
			gresult.code.data, 
			name.data
		);

		str_dtor(&name);
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
	}
	else if(node->type == erw_ASTNODETYPE_ENUMLITERAL)
	{
		struct erw_GenerationResult gresult = erw_generatetype(
			generator,
			node->enumliteral.literaltype,
			scope,
			""
		);

		//No need for header; should already be generated
		struct Str upper;
		str_ctor(&upper, node->enumliteral.name->text);
		str_upper(&upper);
		str_appendfmt(&result.code, 
			"%.*s_%s", 
			(int)(gresult.code.len - sizeof "const*"), 
			gresult.code.data, 
			upper.data
		);

		str_dtor(&upper);
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
	}
	else if(node->type == erw_ASTNODETYPE_FUNCCALL)
	{
		if(node->funccall.callee->type == erw_ASTNODETYPE_LITERAL)
		{
			struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
				scope, 
				node->funccall.callee->token->text,
				1
			);

			if(func)
			{
				if(!func->isforeign)
				{
					struct Str name = erw_generatefuncname(func, scope, module);
					str_appendfmt(&result.code, "%s(", name.data);
					str_dtor(&name);
				}
				else
				{
					if(func->node->foreign.name)
					{
						str_appendfmt(
							&result.code, 
							"%.*s(",
							(int)vec_getsize(
								func->node->foreign.name->text
							) - 3,
							func->node->foreign.name->text + 1
						);
					}
					else
					{
						str_appendfmt(&result.code, "%s(", func->name);
					}
				}
			}
			else //function pointer
			{
				str_appendfmt(
					&result.code, 
					ERW_PREFIX "%s(", 
					node->funccall.callee->token->text
				);
			}
		}
		else
		{
			if(node->funccall.callee->binexpr.expr1->type 
				== erw_ASTNODETYPE_TYPE)
			{
				struct erw_Module* importmodule = erw_scope_findmodule(
					scope,
					node->funccall.callee->binexpr.expr1->token->text
				);

				struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
					importmodule->result.scope,
					node->funccall.callee->binexpr.expr2->token->text,
					1
				);
				
				if(func)
				{
					if(!func->isforeign)
					{
						struct Str name = erw_generatefuncname(
							func, 
							scope, 
							importmodule->name
						);

						str_appendfmt(&result.code, "%s(", name.data);
						str_dtor(&name);
					}
					else
					{
						if(func->node->foreign.name)
						{
							str_appendfmt(
								&result.code, 
								"%.*s(",
								(int)vec_getsize(
									func->node->foreign.name->text
								) - 3,
								func->node->foreign.name->text + 1
							);
						}
						else
						{
							str_appendfmt(&result.code, "%s(", func->name);
						}
					}
				}
				else //function pointer
				{
					str_appendfmt(
						&result.code, 
						ERW_PREFIX "%s(", 
						node->funccall.callee->token->text
					);
				}
			}
			else //struct function pointer
			{
				struct erw_GenerationResult name = erw_generateexpr(
					generator, 
					node->funccall.callee,
					scope,
					scopecounter,
					module
				);

				str_appendfmt(&result.code, "%s(", name.code.data);
				str_dtor(&name.code);
				str_dtor(&name.header);
			}
		}

		for(size_t i = 0; i < vec_getsize(node->funccall.args); i++)
		{
			struct erw_GenerationResult expr = erw_generateexpr(
				generator,
				node->funccall.args[i],
				scope,
				scopecounter,
				""
			);

			str_append(&result.code, expr.code.data);
			str_append(&result.header, expr.header.data);
			str_dtor(&expr.code);
			str_dtor(&expr.header);

			if(i != vec_getsize(node->funccall.args) - 1)
			{
				str_append(&result.code, ", ");
			}
		}

		str_append(&result.code, ")");
	}
	else if(node->type == erw_ASTNODETYPE_TEMPLATECALL)
	{
		return erw_generateexpr(
			generator, 
			node->templatecall.body, 
			scope, 
			scopecounter, 
			""
		);
	}
	else if(node->type == erw_ASTNODETYPE_LITERAL)
	{
		if(node->token->type == erw_TOKENTYPE_LITERAL_BOOL)
		{
			if(!strcmp(node->token->text, "true"))
			{ 
				str_append(&result.code, ERW_PREFIX "TRUE");
			}
			else
			{ 
				str_append(&result.code, ERW_PREFIX "FALSE");
			}
		}
		else if(node->token->type == erw_TOKENTYPE_IDENT)
		{
			struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
				scope,
				node->token->text,
				0
			);

			if(func)
			{
				struct Str name = erw_generatefuncname(func, scope, module);
				str_append(&result.code, name.data);
				str_dtor(&name);
			}
			else
			{
				if(node->extensiontypes.extensiontype)
				{
					struct erw_Type* type = node->extensiontypes.structtype;
					if(type->info == erw_TYPEINFO_REFERENCE)
					{
						struct erw_GenerationResult gresult = erw_generatetype(
							generator,
							node->extensiontypes.extensiontype,
							scope,
							""
						);

						str_appendfmt(&result.code, 
							"(%s)" ERW_PREFIX "%s", 
							gresult.code.data,
							node->token->text
						);

						str_dtor(&gresult.code);
						str_dtor(&gresult.header);
					}
					else
					{
						str_appendfmt(
							&result.code, 
							ERW_PREFIX "%s", 
							node->token->text
						);

						while(1)
						{
							if(!erw_type_compare(
								type, 
								node->extensiontypes.extensiontype))
							{
								type = erw_type_getbase(type)->struct_
									.extensiontype;

								str_append(&result.code, ".ext");
							}
							else
							{
								break;
							}
						}
					}
				}
				else
				{
					str_appendfmt(
						&result.code, 
						ERW_PREFIX "%s", 
						node->token->text
					);
				}
			}
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_FLOAT)
		{
			str_appendfmt(&result.code, "%s", node->token->text);
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_STRING)
		{
			str_appendfmt(
				&result.code, 
				"(erw_CharSlice){%s, %zu}", 
				node->token->text,
				vec_getsize(node->token->text) - 2
			);
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_INT)
		{
			str_append(&result.code, node->token->text);
		}
		else if(node->token->type == erw_TOKENTYPE_LITERAL_CHAR)
		{
			str_append(&result.code, node->token->text);
		}
		else
		{
			log_assert(0, "Unhandled type: %s", node->token->type->name);
		}
	}
	else if(node->type == erw_ASTNODETYPE_ACCESS)
	{
		struct erw_GenerationResult expr = erw_generateexpr(
			generator, 
			node->access.expr,
			scope,
			scopecounter,
			""
		);

		str_append(&result.code, expr.code.data);
		str_append(&result.code, ".arr");

		struct erw_GenerationResult index = erw_generateexpr(
			generator, 
			node->access.index,
			scope,
			scopecounter,
			""
		);

		str_appendfmt(&result.code, "[%s]", index.code.data);
		str_dtor(&index.code);
		str_dtor(&index.header);
		str_dtor(&expr.code);
		str_dtor(&expr.header);
	}
	else if(node->type == erw_ASTNODETYPE_FUNCLITERAL)
	{
		if(node->funcliteral.functype->func.rettype)
		{
			struct erw_GenerationResult gresult = erw_generatetype(
				generator,
				node->funcliteral.functype->func.rettype,
				scope,
				""
			);

			str_append(&result.header, gresult.header.data);
			str_appendfmt(&result.header, "%s ", gresult.code.data);

			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
		}
		else
		{
			str_append(&result.header, "void ");
		}

		struct Str name;
		str_ctor(&name, "");
		erw_generatetypename(&name, node->funcliteral.functype);

		struct erw_Scope* tempscope = scope;
		while(tempscope->parent)
		{
			if(tempscope->isfunc)
			{
				str_prependfmt(&name, "%s_", tempscope->name);
			}
			else
			{
				str_prependfmt(&name, "%zu_", tempscope->index);
			}

			tempscope = tempscope->parent;
		}

		str_prependfmt(&name, ERW_PREFIX "%s_", module);
		str_append(&result.code, name.data);
		str_append(&result.header, name.data);
		str_dtor(&name);

		struct erw_Scope* funcscope = scope->children[*scopecounter];
		(*scopecounter)++;
		if(!vec_getsize(node->funcliteral.params))
		{
			str_append(&result.header, "(void)");
		}
		else
		{
			str_append(&result.header, "(");
			for(size_t i = 0; i < vec_getsize(node->funcliteral.params); i++)
			{
				struct erw_GenerationResult gresult = erw_generatetype(
					generator,
					funcscope->variables[i].type,
					funcscope,
					""
				);

				str_append(&result.header, gresult.header.data);
				str_appendfmt(
					&result.header, 
					"%s " ERW_PREFIX "%s", 
					gresult.code.data,
					funcscope->variables[i].name
				);

				if(i != vec_getsize(node->funcliteral.functype->func.params) 
					- 1)
				{
					str_append(&result.code, ", ");
				}

				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
			}

			str_append(&result.header, ")");
		}

		struct erw_GenerationResult gresult = erw_generateblock(
			generator, 
			node->funcliteral.block,
			funcscope,
			0,
			module
		);

		str_append(&result.header, "\n");
		str_prepend(&result.header, gresult.header.data);
		str_append(&result.header, gresult.code.data);
		str_append(&result.header, "\n");
		
		str_dtor(&gresult.code);
		str_dtor(&gresult.header);
	}
	else
	{
		log_assert(0, "Unhandled type: %s", node->type->name);
	}

	return result;
}

static struct erw_GenerationResult erw_generateblock(
	struct erw_Generator* generator,
	struct erw_ASTNode* node,
	struct erw_Scope* scope,
	size_t indentlvl,
	const char* module)
{ 
	log_assert(generator, "is NULL");
	log_assert(node, "is NULL");
	log_assert(
		node->type == erw_ASTNODETYPE_BLOCK, 
		"invalid type (%s)", 
		node->type->name
	);
	log_assert(scope, "is NULL");
	log_assert(module, "is NULL");

	size_t scopecounter = 0;
	size_t funccounter = 0;
	size_t typecounter = 0;
	size_t varcounter = 0;

	if(scope->isfunc)
	{
		varcounter += vec_getsize(
			erw_scope_findfuncdeclr(scope, scope->name, 0)->node->funcdef.params
		);
	}

	for(size_t i = 0; i < vec_getsize(scope->variables); i++)
	{
		if(scope->variables[i].isref)
		{
			varcounter++;
		}
	}

	struct erw_GenerationResult result;
	str_ctor(&result.code, "");
	str_ctor(&result.header, "");

	for(size_t i = 0; i < indentlvl; i++)
	{ 
		str_append(&result.code, "\t");
	}
	
	str_append(&result.code, "{\n");
	for(size_t i = 0; i < vec_getsize(node->block.stmts); i++)
	{
		if(node->block.stmts[i]->type == erw_ASTNODETYPE_TYPEDECLR)
		{
			struct erw_TypeDeclr* type = &scope->types[typecounter];
			struct erw_GenerationResult gresult = erw_generatetypedeclr(
				generator, 
				type, 
				scope,
				module
			);

			str_append(&result.header, gresult.header.data);
			str_append(&result.header, gresult.code.data);
			str_dtor(&gresult.code);
			str_dtor(&gresult.header);

			typecounter++;
		}
		else if(node->block.stmts[i]->type == erw_ASTNODETYPE_FUNCDEF)
		{
			struct erw_FuncDeclr* func = &scope->functions[funccounter];
			struct erw_GenerationResult gresult = erw_generatefuncprot(
				generator, 
				func, 
				func->scope,
				module
			);

			struct erw_GenerationResult gresult2 = erw_generateblock(
				generator, 
				func->node->funcdef.block, 
				func->scope,
				0,
				module
			);

			str_append(&result.header, gresult.header.data);
			str_append(&result.header, gresult2.header.data);

			str_append(&result.header, gresult.code.data);
			str_append(&result.header, "\n");
			str_append(&result.header, gresult2.code.data);
			str_append(&result.header, "\n");

			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
			str_dtor(&gresult2.code);
			str_dtor(&gresult2.header);

			scopecounter++;
			funccounter++;
		}
		else if(node->block.stmts[i]->type == erw_ASTNODETYPE_FOREIGN)
		{
			if(node->block.stmts[i]->foreign.node->type 
				== erw_ASTNODETYPE_TYPEDECLR)
			{
				typecounter++;
			}
			else if(node->block.stmts[i]->foreign.node->type 
				== erw_ASTNODETYPE_FUNCDEF)
			{
				funccounter++;
			}
		}
		else if(node->block.stmts[i]->type == erw_ASTNODETYPE_BREAK)
		{
			for(size_t j = 0; j < indentlvl + 1; j++)
			{ 
				str_append(&result.code, "\t");
			}

			str_append(&result.code, "break;\n");
		}
		else if(node->block.stmts[i]->type == erw_ASTNODETYPE_CONTINUE)
		{
			for(size_t j = 0; j < indentlvl + 1; j++)
			{ 
				str_append(&result.code, "\t");
			}

			str_append(&result.code, "continue;\n");
		}
		else
		{
			for(size_t j = 0; j < indentlvl + 1; j++)
			{ 
				str_append(&result.code, "\t");
			}

			if(node->block.stmts[i]->type == erw_ASTNODETYPE_VARDECLR)
			{
				struct erw_VarDeclr* var = &scope->variables[varcounter];
				struct erw_GenerationResult gresult = erw_generatetype(
					generator,
					var->type,
					scope,
					""
				);

				str_append(&result.header, gresult.header.data);
				str_append(&result.code, gresult.code.data);
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);

				str_appendfmt(
					&result.code, 
					" " ERW_PREFIX "%s", 
					var->node->vardeclr.name->text
				);
				
				if(var->node->vardeclr.value)
				{
					struct erw_GenerationResult expr = erw_generateexpr(
						generator,
						var->node->vardeclr.value, 
						scope,
						&scopecounter,
						""
					);

					str_appendfmt(&result.code, " = %s", expr.code.data);
					str_append(&result.header, expr.header.data);
					str_dtor(&expr.code);
					str_dtor(&expr.header);
				}

				str_append(&result.code, ";\n");
				varcounter++;
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_WHILE)
			{
				str_append(&result.code, "while(");
				struct erw_GenerationResult expr = erw_generateexpr(
					generator,
					node->block.stmts[i]->while_.expr, 
					scope,
					&scopecounter,
					""
				);

				str_appendfmt(&result.code, "%s)\n", expr.code.data);
				struct erw_GenerationResult gresult = erw_generateblock(
					generator,
					node->block.stmts[i]->while_.block,
					scope->children[scopecounter],
					indentlvl + 1,
					module
				);

				str_append(&result.header, gresult.header.data);
				str_append(&result.code, gresult.code.data);
				str_dtor(&gresult.header);
				str_dtor(&gresult.code);
				str_dtor(&expr.header);
				str_dtor(&expr.code);

				scopecounter++;
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_IF)
			{
				str_append(&result.code, "if(");
				struct erw_GenerationResult expr = erw_generateexpr(
					generator,
					node->block.stmts[i]->if_.expr, 
					scope,
					&scopecounter,
					""
				);

				str_appendfmt(&result.code, "%s)\n", expr.code.data);
				struct erw_GenerationResult gresult = erw_generateblock(
					generator,
					node->block.stmts[i]->if_.block,
					scope->children[scopecounter],
					indentlvl + 1,
					module
				);

				str_append(&result.header, gresult.header.data);
				str_append(&result.code, gresult.code.data);
				str_dtor(&gresult.header);
				str_dtor(&gresult.code);
				str_dtor(&expr.code);
				str_dtor(&expr.header);

				scopecounter++;

				for(size_t j = 0; 
					j < vec_getsize(node->block.stmts[i]->if_.elseifs); 
					j++)
				{
					for(size_t k = 0; k < indentlvl + 1; k++)
					{ 
						str_append(&result.code, "\t");
					}

					str_append(&result.code, "else if(");
					expr = erw_generateexpr(
						generator,
						node->block.stmts[i]->if_.elseifs[j]->elseif.expr, 
						scope,
						&scopecounter,
						""
					);

					str_appendfmt(&result.code, "%s)\n", expr.code.data);
					gresult = erw_generateblock(
						generator,
						node->block.stmts[i]->if_.elseifs[j]->elseif.block, 
						scope->children[scopecounter],
						indentlvl + 1,
						module
					);

					str_append(&result.header, gresult.header.data);
					str_append(&result.code, gresult.code.data);
					str_dtor(&gresult.header);
					str_dtor(&gresult.code);
					str_dtor(&expr.header);
					str_dtor(&expr.code);

					scopecounter++;
				}

				if(node->block.stmts[i]->if_.else_)
				{
					for(size_t j = 0; j < indentlvl + 1; j++)
					{ 
						str_append(&result.code, "\t");
					}

					str_append(&result.code, "else\n");
					gresult = erw_generateblock(
						generator,
						node->block.stmts[i]->if_.else_->else_.block, 
						scope->children[scopecounter],
						indentlvl + 1,
						module
					);

					str_append(&result.header, gresult.header.data);
					str_append(&result.code, gresult.code.data);
					str_dtor(&gresult.header);
					str_dtor(&gresult.code);

					scopecounter++;
				}
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_RETURN)
			{
				//TODO: Defer
				if(node->block.stmts[i]->return_.expr)
				{
					struct erw_GenerationResult expr = erw_generateexpr(
						generator,
						node->block.stmts[i]->return_.expr, 
						scope,
						&scopecounter,
						""
					);

					str_appendfmt(&result.code, "return %s;\n", expr.code.data);
					str_append(&result.header, expr.header.data);
					str_dtor(&expr.header);
					str_dtor(&expr.code);
				}
				else
				{
					str_append(&result.code, "return;\n");
				}
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_FUNCCALL)
			{
				struct erw_GenerationResult name = erw_generateexpr(
					generator, 
					node->block.stmts[i],
					scope,
					&scopecounter,
					module
				);

				str_appendfmt(&result.code, "%s;\n", name.code.data);
				str_append(&result.header, name.header.data);
				str_dtor(&name.code);
				str_dtor(&name.header);
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_TEMPLATECALL)
			{
				struct erw_GenerationResult name = erw_generateexpr(
					generator, 
					node->block.stmts[i],
					scope,
					&scopecounter,
					module
				);

				str_appendfmt(&result.code, "%s;\n", name.code.data);
				str_append(&result.header, name.header.data);
				str_dtor(&name.code);
				str_dtor(&name.header);
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_ASSIGNMENT)
			{
				struct erw_GenerationResult assignee = erw_generateexpr(
					generator,
					node->block.stmts[i]->assignment.assignee,
					scope,
					&scopecounter,
					""
				);

				struct erw_GenerationResult expr = erw_generateexpr(
					generator,
					node->block.stmts[i]->assignment.expr,
					scope,
					&scopecounter,
					""
				);

				if(node->block.stmts[i]->token->type
					== erw_TOKENTYPE_OPERATOR_POWASSIGN)
				{
					str_appendfmt(
						&result.code, "%s = pow(%s, %s);\n", 
						assignee.code.data, 
						assignee.code.data, 
						expr.code.data
					);
				}
				else
				{
					str_appendfmt(
						&result.code, "%s %s %s;\n", 
						assignee.code.data, 
						node->block.stmts[i]->token->text, 
						expr.code.data
					);
				}

				str_dtor(&assignee.header);
				str_dtor(&assignee.code);
				str_dtor(&expr.header);
				str_dtor(&expr.code);
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_MATCH)
			{
				struct erw_GenerationResult expr = erw_generateexpr(
					generator, 
					node->block.stmts[i]->match.expr, 
					scope,
					&scopecounter,
					""
				);

				struct erw_GenerationResult gresult = erw_generatetype(
					generator, 
					node->block.stmts[i]->match.exprtype,
					scope,
					""
				);

				struct Str matchexprname;
				str_ctorfmt(
					&matchexprname, 
					ERW_PREFIX "_match%zuexpr", 
					scopecounter
				);

				str_appendfmt(
					&result.code, 
					"%s %s = %s;\n", 
					gresult.code.data,
					matchexprname.data,
					expr.code.data
				);

				for(size_t j = 0; j < indentlvl + 1; j++)
				{ 
					str_append(&result.code, "\t");
				}

				for(size_t j = 0; 
					j < vec_getsize(node->block.stmts[i]->match.cases); 
					j++)
				{
					if(j != 0)
					{
						for(size_t k = 0; k < indentlvl + 1; k++)
						{ 
							str_append(&result.code, "\t");
						}

						str_append(&result.code, "else ");
					}

					str_append(&result.code, "if(");
					if(node->block.stmts[i]->match.cases[j]->case_.expr->type 
						== erw_ASTNODETYPE_VARDECLR)
					{
						int isref = 0;
						if(node->block.stmts[i]->match.cases[j]->case_.expr
							->vardeclr.isref)
						{
							isref = 1;
						}

						struct erw_VarDeclr* newvar = &scope->children[
							scopecounter
						]->variables[0];

						struct Str typename;
						str_ctor(&typename, "");
						erw_generatetypename(
							&typename, 
							isref ? newvar->type->reference.type : newvar->type
						);

						str_upper(&typename);
						str_appendfmt(
							&result.code, 
							"%s.type == %s_%s)\n", 
							matchexprname.data,
							gresult.code.data,
							typename.data
						);

						struct erw_GenerationResult block = erw_generateblock(
							generator, 
							node->block.stmts[i]->match.cases[j]->case_.block,
							scope->children[scopecounter],
							indentlvl + 1,
							module
						);

						struct erw_GenerationResult vartype = erw_generatetype(
							generator,
							newvar->type,
							scope,
							""
						);

						str_lower(&typename);
						str_append(&result.header, vartype.header.data);

						for(size_t k = 0; k < indentlvl + 2; k++)
						{ 
							str_insert(&block.code, 3, "\t");
						}

						str_insertfmt(
							&block.code, 
							3 + indentlvl + 2,
							"%s " ERW_PREFIX "%s = %s%s." ERW_PREFIX "%s;\n", 
							vartype.code.data,
							newvar->node->vardeclr.name->text,
							isref ? "&" : "",
							matchexprname.data,
							typename.data
						);

						str_dtor(&typename);
						str_dtor(&vartype.code);
						str_dtor(&vartype.header);

						str_append(&result.header, block.header.data);
						str_append(&result.code, block.code.data);
						str_dtor(&block.code);
						str_dtor(&block.header);
					}
					else
					{
						struct erw_GenerationResult caseexpr = erw_generateexpr(
							generator, 
							node->block.stmts[i]->match.cases[j]->case_.expr,
							scope,
							&scopecounter,
							""
						);

						str_appendfmt(
							&result.code, 
							"%s == %s)\n", 
							matchexprname.data,
							caseexpr.code.data
						);

						struct erw_GenerationResult block = erw_generateblock(
							generator, 
							node->block.stmts[i]->match.cases[j]->case_.block,
							scope->children[scopecounter],
							indentlvl + 1,
							module
						);

						str_append(&result.header, block.header.data);
						str_append(&result.code, block.code.data);

						str_dtor(&block.code);
						str_dtor(&block.header);
						str_dtor(&caseexpr.code);
						str_dtor(&caseexpr.header);
					}

					scopecounter++;
				}

				if(node->block.stmts[i]->match.else_) 
				{
					for(size_t j = 0; j < indentlvl + 1; j++)
					{ 
						str_append(&result.code, "\t");
					}

					str_append(&result.code, "else\n");
					struct erw_GenerationResult block = erw_generateblock(
						generator, 
						node->block.stmts[i]->match.else_->else_.block,
						scope->children[scopecounter],
						indentlvl + 1,
						module
					);

					str_append(&result.header, block.header.data);
					str_append(&result.code, block.code.data);
					str_dtor(&block.code);
					str_dtor(&block.header);

					scopecounter++;
				}

				str_append(&result.code, "\n");
				str_dtor(&matchexprname);
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
				str_dtor(&expr.code);
				str_dtor(&expr.header);
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_BLOCK)
			{
				struct erw_GenerationResult gresult = erw_generateblock(
					generator, 
					node->block.stmts[i], 
					scope->children[scopecounter], 
					indentlvl + 1,
					module
				);

				str_appendfmt(&result.code, "\n%s\n", gresult.code.data);
				str_append(&result.header, gresult.header.data);

				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_FOREACH)
			{
				str_append(&result.code, "for(size_t i = 0; i < ");
				struct erw_Scope* forscope = scope->children[scopecounter];
				scopecounter++;

				struct erw_GenerationResult gresult = erw_generatetype(
					generator,
					node->block.stmts[i]->foreach.exprtype,
					scope,
					""
				);

				str_append(&result.header, gresult.header.data);
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);

				gresult = erw_generateexpr(
					generator, 
					node->block.stmts[i]->foreach.expr, 
					forscope,
					&scopecounter,
					""
				);
				
				str_appendfmt(
					&result.code, 
					"%s.len; i++)\n", 
					gresult.code.data
				);

				struct erw_GenerationResult block = erw_generateblock(
					generator, 
					node->block.stmts[i]->foreach.block,
					forscope,
					indentlvl + 1,
					module
				);

				struct erw_VarDeclr* newvar = &forscope->variables[0];
				struct erw_GenerationResult vartype = erw_generatetype(
					generator,
					newvar->type,
					scope,
					""
				);

				for(size_t k = 0; k < indentlvl + 2; k++)
				{ 
					str_insert(&block.code, 3, "\t");
				}

				str_insertfmt(
					&block.code, 
					3 + indentlvl + 2,
					"%s " ERW_PREFIX "%s = %s%s.arr[i];\n", 
					vartype.code.data,
					newvar->name,
					newvar->isref ? "&" : "",
					gresult.code.data
				);

				str_append(&result.header, block.header.data);
				str_append(&result.code, block.code.data);
				str_append(&result.code, "\n");

				str_dtor(&vartype.code);
				str_dtor(&vartype.header);
				str_dtor(&block.code);
				str_dtor(&block.header);
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_FOR)
			{
				str_append(&result.code, "for(");

				struct erw_Scope* forscope = scope->children[scopecounter];
				scopecounter++;

				struct erw_VarDeclr* var = &forscope->variables[0];
				struct erw_GenerationResult gresult = erw_generatetype(
					generator,
					var->type,
					scope,
					""
				);

				str_append(&result.header, gresult.header.data);
				str_append(&result.code, gresult.code.data);
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);

				str_appendfmt(
					&result.code, 
					" " ERW_PREFIX "%s", 
					var->node->vardeclr.name->text
				);
				
				if(var->node->vardeclr.value)
				{
					struct erw_GenerationResult expr = erw_generateexpr(
						generator,
						var->node->vardeclr.value, 
						forscope,
						&scopecounter,
						""
					);

					str_appendfmt(&result.code, " = %s", expr.code.data);
					str_append(&result.header, expr.header.data);
					str_dtor(&expr.code);
					str_dtor(&expr.header);
				}

				str_append(&result.code, "; ");
				gresult = erw_generateexpr(
					generator, 
					node->block.stmts[i]->for_.expr, 
					forscope, 
					&scopecounter,
					""
				);

				str_append(&result.code, gresult.code.data);
				str_append(&result.header, gresult.header.data);
				str_append(&result.code, "; ");

				struct erw_GenerationResult assignee = erw_generateexpr(
					generator,
					node->block.stmts[i]->for_.increment->assignment.assignee,
					scope,
					&scopecounter,
					""
				);

				struct erw_GenerationResult expr = erw_generateexpr(
					generator,
					node->block.stmts[i]->for_.increment->assignment.expr,
					scope,
					&scopecounter,
					""
				);

				if(node->block.stmts[i]->for_.increment->token->type
					== erw_TOKENTYPE_OPERATOR_POWASSIGN)
				{
					str_appendfmt(
						&result.code, "%s = pow(%s, %s)", 
						assignee.code.data, 
						assignee.code.data, 
						expr.code.data
					);
				}
				else
				{
					str_appendfmt(
						&result.code, "%s %s %s", 
						assignee.code.data, 
						node->block.stmts[i]->for_.increment->token->text, 
						expr.code.data
					);
				}

				str_dtor(&assignee.header);
				str_dtor(&assignee.code);
				str_dtor(&expr.header);
				str_dtor(&expr.code);
				str_append(&result.code, ")\n");

				struct erw_GenerationResult block = erw_generateblock(
					generator, 
					node->block.stmts[i]->for_.block,
					forscope,
					indentlvl + 1,
					module
				);

				str_append(&result.header, block.header.data);
				str_append(&result.code, block.code.data);
				str_append(&result.code, "\n");
				str_dtor(&block.code);
				str_dtor(&block.header);
				str_dtor(&gresult.code);
				str_dtor(&gresult.header);
			}
			else if(node->block.stmts[i]->type == erw_ASTNODETYPE_TEMPLATE)
			{
				if(node->block.stmts[i]->template.body->type
					== erw_ASTNODETYPE_TYPEDECLR)
				{
					for(size_t j = 0; 
						j < vec_getsize(
							node->block.stmts[i]->template.generations
						);
						j++)
					{
						struct erw_TypeDeclr* type = erw_scope_findtypedeclr(
							scope, 
							node->block.stmts[i]->template.generations[j]
								->typedeclr.name->text
						);

						struct erw_GenerationResult gresult 
							= erw_generatetypedeclr(
								generator, 
								type, 
								scope,
								module
							);

						str_append(&result.header, gresult.header.data);
						str_append(&result.header, gresult.code.data);
						str_dtor(&gresult.code);
						str_dtor(&gresult.header);
					}
				}
				else
				{
					for(size_t j = 0; 
						j < vec_getsize(
							node->block.stmts[i]->template.generations
						);
						j++)
					{
						struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
							scope, 
							node->block.stmts[i]->template.generations[j]
								->funcdef.name->text,
							0
						);

						struct erw_GenerationResult gresult 
							= erw_generatefuncprot(
								generator, 
								func, 
								scope,
								module
							);

						str_append(&result.header, gresult.header.data);
						str_append(&result.header, gresult.code.data);
						str_append(&result.header, "\n");
						str_dtor(&gresult.code);
						str_dtor(&gresult.header);

						gresult = erw_generateblock(
							generator, 
							func->node->funcdef.block, 
							func->scope,
							0,
							module
						);

						str_append(&result.header, gresult.header.data);
						str_append(&result.header, gresult.code.data);
						str_append(&result.header, "\n");
						str_dtor(&gresult.code);
						str_dtor(&gresult.header);
					}
				}
			}
			else
			{
				log_assert(
					0,
					"Unhandled type: %s", 
					node->block.stmts[i]->type->name
				);
			}
		}
	}

	for(size_t i = 0; i < indentlvl; i++)
	{ 
		str_append(&result.code, "\t");
	}

	str_append(&result.code, "}\n");
	return result;
}

struct Str erw_generateglobalblock(
	struct erw_Generator* generator,
	struct erw_ASTNode* ast, 
	struct erw_Scope* scope,
	const char* module,
	struct erw_Scope* mainscope)
{
	log_assert(generator, "is NULL");
	log_assert(ast, "is NULL");
	log_assert(scope, "is NULL");
	log_assert(module, "is NULL");
	log_assert(mainscope, "is NULL");

	struct Str code;
	str_ctor(&code, "");

	/*
	for(size_t i = 0; i < vec_getsize(scope->functions); i++)
	{
		struct erw_ASTNode* funcnode = scope->functions[i].node;
		struct Str funcprot = erw_generatefuncprot(funcnode);

		str_append(&code, funcprot.data);
		str_append(&code, ";");
		str_dtor(&funcprot);
	}
	*/

	for(size_t i = 0; i < vec_getsize(ast->start.children); i++)
	{
		if(ast->start.children[i]->type == erw_ASTNODETYPE_TYPEDECLR)
		{
			struct erw_TypeDeclr* type = erw_scope_findtypedeclr(
				scope, 
				ast->start.children[i]->typedeclr.name->text
			);

			struct erw_GenerationResult gresult = erw_generatetypedeclr(
				generator, 
				type, 
				scope,
				module
			);

			str_append(&code, gresult.header.data);
			str_append(&code, gresult.code.data);
			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_FUNCDEF)
		{
			struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
				scope, 
				ast->start.children[i]->funcdef.name->text,
				0
			);

			struct erw_GenerationResult gresult = erw_generatefuncprot(
				generator, 
				func, 
				func->scope,
				module
			);

			struct erw_GenerationResult gresult2 = erw_generateblock(
				generator, 
				func->node->funcdef.block, 
				func->scope,
				0,
				module
			);

			str_append(&code, gresult.header.data);
			str_append(&code, gresult2.header.data);
			str_append(&code, gresult.code.data);
			str_append(&code, "\n");
			str_append(&code, gresult2.code.data);
			str_append(&code, "\n");

			str_dtor(&gresult.code);
			str_dtor(&gresult.header);
			str_dtor(&gresult2.code);
			str_dtor(&gresult2.header);
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_FOREIGN)
		{
			if(ast->start.children[i]->foreign.node->type 
				== erw_ASTNODETYPE_TYPEDECLR)
			{
			}
			else if(ast->start.children[i]->foreign.node->type 
				== erw_ASTNODETYPE_FUNCDEF)
			{
			}
			else if(ast->start.children[i]->foreign.node->type 
				== erw_ASTNODETYPE_IMPORT)
			{
				str_appendfmt(
					&code, 
					"#include %s\n\n", 
					ast->start.children[i]->foreign.node->import.file->text
				);
			}
			else
			{
				log_assert(
					0,
					"Unhandled type: %s", 
					ast->start.children[i]->type->name
				);
			}
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_IMPORT)
		{
			struct erw_Module* importmodule = &scope->modules[
				ast->start.children[i]->import.moduleindex
			];

			struct Str globalblock = erw_generateglobalblock(
				generator, 
				importmodule->result.ast, 
				importmodule->result.scope,
				importmodule->name ? importmodule->name : "",
				mainscope
			);

			str_append(&code, globalblock.data);
			str_dtor(&globalblock);
		}
		else if(ast->start.children[i]->type == erw_ASTNODETYPE_TEMPLATE)
		{
			if(ast->start.children[i]->template.body->type
				== erw_ASTNODETYPE_TYPEDECLR)
			{
				for(size_t j = 0; 
					j < vec_getsize(
						ast->start.children[i]->template.generations
					);
					j++)
				{
					struct erw_TypeDeclr* type = erw_scope_findtypedeclr(
						module[0] == '\0' ? mainscope : scope, 
						ast->start.children[i]->template.generations[j]
							->typedeclr.name->text
					);

					struct erw_GenerationResult gresult 
						= erw_generatetypedeclr(
							generator, 
							type, 
							module[0] == '\0' ? mainscope : scope, 
							module
						);

					str_append(&code, gresult.header.data);
					str_append(&code, gresult.code.data);
					str_dtor(&gresult.code);
					str_dtor(&gresult.header);
				}
			}
			else if(ast->start.children[i]->template.body->type
				== erw_ASTNODETYPE_FUNCDEF)
			{
				for(size_t j = 0; 
					j < vec_getsize(
						ast->start.children[i]->template.generations
					);
					j++)
				{
					struct erw_FuncDeclr* func = erw_scope_findfuncdeclr(
						module[0] == '\0' ? mainscope : scope, 
						ast->start.children[i]->template.generations[j]
							->funcdef.name->text,
						0
					);

					struct erw_GenerationResult gresult 
						= erw_generatefuncprot(
							generator, 
							func, 
							module[0] == '\0' ? mainscope : scope, 
							module
						);

					str_append(&code, gresult.header.data);
					str_append(&code, gresult.code.data);
					str_append(&code, "\n");
					str_dtor(&gresult.code);
					str_dtor(&gresult.header);

					gresult = erw_generateblock(
						generator, 
						func->node->funcdef.block, 
						func->scope,
						0,
						module
					);

					str_append(&code, gresult.header.data);
					str_append(&code, gresult.code.data);
					str_append(&code, "\n");
					str_dtor(&gresult.code);
					str_dtor(&gresult.header);
				}
			}
		}
		else
		{
			log_assert(
				0,
				"Unhandled type: %s", 
				ast->start.children[i]->type->name
			);
		}
	}

	return code;
}

struct Str erw_generate(struct erw_Scope* scope)
{ 
	log_assert(scope, "is NULL");

	const char header[] = { 
		"//Generated with Erwall\n\n" //TODO: Add date and time

		"#include <inttypes.h>\n"
		"#include <math.h>\n"
		"#include <gmp.h>\n\n"

		"enum\n{\n\t" ERW_PREFIX "FALSE,\n\t" ERW_PREFIX "TRUE\n};\n\n"

		"typedef int8_t "   ERW_PREFIX "Int8;\n"
		"typedef int16_t "  ERW_PREFIX "Int16;\n"
		"typedef int32_t "  ERW_PREFIX "Int32;\n"
		"typedef int64_t "  ERW_PREFIX "Int64;\n"
		"typedef uint8_t "  ERW_PREFIX "UInt8;\n"
		"typedef uint16_t " ERW_PREFIX "UInt16;\n"
		"typedef uint32_t " ERW_PREFIX "UInt32;\n"
		"typedef uint64_t " ERW_PREFIX "UInt64;\n"
		"typedef float "    ERW_PREFIX "Float32;\n" //XXX: Not portable
		"typedef double "   ERW_PREFIX "Float64;\n" //XXX: Not portable
		"typedef char "     ERW_PREFIX "Char;\n"
		"typedef _Bool "    ERW_PREFIX "Bool;\n"
		"typedef mpz_t "    ERW_PREFIX "BigInt;\n"
		"typedef struct\n{\n\tchar _;\n} " ERW_PREFIX "None;\n\n"
	};

	const char footer[] = { 
		"int main(void)\n"
		"{\n"
		"\treturn " ERW_PREFIX "main();\n"
		"}\n\n"
	};

	struct Str code;
	str_ctor(&code, header);

	struct erw_Generator generator = {
		.generatedtypes = vec_ctor(struct erw_Type*, 0)
	};

	struct erw_Type* charslice = erw_type_new(erw_TYPEINFO_SLICE, NULL);
	charslice->slice.type = erw_type_builtins[erw_TYPEBUILTIN_CHAR];

	struct erw_GenerationResult result = erw_generatetype(
		&generator, 
		charslice, 
		scope,
		""
	);

	str_append(&code, result.header.data);
	str_dtor(&result.code);
	str_dtor(&result.header);

	struct Str globalblock = erw_generateglobalblock(
		&generator, 
		scope->node, 
		scope,
		"",
		scope
	);

	str_append(&code, globalblock.data);
	str_append(&code, footer);
	vec_dtor(generator.generatedtypes);
	str_dtor(&globalblock);

	return code;
}

